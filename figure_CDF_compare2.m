%Generate CDF curve for the fully-normalized model based on asymptotic
%approximation, numerical simulation and Gaussian approximation

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.004;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 2.2;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.1;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 5000;
dC = 1/100; %length of bin for histgram;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for each mu(i) and  CDF from T_r
%p_0 = f_a_pdf_test_3(mu_0,mu_0,T_r_0,A,B1,epsilon,beta);
%A_0 = trapz(T_r_0,p_0);
int_a = zeros(1,n);
int_sym = zeros(1,n); %cdf for symmetric case
sim_cdf = zeros(1,n);
sim_cdf_sym = zeros(1,n); %cdf for symmetric case based on numerics
int_gau = zeros(1,n);%cdf for gaussian approximation
%N = 1;
for i=1:n;
    %mu(i)
    root = f_n_root(mu(i),c1,c2);
    %root(1)
    T_r = root(2)+0.001:0.001:2; %define the domain of PDF for every different mu(i)
    T_r_sym = -sqrt(mu(i))+0.001:0.001:2; %the domain of the PDF for the symmetric case
    p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta);
    p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    %[p_a p_sym] = f_a_npdf_compare(mu(i),T_r,c1,c2,epsilon,beta); 
    %if p_a(1)<10^(-8);
        %N = N+1;
    %end;
    int_a(i) = trapz(T_r,p_a);
    int_sym(i) = trapz(T_r_sym,p_sym);
    %figure(2);plot(T_r,p_a./int_a(1));hold on;
    
    %compare the CDF curve with histogram from numerical simulations
    mu_1 = 0.8:(-v*dt):mu(i);
    m = length(mu_1);
    T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
    [h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
    sim_cdf(i) = 1-NUM/loop;
    %C = T_min:dC:2;
    %T_hist = hist(h,C)./loop./dC;
    %plot(C,T_hist,'ro');
    %axis([0 2 0 max(p_a./int_a(i))]);
    %pause;
    
     %compare the CDF curve with histogram from numerical simulations for
     %symmetric case
    %mu_1 = 0.8:(-v*dt):mu(i);
    %m = length(mu_1);
    %T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
    [h_sym,NUM_sym] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,-0.001,0);
    sim_cdf_sym(i) = 1-NUM_sym/loop;
    
    %compute the CDF by Gaussian approximation
    p_gau = f_gau_pdf(mu_1,v,dt,T_r,epsilon);
    int_gau(i) = trapz(T_r,p_gau);
end;

Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case

%CDF after normalization
%INT_a = zeros(1,n);
%for i=1:n;
    %if i<=N;
        %INT_a(i) = 1;
    %else
        %INT_a(i) = int_a(i)/int_a(N);
    %end;
%end;

Fig = figure;
%plot(mu,INT_a,'b-',mu,int_sym*Con,'r-.','LineWidth',2);
plot(mu,int_a/int_a(1),'b-',mu,int_sym*Con,'r-.','LineWidth',2);
hold on;plot(mu,sim_cdf,'k:','LineWidth',2);
hold on;plot(mu,sim_cdf_sym,'r:','LineWidth',2);
hold on;plot(mu,int_gau,'g:','LineWidth',2);
title(['(b):$$\epsilon = $$',num2str(epsilon),'; $$\beta =$$ ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
axis([0 0.05 0.85 1.05])
xlabel('$$\mu$$','fontsize',20,'interpreter','latex');
%print(Fig,'-depsc','figure_cdf_compare2.eps')

% check the behave of f_1 in O(1) for decreasing mu
%amplitude = zeros(1,n);
%for i=1:n;
    %mu_s = mu_0:(-0.01):mu(i);
    %f_1 = f_f_1_chop(mu_0,mu(i),A,B1,epsilon,beta);
    %amplitude(i) = exp(f_1);
%end;
%hold on;plot(mu,amplitude)

%Calculate the numerical simulation of temperature model for mu(i) as each
%ending value
