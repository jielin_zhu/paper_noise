%compare the bifurcation diagram for the full original asymmetric case and
%the one with c0=0;

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

mu = 0.04:-0.001:0;
m = length(mu);
%compute equilibirum of the normalized temperature asymmetric system
T = zeros(2,m);
for i=1:m;
    R = f_n_fullroot(mu(i),c0,c1,c2);
    %pause;
    T(1,i) = R(1);
    T(2,i) = R(2);
    %T(1,i) = R(3);
end;

figure(1);
plot(mu,T(1,:),'b-');hold on;plot(mu,T(2,:),'b-.');hold on;

c0=0;
mu = 0.04:-0.001:-0.02;
m = length(mu);
T = zeros(3,m);
for i=1:m;
    R = f_n_fullroot(mu(i),c0,c1,c2);
    %pause;
    if mu(i)<0;
        T(3,i) = R(1);
    else
    T(1,i) = R(1);
    T(2,i) = R(2);
    T(3,i) = R(3);
    end;
end;
index_pos = max(find(T(1,:)>0));
plot(mu(1:index_pos),T(1,1:index_pos),'r-');hold on;
plot(mu(1:index_pos),T(2,1:index_pos),'r-.');hold on;
plot(mu,T(3,:),'r-');