%check whether the CDF based on the asymptotic approximation for the
%three different symmetric case with different values of c1 and c2


%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.004;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.1;%3, size of the additional noise;
beta = 2.7;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.1;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 1000;
dC = 1/100; %length of bin for histgram;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate CDF
%p_0 = f_a_pdf_test_3(mu_0,mu_0,T_r_0,A,B1,epsilon,beta);
%A_0 = trapz(T_r_0,p_0);
int_a = zeros(1,n); %cdf for c1=1 and c2=-1
int_opp = zeros(1,n); %cdf for c1=-1 and c2=1
int_sym = zeros(1,n); %cdf for c1=c2=0
sim_cdf = zeros(1,n);
int_gau = zeros(1,n);%cdf for gaussian approximation
Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case
%N = 1;
for i=1:n; 
    %compute p_a and related CDF
    root = f_n_root(mu(i),1,-1);
    T_r = root(2)+0.001:0.0005:root(3)-0.001; %specificly for c1>0 and c2<0
    p_a = f_a_npdf(mu(i),T_r,1,-1,epsilon,beta); %for the case c1=1&c2=-1
    int_a(i) = trapz(T_r,p_a);
    
     %compute PDF and CDF for c1=-1&c2=1;
    root = f_n_root(mu(i),-1,1);
    T_r_opp = root(2)+0.001:0.0005:2; %define the domain of PDF for every different mu(i)
    p_opp = f_a_npdf(mu(i),T_r_opp,-1,1,epsilon,beta);
    int_opp(i) = trapz(T_r_opp,p_opp);
    
    %compute PDF and CDF for the symmetric case;
    T_r_sym = -sqrt(mu(i))+0.001:0.001:2; %the domain of the PDF for the symmetric case
    p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    int_sym(i) = trapz(T_r_sym,p_sym);
    
    %compare the CDF curve with histogram from numerical simulations
    mu_1 = 0.5:(-v*dt):mu(i);
    m = length(mu_1);
    T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
    [h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,-0.001,0);
    sim_cdf(i) = 1-NUM/loop;
    %C = T_min:dC:2;
    %T_hist = hist(h,C)./loop./dC;
    %plot(C,T_hist,'ro');
    %axis([0 2 0 max(p_a./int_a(i))]);
    %pause;
    
    %compute the CDF by Gaussian approximation
    p_gau = f_gau_pdf(mu_1,v,dt,T_r_sym,epsilon);
    int_gau(i) = trapz(T_r_sym,p_gau);
end;

Fig = figure;
plot(mu,int_a/int_a(1),'b-.','LineWidth',2);
hold on;plot(mu,int_opp/int_opp(1),'k-.','Linewidth',2)
%plot(mu,int_a/max(int_a)*max(int_sym)*Con,'b-','LineWidth',2);
hold on;plot(mu,int_sym*Con,'r-','LineWidth',2);
%plot(mu,INT_a,'b-',mu,int_sym*Con,'r-.','LineWidth',2);
hold on;plot(mu,sim_cdf,'k:','LineWidth',2);
hold on;plot(mu,int_gau,'g:','LineWidth',2);
title(['$$\epsilon = $$',num2str(epsilon),'; $$\beta =$$ ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
%axis([0 0.05 0.8 1.02]);
xlabel('$$\mu$$','fontsize',20,'interpreter','latex');
%print(Fig,'-depsc','figure_cdf_compare6.eps')
