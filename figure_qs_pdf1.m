%generate figure to compare the results of PDF based on quasi-stationary
%approximation, gaussian approximation, symmetric case and numerical
%simulation for relative smaller noise but will still trigger an early
%tipping

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.05;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 3;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.1;%5, length of time step
T_min = -1;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
dT = 0.001;
T_r = T_min:dT:2;
loop = 250000;

%quasi-stationary approximation at mu_n;
%p_a = f_a_npdf2(mu_n,T_r,c1,c2,epsilon,beta); 
p_a = f_a_npdf4(mu_n,T_r,c0,c1,c2,epsilon,beta); 
p_sym = f_npdf_sym7(mu_n,T_r,epsilon,beta);
%p_a0 = f_a_npdf2(mu_0,T_r,c1,c2,epsilon,beta);
p_a0 = f_a_npdf4(mu_0,T_r,c0,c1,c2,epsilon,beta);
p_sym0 = f_npdf_sym7(mu_0,T_r,epsilon,beta);
int_asym = trapz(T_r,p_a);
int_sym = trapz(T_r,p_sym0);

%compare the CDF curve with histogram from numerical simulations
mu_1 = mu_0:(-v*dt):mu_n;
m = length(mu_1);
R = f_n_root(mu_0,c1,c2);
T0 = R(1).*ones(length(T_r),1);
T_s = zeros(length(T_r),m);
T_s(:,1) = T0;
dW = randn(length(T_r),m);
for i=2:m;
    T_s(:,i) = T_s(:,i-1)+(c0.*T_s(:,i-1).^4+c1.*T_s(:,i-1).^3-T_s(:,i-1).^2+c2.*mu_1(i-1).*T_s(:,i-1)+mu_1(i-1)).*dt+epsilon.*sqrt(dt).*dW(:,i);
    %T_s(:,i) = T_s(:,i-1)+(c1.*T_s(:,i-1).^3-T_s(:,i-1).^2+c2.*mu_1(i-1).*T_s(:,i-1)+mu_1(i-1)).*dt+epsilon.*sqrt(dt).*dW(:,i);
end;
dC = 1/200;
C = 0.15:dC:0.5;
T_hist = hist(T_s(:,end),C)./length(T0)./dC;

%calculate pdf based on gaussian approximation
p_gau = f_gau_pdf(mu_1,v,dt,T_r,epsilon);

%generate figure result in .eps form
Fig = figure;
plot(C(2:end),T_hist(2:end),'Marker','o','Color','r','linewidth',1.5);
hold on;plot(T_r,p_a./int_asym,'b-','Linewidth',2);
hold on;plot(T_r,p_sym./int_sym,'k--','Linewidth',2);
hold on;plot(T_r,p_gau,'m-.','Linewidth',2);
axis([0 0.5 0 11]);
set(gca,'fontsize',18);
xlabel('$$T$$','fontsize',20,'interpreter','latex');
ylabel('$$p(T,\mu)$$','fontsize',20,'interpreter','latex');
title(['(a): $$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$v$$= ',num2str(V),'; $$\mu$$= ',num2str(mu_n)],'fontsize',20,'interpreter','latex')
print(Fig,'-depsc','figure_qs_pdf1.eps')