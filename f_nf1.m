%rewrite numerical approximation of f_1 linked to new version of psi_1T,
%psi_1TT, psi_1mu including terms as A2,B2

function [f_11 f_12 f_13] = f_nf1(mu,c1,c2,epsilon,beta)
% mu is the end point of the drifting term
% c1,c2 are coefficients for the fully normalized model
% f_11 gives the integrate of -psi1_TT/2
% f_12 gives the integration of -psi1_T^2/2
% f_13 gives the integration of -psi1_mu
% f_1 = f_11+f_12+f_13

dmu = 0.001;
mu_s = mu:dmu:0.8; %trapz range that can be changed
n = length(mu_s);

psi_1T = zeros(1,n);
psi_1TT = zeros(1,n);
psi_1mu = zeros(1,n);
for i=1:n;
    root = f_n_root(mu_s(i),c1,c2);
    T = root(1);
    psi_1T(i) = f_npsi1_t(T,mu_s(i),c1,c2);
    psi_1TT(i) = f_npsi1_tt(T,mu_s(i),c1,c2);
    psi_1mu(i) = f_npsi1_mu(T,mu_s(i),c1,c2);
end;

if isreal(psi_1mu)==0
    error('Something wrong with f_npsi1_mu.m');
end;

f_11 = -trapz(mu_s,-psi_1TT./2);
f_12 = -trapz(mu_s,-psi_1T.^2./2.*epsilon^(beta-2));
f_13 = -trapz(mu_s,-psi_1mu.*epsilon^(beta-2));