%compute partial pdf at mu=mu2 based on numerical simulation and initial
%asymptotic approximation by using one-stage conditional pdf approximation
%given pdf at mu=mu1

function p_one = f_npdf_sym_local_brownian(mu1,mu2,T,T_s,p,epsilon,beta,dt,loop);
%mu1, initial condition where asymptotic approximation alone provides good
%approximation
%mu2, where the system stops
%dt, length of small time step
%T, series of the whole temperature range, which should be separated by
%T_s
%p, pdf at mu=mu1 in the range T
%loop, rough number of numerical simulation

p_one = zeros(1,length(T));
%separate T by -sqrt(mu2);
%dT = T(2)-T(1);
v = epsilon^beta;
mu = mu1:(-v*dt):mu2;
m=1;
n = 1;
while m<length(T) && T(m)<=T_s+0.0001;
    m = m+1;
    if T(m)>-sqrt(mu1)
        n = n+1;
    end;
end;
%m
%n
%pause;
%test how many small fragments between -sqrt(mu1) and -sqrt(mu2) where the
%conditional probability actually work
%frag = (sqrt(mu1)-sqrt(mu2))/dT
%pause;

T1 = zeros(1,m);
T2 = zeros(1,length(T)-m+1);
p_11 = zeros(m,1); %pdf at mu1
for i=1:m; %assign T1 completely first then compute p_tran
    T1(i) = T(i);
    p_11(i) = p(i);
end;
%T1(end)
%p_11(end)
%generate series of T0 for numerical simulation
%dT1 = zeros(1,m+1); %generate the difference between each T1(i)
%for i=1:m;
    %dT1(i) = T1(i+1)-T1(i);
%end;
%dT1(m+1) = dT1(m);
num_T0 = zeros(1,m);%generate number of T0 the same as value T1(i)
for i=1:m;
    %N = dT1(i)*p_11(i)*loop;
    N = p_11(i)/p_11(end)*loop; %make loop to be the number of the simulation given T0 = T1(end)
    num_T0(i) = round(N);
end;
%num_T0(1)
%pause;
%figure(4);
%plot(T1,num_T0,'b-');hold on;plot(-sqrt(mu2),0,'ro');
%axis([-sqrt(mu1) T1(end) 0 max(num_T0)]);
%pause;
%generate series of T0 following the pdf p_11
T0 = ones(1,num_T0(1)).*T1(1);
for i=2:m;
    S = ones(1,num_T0(i)).*T1(i);
    T0 = [T0 S];
end;
%generate each T_n(i) based on T0(i), epsilon, beta, mu1 and mu2 by the full system;
T_n = zeros(1,length(T0));
for i=1:length(T0);
    TE = f_brownian(mu,T0(i),dt,epsilon);
    T_n(i) = TE(end);
end;
p_1 = hist(T_n,T)./length(T0)./(T(2)-T(1));
%trapz(T,p_1)
%pause;
%figure(3);plot(T,p_1,'k-');hold on;plot(-sqrt(mu2),0,'ro');
%axis([-sqrt(mu1)-0.05 0 0 max(p_1)]);
%pause;
for i=1:length(T)-m+1;
    T2(i) = T(m-1+i);
end;
p_2 = f_npdf_sym6(mu2,T2,epsilon,beta); %quasi-stationary only, p_2(1) is used to connect the two pieces
T2(1)
p_2(1)
pause;
%compare p_1 and p_2 to be one series under T
for i=1:length(T);
    if i<=m;
        p_one(i) = p_1(i)/p_1(m)*p_2(1);
    else
        p_one(i) = p_2(i-m+1);
    end;
end;

%compare the partial conditional with the full conditional
loop1 = 1000;
p_full = f_npdf_sym_local_sim2(mu1,mu2,T,-5,p,epsilon,beta,dt,loop1);

%see the behaviour of the whole p_1 and compare with the matching one with
%quasi-stationary approximation
int_p_one = trapz(T,p_one);
figure(2);
plot(T,p_1,'r-.');hold on;
plot(T,p_one./int_p_one,'b-');hold on;
plot(T,p_full,'k:');hold on;
plot(-sqrt(mu2),0,'ro');hold on;
plot(T_s,0,'k*');
pause;

p_one = p_one./int_p_one; %including the normalization part
%trapz(T,p_one)
%pause;

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%figure(1);
%plot(T,p_one);hold on;plot(T_s,0,'ko');hold on;plot(-sqrt(mu2),0,'b*');
%axis([-sqrt(mu1) -sqrt(mu2) 0 max(p_one)]);
%pause;

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%before normalization with the asymptotic part
%figure(2);
%plot(T1,p_11,'r-.');hold on; %show the conditional pdf part based on T_s
%plot(-sqrt(mu2),0,'ko');
%figure(3);
%plot(T2,p_2);
%axis([-sqrt(mu1)-0.05 -sqrt(mu2)+0.05 0 max(p_11)]);
%pause;