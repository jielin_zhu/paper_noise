%generate the figure to show  80% of probability where T stay around the
%stable equilibrium

load file_PDF_asym_merge2.dat
[m1,n1] = size(file_PDF_asym_merge2);
T = file_PDF_asym_merge2(1:m1-1,1);
mu = file_PDF_asym_merge2(end,2:end);
pdf_asym_merge = file_PDF_asym_merge2(1:m1-1,2:end); %each column is pdf at mu(i)
%load file_PDF_asym_num.dat
%pdf_asym_num = file_PDF_asym_num(1:m1-1,2:end);

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
beta = 2.5;
dt = 0.1;
T_min = -3;

%two series for upper and lower bound of T at each mu(i)
%satisfying prob(T<Tl)=prob(T>Th)=0.05
Tl = zeros(length(mu),1);
Th = zeros(length(mu),1);
for i=1:length(mu);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T,pdf_asym_merge(:,i)); %computing the escaping probability below Td
    while j1<length(T) && prob_upper<0.05;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T(end-j1:end)',pdf_asym_merge(end-j1:end,i));
    end;
    %prob_upper
    Th(i) = T(end-j1);
    while j2<length(T) && prob_lower<0.05; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T,pdf_asym_merge(:,i));
        prob_lower = X+trapz(T(1:j2)',pdf_asym_merge(1:j2,i));
    end;
    %prob_lower
    Tl(i) = T(j2);
    
    %{
    figure(5);
    plot(T,pdf_asym_merge(:,i),'b-');hold on;
    plot(Th(i),0,'ro');hold on;plot(Tl(i),0,'r*');hold off;
    pause;
    %}
end;

%also generate bifurcation diagram
%compute equilibirum of the normalized temperature asymmetric system
mu_s = mu(1):(-epsilon^beta*dt):mu(end);
index_pos = max(find(mu_s>0));
T_e = zeros(2,index_pos);
for i=1:index_pos;
    R = f_n_root(mu_s(i),c1,c2);
    %pause;
    T_e(1,i) = R(1);
    T_e(2,i) = R(2);
    %T(1,i) = R(3);
end;

%draw the tipping related to only drifting rate
T_s = zeros(1,length(mu_s));
T_s(1) = T_e(1,1);
i=1;
while i<length(mu_s) && T_s(i)>T_min;
    i = i+1;
    K1 = c1*T_s(i-1)^3-T_s(i-1)^2+c2*mu_s(i-1)*T_s(i-1)+mu_s(i-1);
    X_pre = T_s(i-1)+K1*dt;
    K2 = c0*X_pre^4+c1*X_pre^3-X_pre^2+c2*mu_s(i)*X_pre+mu_s(i);
    T_s(i) = T_s(i-1)+(K1+K2)*dt/2;
end;
%mu_s(i)
%pause;
Y = -1:0.01:1;
T_ver = mu_s(i).*ones(length(Y),1);

Fig = figure;
plot(mu,Tl,'b--','linewidth',2);hold on;plot(mu,Th,'b--','linewidth',2);hold on;
plot(mu_s(1:index_pos),T_e(1,:),'k-','linewidth',2);hold on;plot(mu_s(1:index_pos),T_e(2,:),'k-.','linewidth',2);hold on;
%plot(T_ver,Y,'r-.');
xlabel('$$\mu$$','FontSize',40,'interpreter','latex');
set(gca,'fontsize',18);
axis([0 0.03 -0.5 0.4]);
print(Fig,'-depsc','figure_attract_prob.eps')