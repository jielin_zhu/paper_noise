% compute PDF for the symmetric case with one more psi approximation for
% the limitation beta+3alpah/2<4

function p_sym = f_npdf_sym3(mu,T,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

n1 = length(T);
f1_sym = log(mu)/4;%-epsilon^(beta-2)*log(4)
f1_check = log(mu)/4+epsilon^(beta-2)*log(mu)/4;
%f2_sym = epsilon^(2)*(-5*mu^(-3/2)/96);
f2_sym = epsilon^2*(7/48*mu^(-3/2));
f3_sym = epsilon^4*(-5/256*mu^(-3));
%exp_psi0_sym = zeros(1,n1);
psi0_sym = zeros(1,n1);
psi1_sym = zeros(1,n1);
psi2_sym = zeros(1,n1);
psi3_sym = zeros(1,n1);
p_sym = zeros(1,n1);

%check the difference by adding higher order terms
p_check = zeros(1,n1);
p_origin = zeros(1,n1);

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    psi1_sym(i) = epsilon^(beta-2)*(2*log((sqrt(mu)+T(i)))-log(mu));
    %psi2_sym(i) = epsilon^beta*(-1/(T(i)+sqrt(mu))-sqrt(mu)/(T(i)+sqrt(mu))^2)/4/mu+epsilon^beta*3*mu^(-3/2)/8;
    psi2_sym(i) = epsilon^beta*(-1/(T(i)+sqrt(mu))-sqrt(mu)/(T(i)+sqrt(mu))^2)/4/mu+epsilon^beta*mu^(-3/2)/2;
    psi3_sym(i) = epsilon^(2*beta-2)*((T(i)+2*sqrt(mu)/2/mu/(T(i)+sqrt(mu))^2)+3*mu^(-3/2)/8);
    %exp_psi0_sym(i) = exp(psi0_sym(i)/epsilon^2);
    %p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i)+psi3_sym(i)+f1_sym+f2_sym);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+f1_sym+f2_sym+f3_sym);
    p_origin(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+f1_sym);
    psi1_check = epsilon^(beta-2)*(2*log(sqrt(mu)+T(i))-log(4*mu))
    p_check(i) = exp(psi0_sym(i)/epsilon^2+psi1_check+f1_check);
end;

if isreal(psi1_sym)==0
    error('Something wrong with psi0_sym, doulbe check T_r');
end;

int_origin = trapz(T,p_origin);
int_check = trapz(T,p_check);

figure(5);
plot(T,p_origin,'r-');
hold on;plot(T,p_check,'b-.');%hold on;plot(T,p_sym,'k:');
%figure(6);
%plot(T,exp(psi2_sym));hold on;%axis([min(T) max(T) 0 5]);
figure(7);
plot(T,p_origin/int_origin,'r-');hold on;plot(T,p_check/int_check,'b-.');
%figure(8);
%plot(mu,exp(f2_sym),'bo');hold on;
%max(p_check)-max(p_origin)
figure(8);
plot(mu,exp(f1_sym),'ro');hold on;plot(mu,exp(f1_sym)*exp(f2_sym)*exp(f3_sym),'b*');hold on;
%exp_f1 = exp(f1_sym)
%exp_f2 = exp(f2_sym)
%exp_f2f3 = exp(f3_sym)*exp(f2_sym)
pause;