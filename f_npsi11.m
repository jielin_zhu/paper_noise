%full analytic form of psi11 including T2 and T3

function psi11 = f_npsi11(T,mu,c1,c2)
%T, specific point we want to approximate psi_{1mu};
%mu, bifurcation parameter used to determine the equilibiria of the system
%c1,c2 are coefficients for the fully normalized model

root = f_n_root(mu,c1,c2);
R1 = root(1); %the stable equilibrium
R2 = root(2); %the unstable equilibrium
R3 = root(3);

%separate psi11 for case c1>0,c2<0 and other values
if c1>0 && c2<=0;
    psi_11_num = (2+c2*(R1+R2))*(log(T-R2)-log(-R2))-(2+c2*(R1+R3))*(log(R3-T)-log(R3));
    %check = log(T-R2)-log(-R2)
    if isreal(psi_11_num)==0
    error('Something wrong with psi_11_num, doulbe check T_r');
    end;
else
    psi_11_num = -(2+c2*(R1+R3))*(log(T-R3)-log(-R3))+(2+c2*(R1+R2))*(log(T-R2)-log(-R2));
end;
psi_11_den = c1*(R3-R2);

%Particular for c1<0 and c2>0
%root = f_n_root(mu,c1,c2);
%T1 = root(1); %the stable equilibrium
%T2 = -root(2); %the unstable equilibrium
%T3 = -root(3);
%psi_11_num = -(2+c2*(T1-T3))*(log(T+T3)-log(T3))+(2+c2*(T1-T2))*(log(T+T2)-log(T2));
%psi_11_den = c1*(T2-T3);

psi11 = psi_11_num/psi_11_den;
