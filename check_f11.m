%check f11 for different values of c1 and c2

%define initial coefficients
c1 = 1;
c2 = -1;
mu_0 = 0.1;
mu_n = 0.005;
epsilon = 0.02;
beta = 2.2;

%define the require series of mu
mu = mu_0:(-0.002):mu_n;
n = length(mu);

F1 = zeros(1,n); %check if it's 0 for the symmetric case
F1_sym = zeros(1,n);
F11 = zeros(1,n);
F12 = zeros(1,n);
F13 = zeros(1,n);
F0 = zeros(1,n);
F0_sym = zeros(1,n);
int_a = zeros(1,n);
for i=1:n;
    root = f_n_root(mu(i),c1,c2);
    %T_r = root(2)+0.001:0.0005:2; %define the domain of PDF for every different mu(i)
    T_r = root(2)+0.001:0.0005:root(3)-0.001; %specificly for c1>0 and c2<0
    T_r_sym = -sqrt(mu(i))+0.001:0.001:2; %the domain of the PDF for the symmetric case
    [f1 f2 f3] = f_nf1(mu(i),c1,c2,epsilon,beta);
    F1(i) = exp(f1+f2+f3);
    %F11(i) = f1;%exp(f1);
    %F12(i) = f2;%exp(f2);
    %F13(i) = f3;%exp(f3);
    F1_sym(i) = exp(log(mu(i))/4);
    F0(i) = f_nf0(mu(i),c1,c2);
    F0_sym(i) = -4*mu(i)^(3/2)/3;
    
    %check f1 for each mu(i) before the integration
    %dmu = 0.001;
    %mu_s = mu(i):dmu:0.16;
    %m = length(mu_s);
    %psi11_TT = zeros(1,m);
    %for j=1:m;
        %root = f_n_root(mu_s(j),c1,c2);
        %T = root(1);
        %psi11_TT(j) = f_npsi1_tt(T,mu_s(j),c1,c2);
    %end;
    %figure(1);hold on;plot(mu_s,psi11_TT,'r-','Linewidth',2);pause;
    
    %compute p_a and related CDF for different f11
    %p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta);
    %int_a(i) = trapz(T_r,p_a);
end;

%figure(1);
%plot(mu,F11,'k-.','Linewidth',2);grid on; hold on;
%figure(2);
%plot(mu,exp(F11),'b-.','Linewidth',2);grid on; hold on;
figure(2);
plot(mu,F1/F1(1));grid on;
%plot(mu,F1,'b-',mu,F1_sym,'r-.','Linewidth',2);hold on;
%plot(mu,F11,'k-',mu,F12,'k-.',mu,F13,'k:','Linewidth',2);
%figure(4);
%plot(mu,int_a/int_a(1),'b-','Linewidth',2);hold on;grid on;