%seprate the numerical simulation for the temperature into two different
%stage
%for mu1 = mu_0:dmu:epsilon^(4/3) using the simulation of the full dynamic
%system
%for mu2 = mu1(n):dmu:mu_n using the simulation of the temperature model
%with fixed mu_middle

function [T_final,NUM] = f_conditional_hist2(loop,mu1,mu2,dt,epsilon,T_min,T_exit,c1,c2)
%return to series of T_final at mu2(n)
%T_exit gives the smallest value of T to determine early escape
%T_min represents the unstable equilibrium the asymptotic approximation can
%get

r = f_n_root(mu1(1),c1,c2);
%T_0 = f_T_cen(mu(1),A,B1);
T_0 = r(1);
T_final = zeros(1,loop);
i1 = 1;
NUM = 0;
NUM_tipping_check = 0;
mu3 = ones(1,length(mu2)).*mu2(1);%series for the value of mu in the second part of the simulation
while (i1<loop+1)
    TE1 = f_ntemperature(mu1,T_0,dt,epsilon,T_exit,c1,c2);
    %TE2 = f_brownian(mu2,TE1(length(TE1)),dt,epsilon);
    TE2 = f_ntemperature(mu3,TE1(length(TE1)),dt,epsilon,T_exit,c1,c2);
    n1 = length(TE2);
    T_final(i1) = TE2(n1);
    if TE2(n1) <= T_min;
        NUM = NUM+1;
        if TE2(n1) <= T_exit;
            NUM_tipping_check = NUM_tipping_check+1;
        end;
    end;
    i1 = i1+1;
end;


NUM_tipping_check