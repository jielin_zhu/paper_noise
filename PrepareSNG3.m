% the decreasing rate of drift is defined by a_0, a_t, drift \mu and length of each
% time step dt
function dy = PrepareSNG3(t,y) % use ODE45 to solve the system for saddle node bifurcation ODE

dy = zeros(5,1);

%y(1) = F, satisfying F'=a-u*t-F^2;
%y(2) = Q, satisfying Q'=F;
%y(3) = g, satisfying g'=-2g^2-4*g*F
%y(4) = mu, satisfying mu'=0;
%y(5) = a_t, satisfying a'=-u;

dy(1) = y(5)-y(1)^2;
dy(2) = y(1);
dy(3) = -2*y(3)^2+4*y(3)*y(1);
dy(4) = 0;
dy(5) = -y(4);

%call with
%[T,Y] = ode45(@PrepareSNG,[0 80],[10^(-6) 1 1]);
%
%plot with
%plot(T,Y(:,3))