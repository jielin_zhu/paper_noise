function T = f_ntemperature(mu,T_0,dt,epsilon,T_min,c1,c2)
%return to normalized temperature series by one-path simulation
%mu is slowing decreasing with rate v
%T_0 gives the normalized initial condition
%dt is the time step for each simulation
%epsilon provides small additive noise
%T_min provides lower bound of the process
%A2,B2 are asymmetric terms in the temperature model

n = length(mu);%provide time step for the process
T = zeros(1,n);
dW = randn(1,n);
T(1) = T_0;

for i=2:n;
    X = T(i-1)+((c1*T(i-1)^3-T(i-1)^2)+c2*mu(i)*T(i-1)+mu(i))*dt+epsilon*sqrt(dt)*dW(i); %ignore the highest order;
    %X = T(i-1)+(((-A2)*T(i-1)^4+4*(-A2)*T(i-1)^3+6*(-A)*T(i-1)^2)+B2*mu(i)*T(i-1)/4+B1*mu(i)/16)*dt+epsilon*sqrt(dt)*dW(i);
    %X = T(i-1)+(-A*(4*T(i-1)^3+6*T(i-1)^2)+B1*mu(i)*T(i-1)/4+B1*mu(i)/16)*dt+epsilon*sqrt(dt)*dW(i);
    if X<T_min;
        T(i) = T_min;
        %T(n) = T_min;
        %break;
    else
        T(i) = X;
    end;
end;


