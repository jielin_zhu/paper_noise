% compute PDF for case A for the fully normalized model and compare with
% the symmetric case;

function [p p_sym] = f_a_npdf_compare(mu,T,c1,c2,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

F = f_nf0(mu,c1,c2);
%if isreal(F)==0 %check the validity of f0
    %error('Something wrong with f_nf0.m');
%end;

n1 = length(T);
psi_0 = zeros(1,n1);
psi_11 = zeros(1,n1);
[f11 f12 f13] = f_nf1(mu,c1,c2,epsilon,beta);
f_1 = f11+f12+f13;
%if isreal(f_1)==0 %check the validity for f1
    %error('Something wrong with f_nf1.m');
%end;
f1_sym = log(mu)/4;%-epsilon^(beta-2)*log(4)

exp_psi = zeros(1,n1);
exp_psi0 = zeros(1,n1);
exp_psi0_sym = zeros(1,n1);
psi0_sym = zeros(1,n1);
psi1_sym = zeros(1,n1);
p_sym = zeros(1,n1);

for i=1:n1;
    psi_0(i) = -2*T(i)^3/3+2*mu*T(i)+c1*T(i)^4/2+c2*mu*T(i)^2;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    psi_11(i) = f_npsi11(T(i),mu,c1,c2);
    %psi1_sym(i) = epsilon^(beta-2)*(2*log(sqrt(mu)+T(i))-log(mu));
    psi1_sym(i) = epsilon^(beta-2)*(log((sqrt(mu)+T(i))^2)-log(mu));
    exp_psi(i) = exp((psi_0(i)+F)/epsilon^2+psi_11(i)*epsilon^(beta-2)+f_1);%to avoid large value in exp_psi0, we need to combine psi and f first before divided by epsilon^2
    %exp_psi0(i) = exp(psi_0(i)/epsilon^2);
    exp_psi0_sym(i) = exp(psi0_sym(i)/epsilon^2);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+f1_sym);
end;

%check the validity of psi_1;
%if isreal(psi_11)==0
    %error('Something wrong with f_npsi11.m');
%end;

%p = exp_psi.*exp(F/epsilon^2).*exp(f_1);
p = exp_psi;

%check the validity of p and p_sym
if isreal(p)==0
    error('Something wrong with p_a');
end;
if isreal(psi1_sym)==0
    error('Something wrong with psi0_sym');
end;

%p_test = exp_psi0.*exp(F/epsilon^2);
%figure(5)
%plot(mu,exp(f_1),'b*',mu,exp(f1_sym),'ro');hold on;
%plot(T,p,'b-',T,p_sym,'r-.');pause; %the peak is higher in the asymmetric version
%plot(T,psi_0+F,'b-',T,psi0_sym,'r-.');%psi0+f0 fits well
%plot(T,exp_psi0.*exp(F/epsilon^2),'b-',T,exp_psi0_sym,'r-.');
%plot(T,psi_11.*epsilon^(beta-2),'b-',T,psi1_sym,'r-.'); %psi1 fits well
%psi1(T)-psi1(0)
%figure(6)
%plot(T,psi_0+F-psi0_sym);
%figure(7)
%plot(T,exp_psi0.*exp(F/epsilon^2)-exp_psi0_sym);
%plot(mu,exp(f_1)-exp(f1_sym),'b*');hold on;
%plot(mu,f_1,'b*',mu,f1_sym,'ro');hold on; %f1 and f1_sym roughly
%consistent;
%F
%figure(1);plot(T,psi_0);hold on;plot(T,psi_0+F,'r');grid on;
%figure(2);plot(T,(psi_0+F)/epsilon^2);axis([0.3 0.5 -20 20]);grid on;
%figure(3);plot(T,p)
%pause;