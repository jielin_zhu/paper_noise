%return to the probability density function based on Gaussian type
%approximation
%finally we compare this with the results based one WKB-like approximation

function gau_pdf = f_gau_pdf(a,mu,dt,x_r,epsilon)
%a is the drift series: a_t = a_0-mu*t
%mu is the drift rate
%dt is the length of time step
%x_r is the range of x for final pdf

n = length(a);
T0 = 0:0.1:n*dt;
g1 = 0;
g2 = 1/2;

[T,Y] = ode45(@PrepareSNG3,T0,[sqrt(a(1)) g1 g2 mu a(1)]);
M = length(x_r);
m = length(T);
gau_pdf = zeros(1,M);
B = 1/sqrt(pi);
for i=1:M;
    gau_pdf(i) = (B/epsilon)*sqrt(Y(m,3))*exp(-(x_r(i)-Y(m,1)-epsilon*exp(-2*Y(m,2)))^2*Y(m,3)/epsilon^2);
end;




