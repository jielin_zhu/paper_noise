%load files for pdf of symmetric system with drifting rate 0.01 and epsilon
%= 0.2 as the example in periodic paper

load file_PDF_sym_merge4.dat
[m1,n1] = size(file_PDF_sym_merge4);
T_sym = file_PDF_sym_merge4(1:m1-1,1);
mu_sym = file_PDF_sym_merge4(end,2:end);
pdf_sym_merge = file_PDF_sym_merge4(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_sym_num4.dat
pdf_sym_num = file_PDF_sym_num4(1:m1-1,2:end);


%show if the PDF agrees at each mu(i)
for i=2:length(mu_sym);
    figure(1);
    plot(T_sym,pdf_sym_merge(:,i),'b-');hold on;
    plot(T_sym,pdf_sym_num(:,i),'r-.');hold on;
    plot(-sqrt(mu_sym(i)),0,'b*');hold on; %T2
    plot(sqrt(mu_sym(i)),0,'bo');hold on; %T1
    plot(-sqrt(mu_sym(i-1))-2*0.2,0,'k*');hold on;
    %plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
    hold off;
    %axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu_sym(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}

T2 = -2.*ones(1,length(mu_sym));%-sqrt(mu_sym);
CDF_sym = 1;
CDF_sym_num = 1;
for i=1:length(mu_sym);
    index_T = min(find(T_sym>T2(i)));
    
    P_sym = trapz(T_sym(index_T:end),pdf_sym_merge(index_T:end,i));
    CDF_sym = [CDF_sym P_sym];
    P_sym_num = trapz(T_sym(index_T:end),pdf_sym_num(index_T:end,i));
    CDF_sym_num = [CDF_sym_num P_sym_num];
end;
figure;
plot(mu_sym,CDF_sym(2:end),'r-.','linewidth',2);hold on;
plot(mu_sym,CDF_sym_num(2:end),'k--','linewidth',2);hold on;