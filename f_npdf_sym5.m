% compute PDF for the symmetric case by making psi_j = psi_j(T)-psi_j(T1) to eliminate the amplitude change which happen in psi_j 
%for the limitation beta+3alpah/2<4
% return to both the full approximation and the leading order approximation

function [p_sym p_sym_LO] = f_npdf_sym5(mu,T,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

n1 = length(T);
f1_sym = log(mu)/4;
f2_sym = epsilon^2*(-5*mu^(-3/2)/96);
f3_sym = epsilon^beta*(3/16*mu^(-3/2));
f4_sym = epsilon^(2*beta-2)*(-mu^(-3/2)/8);

psi0_sym = zeros(1,n1);
psi1_sym = zeros(1,n1);
psi2_sym = zeros(1,n1);
psi3_sym = zeros(1,n1);
p_sym = zeros(1,n1);
p_norm = zeros(4,n1); %check the shape change from psi_j

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    psi1_sym(i) = epsilon^(beta-2)*(2*log(sqrt(mu)+T(i))-log(4*mu));
    psi2_sym(i) = epsilon^beta*(-1/(T(i)+sqrt(mu))-sqrt(mu)/(T(i)+sqrt(mu))^2)/4/mu+epsilon^beta*3*mu^(-3/2)/16;
    psi3_sym(i) = epsilon^(2*beta-2)*(1/mu/(T(i)+sqrt(mu))+sqrt(mu)/mu/2/(T(i)+sqrt(mu))^2-5*mu^(-3/2)/8);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i)+psi3_sym(i)+f1_sym+f2_sym+f3_sym+f4_sym);
    p_norm(1,i) = exp(psi0_sym(i)/epsilon^2);
    p_norm(2,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i));
    p_norm(3,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i));
    p_norm(4,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i)+psi3_sym(i));
end;
p_sym_LO = p_norm(1,:);

if isreal(psi1_sym)==0
    error('Something wrong with psi0_sym, doulbe check T_r');
end;

%figure(3);
%plot(T,p_norm(1,:),'b-');hold on;plot(T,p_norm(2,:),'r-.');hold on;plot(T,p_norm(4,:),'k:');grid on;
%title(['\mu=',num2str(mu)]);

%figure(4);
%plot(mu,exp(f1_sym),'bo');hold on;plot(mu,exp(f1_sym+f2_sym),'r*');hold on;plot(mu,exp(f1_sym+f2_sym+f3_sym),'kd');hold on;plot(mu,exp(f1_sym+f2_sym+f3_sym+f4_sym),'gs');grid on;

%int_norm1 = trapz(T,p_norm(2,:));
%int_norm2 = trapz(T,p_norm(4,:));
%figure(5);
%plot(mu,int_norm1,'bo');hold on;plot(mu,int_norm2,'r*');grid on;
%figure(6);
%cdf_norm1 = int_norm1*exp(f1_sym); vpa(cdf_norm1,8);
%cdf_norm2 = int_norm2*exp(f1_sym+f2_sym+f3_sym+f4_sym);vpa(cdf_norm2,8);
%plot(mu,cdf_norm1,'bo');hold on;plot(mu,cdf_norm2,'r*');grid on;
%pause;