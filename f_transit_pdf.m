%generate the transit pdf matrix based on simulation based on the full
%system

function p_tran = f_transit_pdf(T,mu1,mu2,loop,dt,epsilon,beta)
%T, series of T starting from T_min stopped at T_s
%mu1, initial mu for the full system
%mu2, ending mu for the full system
%dt, time step

v = epsilon^beta;
mu = mu1:(-v*dt):mu2;
T_min = T(1);
[h,NUM] = f_ntem_histgram_count2(loop,mu,dt,epsilon,T(1),-sqrt(mu2),T_min,0.001,0);
T_hist = hist(h,T)./loop;
p_tran = T_hist';
for i=2:length(T);
    T_0 = T(i);
    [h,NUM] = f_ntem_histgram_count2(loop,mu,dt,epsilon,T_0,-sqrt(mu2),T_min,0.001,0);
    T_hist = hist(h,T)./loop;
    p_tran = [p_tran T_hist'];
end;