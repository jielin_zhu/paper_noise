%show figures of the asymptotic approximation combined with the left tail
%approximation to give a general expression how the iteration works
%only for the symmetric case

load file_PDF_sym_merge2.dat
[m2,n2] = size(file_PDF_sym_merge2);
T_sym = file_PDF_sym_merge2(1:m2-1,1);
mu_sym = file_PDF_sym_merge2(end,2:end);
pdf_sym_merge = file_PDF_sym_merge2(1:m2-1,2:end); %each column is pdf at mu(i)

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
beta = 2.5;
mu = epsilon^beta;
T2 = -sqrt(mu_sym); %zeros(1,length(mu_asym)); %need to be computed for each mu(i)
%index_imag = find(mu_asym<0);
%for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    %R = f_n_fullroot(mu_asym(i),c0,c1,c2);
    %T2(i) = R(2);
%end;
Ts = T2+10*epsilon;
Td = (T2-2*epsilon).*ones(1,length(mu_sym));


index_sym1 = max(find(mu_sym>0.03));
Y = 0:0.01:8;
T_asym2 = T2(index_sym1).*ones(length(Y),1);
Fig1 = figure;
plot(T_sym,pdf_sym_merge(:,index_sym1),'k--','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);
%the actural x_d, x_m, x_s should be for a with difference mu*t where t=2
t=2;
xd = -sqrt((mu_sym(index_sym1)+mu*t))-2*epsilon;
xm = -sqrt((mu_sym(index_sym1)+mu*t))+epsilon;
xs = -sqrt((mu_sym(index_sym1)+mu*t))+10*epsilon;
plot(xd,0,'ro','markerface','r','markersize',8);hold on;plot(xm,0,'b^','markerface','b','markersize',8);hold on;plot(xs,0,'ko','markerface','k','markersize',8);
hold off;
axis([-0.4 0.4 0 7]);
set(gca,'fontsize',18);
title(['$$a$$= ',num2str(mu_sym(index_sym1))],'fontsize',40,'interpreter','latex');
xlabel('$$x$$','fontsize',40,'interpreter','latex');
ylabel('$$p(x,a)$$','fontsize',40,'interpreter','latex');
print(Fig1,'-depsc','figure_PDF_merge_tail1.eps')

index_sym1 = max(find(mu_sym>0.02));
Y = 0:0.01:8;
T_asym2 = T2(index_sym1).*ones(length(Y),1);
Fig2 = figure;
plot(T_sym,pdf_sym_merge(:,index_sym1),'k--','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);
%the actural x_d, x_m, x_s should be for a with difference mu*t where t=2
t=2;
xd = -sqrt((mu_sym(index_sym1)+mu*t))-2*epsilon;
xm = -sqrt((mu_sym(index_sym1)+mu*t))+epsilon;
xs = -sqrt((mu_sym(index_sym1)+mu*t))+10*epsilon;
plot(xd,0,'ro','markerface','r','markersize',8);hold on;plot(xm,0,'b^','markerface','b','markersize',8);hold on;plot(xs,0,'ko','markerface','k','markersize',8);
hold off;
axis([-0.4 0.4 0 7]);
set(gca,'fontsize',18);
title(['$$a$$= ',num2str(mu_sym(index_sym1))],'fontsize',40,'interpreter','latex');
xlabel('$$x$$','fontsize',40,'interpreter','latex');
ylabel('$$p(x,a)$$','fontsize',40,'interpreter','latex');
print(Fig2,'-depsc','figure_PDF_merge_tail2.eps')

index_sym1 = max(find(mu_sym>0.01));
Y = 0:0.01:8;
T_asym2 = T2(index_sym1).*ones(length(Y),1);
Fig3 = figure;
plot(T_sym,pdf_sym_merge(:,index_sym1),'k--','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);
%the actural x_d, x_m, x_s should be for a with difference mu*t where t=2
t=2;
xd = -sqrt((mu_sym(index_sym1)+mu*t))-2*epsilon;
xm = -sqrt((mu_sym(index_sym1)+mu*t))+epsilon;
xs = -sqrt((mu_sym(index_sym1)+mu*t))+10*epsilon;
plot(xd,0,'ro','markerface','r','markersize',8);hold on;plot(xm,0,'b^','markerface','b','markersize',8);hold on;plot(xs,0,'ko','markerface','k','markersize',8);
hold off;
axis([-0.4 0.4 0 7]);
set(gca,'fontsize',18);
title(['$$a$$= ',num2str(mu_sym(index_sym1))],'fontsize',40,'interpreter','latex');
xlabel('$$x$$','fontsize',40,'interpreter','latex');
ylabel('$$p(x,a)$$','fontsize',40,'interpreter','latex');
print(Fig3,'-depsc','figure_PDF_merge_tail3.eps')

index_sym1 = max(find(mu_sym>0.005));
Y = 0:0.01:8;
T_asym2 = T2(index_sym1).*ones(length(Y),1);
Fig4 = figure;
plot(T_sym,pdf_sym_merge(:,index_sym1),'k--','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);
%the actural x_d, x_m, x_s should be for a with difference mu*t where t=2
t=2;
xd = -sqrt((mu_sym(index_sym1)+mu*t))-2*epsilon;
xm = -sqrt((mu_sym(index_sym1)+mu*t))+epsilon;
xs = -sqrt((mu_sym(index_sym1)+mu*t))+10*epsilon;
plot(xd,0,'ro','markerface','r','markersize',8);hold on;plot(xm,0,'b^','markerface','b','markersize',8);hold on;plot(xs,0,'ko','markerface','k','markersize',8);
hold off;
axis([-0.4 0.4 0 7]);
set(gca,'fontsize',18);
title(['$$a$$= ',num2str(mu_sym(index_sym1))],'fontsize',40,'interpreter','latex');
xlabel('$$x$$','fontsize',40,'interpreter','latex');
ylabel('$$p(x,a)$$','fontsize',40,'interpreter','latex');
print(Fig4,'-depsc','figure_PDF_merge_tail4.eps')