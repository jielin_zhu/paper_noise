%compute partial pdf at mu=mu2 based only on numerical simulation by using one-stage conditional pdf approximation
%given pdf at mu=mu1
%use the full range of T without the separation by T_s

function p_one = f_npdf_sym_local_sim2(mu1,mu2,T,T_min,p,epsilon,beta,dt,loop);
%mu1, initial condition where asymptotic approximation alone provides good
%approximation
%mu2, where the system stops
%dt, length of small time step
%T, series of the whole temperature range
%T_min, value that defines the early tipping
%p, pdf at mu=mu1 in the range T
%loop, rough number of numerical simulation for T(i) where max(p) is

v = epsilon^beta;
mu = mu1:(-v*dt):mu2;
p_max = max(p);
num_T0 = zeros(1,length(T));%generate number of T0 the same as value T1(i)
for i=1:length(T);
    %N = dT1(i)*p_11(i)*loop;
    N = p(i)/p_max*loop; %make loop to be the number of the simulation given T0 = T1(end)
    num_T0(i) = round(N);
end;
SUM_full = sum(num_T0) %used to adjust the number of realizations for full-system simulation
%figure(4);
%plot(T1,num_T0,'b-');hold on;plot(-sqrt(mu2),0,'ro');
%axis([-sqrt(mu1) T1(end) 0 max(num_T0)]);
%pause;
%generate series of T0 following the pdf p_11
T0 = ones(1,num_T0(1)).*T(1);
for i=2:length(T);
    S = ones(1,num_T0(i)).*T(i);
    T0 = [T0 S];
end;
%figure(3);plot(T,num_T0);pause;
%generate each T_n(i) based on T0(i), epsilon, beta, mu1 and mu2 by the full system;
T_n = zeros(1,length(T0));
%T_min = -5;
for i=1:length(T0);
    %TE = f_ntemperature(mu,T0(i),dt,epsilon,T(1),0.001,0);
    TE = f_ntemperature(mu,T0(i),dt,epsilon,T_min,0.001,0);
    T_n(i) = TE(end);
end;
%p_one = hist(T_n,T)./length(T0);
p_one = hist(T_n,T)./length(T0)./(T(2)-T(1));%introduce dC to be consistent with full-system simulation
%trapz(T,p_one)

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%figure(1);
%plot(T,p_one);hold on;plot(T_s,0,'ko');hold on;plot(-sqrt(mu2),0,'b*');
%axis([-sqrt(mu1) -sqrt(mu2) 0 max(p_one)]);
%pause;

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%before normalization with the asymptotic part
%figure(2);
%plot(T,p_one,'r-.');hold on; %show the conditional pdf part based on T_s
%plot(-sqrt(mu2),0,'ko');
%figure(3);
%plot(T2,p_2);
%axis([-sqrt(mu1)-0.05 -sqrt(mu2)+0.05 0 max(p_11)]);
%pause;