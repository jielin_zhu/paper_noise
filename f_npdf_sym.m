% compute PDF for the symmetric case

function p_sym = f_npdf_sym(mu,T,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

n1 = length(T);
f1_sym = log(mu)/4;%-epsilon^(beta-2)*log(4)
%exp_psi0_sym = zeros(1,n1);
psi0_sym = zeros(1,n1);
psi1_sym = zeros(1,n1);
p_sym = zeros(1,n1);

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    psi1_sym(i) = epsilon^(beta-2)*(2*log((sqrt(mu)+T(i)))-log(mu));
    %exp_psi0_sym(i) = exp(psi0_sym(i)/epsilon^2);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+f1_sym);
end;

if isreal(psi1_sym)==0
    error('Something wrong with psi0_sym, doulbe check T_r');
end;
