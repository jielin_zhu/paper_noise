%compute psi_{1mu} for fully normalized temperature model

function psi_mu = f_npsi1_mu(T,mu,c1,c2)
%T, specific point we want to approximate psi_{1mu};
%mu, specific point we want to approximate psi_{1mu} and used to find root
%c1,c2 are coefficients for the fully normalized temperature model

root = f_n_root(mu,c1,c2); % three elements as we only consider the case A2,B2 not 0
R1 = root(1);
R2 = root(2);
R3 = root(3);

dR1 = -(1+c2*R1)/(3*c1*R1^2-2*R1+c2*mu);
dR2 = -(1+c2*R2)/(3*c1*R2^2-2*R2+c2*mu);
dR3 = -(1+c2*R3)/(3*c1*R3^2-2*R3+c2*mu);

%break psi_mu into different pieces
%separate cases for c1>0, c2<0 and other values
if ((c1>0) && (c2<=0));
    P1 = ((2+c2*(R1+R2))*(1/(T-R2)+1/R2)*(-dR2)-(2+c2*(R1+R3))*(-1/(T-R3)-1/R3)*dR3)/(c1*(R3-R2));
    P2 = (c2*(dR1+dR2)*(log(T-R2)-log(-R2))-c2*(dR1+dR3)*(log(R3-T)-log(R3)))/(c1*(R3-R2));
    if isreal(P2)==0
    error('Something wrong with P2');
    end;
    P3 = (dR3-dR2)*((2+c2*(R1+R2))*(log(T-R2)-log(-R2))-(2+c2*(R1+R3))*(log(R3-T)-log(R3)))/(-c1*(R3-R2)^2);
    if isreal(P3)==0
    error('Something wrong with P3');
    end;
else
    P1 = ((2+c2*(R1+R2))*(1/(T-R2)+1/R2)*(-dR2)-(2+c2*(R1+R3))*(1/(T-R3)+1/R3)*(-dR3))/(c1*(R3-R2));
    P2 = (c2*(dR1+dR2)*(log(T-R2)-log(-R2))-c2*(dR1+dR3)*(log(T-R3)-log(-R3)))/(c1*(R3-R2));
    P3 = (dR3-dR2)*((2+c2*(R1+R2))*(log(T-R2)-log(-R2))-(2+c2*(R1+R3))*(log(T-R3)-log(-R3)))/(-c1*(R3-R2)^2);
end;

%Particularly for the case c1<0 and c2>0
%root = f_n_root(mu,c1,c2); % three elements as we only consider the case A2,B2 not 0
%T1 = root(1);
%T2 = -root(2);
%T3 = -root(3);
%dT1 = -(1+c2*T1)/(3*c1*T1^2-2*T1+c2*mu);
%dT2 = (1-c2*T2)/(3*c1*T2^2+2*T2+c2*mu);
%dT3 = (1-c2*T3)/(3*c1*T3^2+2*T3+c2*mu);
%break psi_mu into different pieces
%P1 = ((2+c2*(T1-T2))*(1/(T+T2)-1/T2)*dT2-(2+c2*(T1-T3))*(1/(T+T3)-1/T3)*dT3)/(c1*(T2-T3));
%P2 = (c2*(dT1-dT2)*(log(T+T2)-log(T2))-c2*(dT1-dT3)*(log(T+T3)-log(T3)))/(c1*(T2-T3));
%P3 = (dT2-dT3)*((2+c2*(T1-T2))*(log(T+T2)-log(T2))-(2+c2*(T1-T3))*(log(T+T3)-log(T3)))/(-c1*(T2-T3)^2);

%P1 = (-1/(32*A2*(T2-T3)^2))*(dT2-dT3)*((B1+2*B2*(T1-T3))*(log(T+T3)-log(T3))-(B1+2*B2*(T1-T2))*(log(T+T2)-log(T2)));
%P2 = (1/32*A2*(T2-T3))*(2*B2*(dT1-dT3)*(log(T+T3)-log(T3))+(B1+2*B2*(T1-T3))*(dT3/(T+T3)-dT3/T3)-2*B2*(dT1-dT2)*(log(T+T2)-log(T2))-(B1+2*B2*(T1-T2))*(dT2/(T+T2)-dT2/T2));
psi_mu = P1+P2+P3;
