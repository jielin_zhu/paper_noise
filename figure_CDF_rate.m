%load files for pdf of full asymmetric symmetric system with different
%drifting rates
%generate the figure to compare the CDF approximation above T2 in the full
%system based on matching asymptotics, numerical simulation with different
%drifting rates

%{
%load merged pdf based on full leading order approximation with
%epsilon=0.05 and beta=2.5
load file_PDF_asym_merge3.dat
[m1,n1] = size(file_PDF_asym_merge3);
T_asym = file_PDF_asym_merge3(1:m1-1,1);
mu_asym = file_PDF_asym_merge3(end,2:end);
pdf_asym_merge = file_PDF_asym_merge3(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num3.dat
pdf_asym_num = file_PDF_asym_num3(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.1 and
%beta=2.5
load file_PDF_asym_merge5.dat
[m2,n2] = size(file_PDF_asym_merge5);
T_asym2 = file_PDF_asym_merge5(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge5(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge5(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num5.dat
pdf_asym_num2 = file_PDF_asym_num5(1:m2-1,2:end);
%}

%load merged pdf based on full leading order approximation with
%epsilon=0.05 and v=0.0005
%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T_asym = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu_asym = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];
load file_PDF_asym_num6.dat
pdf_asym_num = file_PDF_asym_num6(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.05 and
%beta=0.00025 by combining two files
load file_PDF_asym_merge8.dat
[m2_1,n2_1] = size(file_PDF_asym_merge8);
T_asym2 = file_PDF_asym_merge8(1:m2_1-1,1);
mu_asym2_1 = file_PDF_asym_merge8(end,2:end);
pdf_asym_merge2_1 = file_PDF_asym_merge8(1:m2_1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_merge8_2.dat
[m2_2,n2_2] = size(file_PDF_asym_merge8_2);
%T_asym2_2 = file_PDF_asym_merge8_2(1:m2_2-1,1);
mu_asym2_2 = file_PDF_asym_merge8_2(end,2:end);
pdf_asym_merge2_2 = file_PDF_asym_merge8_2(1:m2_2-1,2:end); %each column is pdf at mu(i)
mu_asym2 = [mu_asym2_1 mu_asym2_2];
size(mu_asym2)
pdf_asym_merge2 = [pdf_asym_merge2_1 pdf_asym_merge2_2];
load file_PDF_asym_num8.dat
[m3,n3] = size(file_PDF_asym_num8);
pdf_asym_num2 = file_PDF_asym_num8(1:m3-1,2:end);

%{
%show if the PDF agrees at each mu(i)
for i=2:length(mu_asym2);
    figure(1);
    plot(T_asym2,pdf_asym_merge2(:,i),'b-');hold on;
    plot(T_asym2,pdf_asym_num2(:,i),'r-.');hold on;
    %plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
    hold off;
    %axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu_asym2(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
%epsilon = 0.05;
%beta = 2.5;
%Ts = T2+10*epsilon;
%Td = (T2-2*epsilon).*ones(1,length(mu_asym));


%compute probability above T2 for small noise
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0,c1,c2);
    T2(i) = R(2);
end;
CDF_asym = 1;
CDF_asym_num = 1;
for i=1:length(mu_asym);
    index_T = min(find(T_asym>T2(i)));
    
    P_asym = trapz(T_asym(index_T:end),pdf_asym_merge(index_T:end,i));
    CDF_asym = [CDF_asym P_asym];
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
end;

%compute probability above T2 for small noise
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym2(i),c0,c1,c2);
    T2(i) = R(2);
end;
CDF_asym2 = 1;
CDF_asym_num2 = 1;
for i=1:length(mu_asym2);
    index_T = min(find(T_asym2>T2(i)));
    
    P_asym = trapz(T_asym2(index_T:end),pdf_asym_merge2(index_T:end,i));
    CDF_asym2 = [CDF_asym2 P_asym];
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,i));
    CDF_asym_num2 = [CDF_asym_num2 P_asym_num];
end;

Fig = figure;
plot(mu_asym,CDF_asym(2:end),'b-','linewidth',2);hold on;
plot(mu_asym,CDF_asym_num(2:end),'k*--','linewidth',1.5);hold on;
plot(mu_asym2,CDF_asym2(2:end),'r-.','linewidth',2);hold on;
plot(mu_asym2,CDF_asym_num2(2:end),'k--','linewidth',1.5);
xlabel('$$\mu$$','FontSize',40,'interpreter','latex');
ylabel('$$Prob(T>-T_2)$$','FontSize',32,'interpreter','latex');
axis([0 0.05 0.3 1.1]);
set(gca,'fontsize',18);
print(Fig,'-depsc','figure_CDF_rate.eps')