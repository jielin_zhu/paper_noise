% compute PDF for the symmetric case and focus on the range of T near the
% unstable equilibrium

function p_sym = f_npdf_sym_neg(mu,T,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

n1 = length(T);
%f1_sym = log(mu)/4;
%f2_sym = epsilon^2*(-5*mu^(-3/2)/96);
%f3_sym = epsilon^beta*(3/16*mu^(-3/2));
%f4_sym = epsilon^(2*beta-2)*(-mu^(-3/2)/8);

psi0_sym = zeros(1,n1);
%psi1_sym = zeros(1,n1);
%psi2_sym = zeros(1,n1);
%psi3_sym = zeros(1,n1);
p_sym = zeros(1,n1);
%p_norm = zeros(4,n1); %check the shape change from psi_j

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    %psi1_sym(i) = epsilon^(beta-2)*(2*log(sqrt(mu)+T(i))-log(4*mu));
    %psi2_sym(i) = epsilon^beta*(-1/(T(i)+sqrt(mu))-sqrt(mu)/(T(i)+sqrt(mu))^2)/4/mu+epsilon^beta*3*mu^(-3/2)/16;
    %psi3_sym(i) = epsilon^(2*beta-2)*(1/mu/(T(i)+sqrt(mu))+sqrt(mu)/mu/2/(T(i)+sqrt(mu))^2-5*mu^(-3/2)/8);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2);%+psi1_sym(i)+psi2_sym(i)+psi3_sym(i)+f1_sym+f2_sym+f3_sym+f4_sym);
    %p_norm(1,i) = exp(psi0_sym(i)/epsilon^2);
    %p_norm(2,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i));
    %p_norm(3,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i));
    %p_norm(4,i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi2_sym(i)+psi3_sym(i));
end;

%if isreal(psi1_sym)==0
    %error('Something wrong with psi0_sym, doulbe check T_r');
%end;

figure;
plot(T,psi0_sym);grid on;pause;