%Create figure that compares
%asympperic saddle-node bifurcation from zero-dimensional temperature model
%and related symmetric saddle-node bifurcation diagram with 

%call the parameter value
a_t = 2:(-0.001):0;
m = length(a_t);

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%compute equilibirum of the normalized temperature asymmetric system
T = zeros(2,m);
for i=1:m;
    R = f_n_fullroot(a_t(i),c0,c1,c2);
    %pause;
    T(1,i) = R(1);
    T(2,i) = R(2);
    %T(1,i) = R(3);
end;

%compute equilibrium of the system with symmetric branch by making T =
%sqrt(B1*mu/96/A)
T1 = zeros(2,m);
for i=1:m;
    T1(1,i) = sqrt(a_t(i));
    T1(2,i) = -sqrt(a_t(i));
end;

Fig = figure;
plot(a_t,T1(1,:),'Color','r','LineWidth',2,'LineStyle','-.');
hold on;plot(a_t,T1(2,:),'Color','r','LineWidth',2,'LineStyle',':');
hold on;plot(a_t,T(1,:),'Color','k','LineWidth',2);hold on;plot(a_t,T(2,:),'Color','k','LineStyle','--','LineWidth',2);
%axis([0 2 -2 3]);
axis([0 0.04 -0.3 0.3]);
%title(['Bifurcation digram for ','$$\mu$$'],'interpreter','latex')
xlabel('$$ \mu$$','fontsize',20,'interpreter','latex')
ylabel('$$ T $$','fontsize',20,'interpreter','latex')
%print(Fig,'-depsc','figure_bif_compare.eps')