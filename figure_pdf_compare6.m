%Generate PDF curve for the fully-normalized model for symmetric &
%asymmetric case based on asymptotic approximation, gaussian approximation,
%numerical simulations for relatively small mu and relatively large beta

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.005;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.02;%3, size of the additional noise;
beta = 2.4;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.01;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 10000;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for mu_n for symetric and asymmetric case
root = f_n_root(mu_n,c1,c2);
T_r = root(2)+0.001:0.001:2; %define the domain of PDF for every different mu(i)
T_r_sym = -sqrt(mu_n)+0.001:0.001:2; %the domain of the PDF for the symmetric case
p_a = f_a_npdf(mu_n,T_r,c1,c2,epsilon,beta); 
p_sym = f_npdf_sym(mu_n,T_r_sym,epsilon,beta);
%[p_a p_sym] = f_a_npdf_compare(mu_n,T_r,c1,c2,epsilon,beta); 
%int_p = trapz(T_r,p_a);
Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case

%Find the proper normalization constant for asymmetric case, which is the
%integral of p_sym for mu just before early tipping happens
root0 = f_n_root(mu_0,c1,c2);
T_r0 = root0(2)+0.001:0.001:2;
p_0 = f_a_npdf(mu_0,T_r0,c1,c2,epsilon,beta);
int_p = trapz(T_r0,p_0);

%compare the CDF curve with histogram from numerical simulations
mu_1 = 0.1:(-v*dt):mu_n;
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
dC = 1/200;
C = T_min:dC:2;
T_hist = hist(h,C)./loop./dC;

%calculate pdf based on gaussian approximation
p_gau = f_gau_pdf(mu_1,v,dt,T_r,epsilon);

%also sketch the unstable boundary for our approximation
Y = 0:0.1:15;
l = length(Y);
L = ones(1,l).*root(2);

%generate figure result in .eps form
Fig = figure;
plot(C,T_hist,'Marker','o','Color','k');
hold on;plot(T_r,p_a/int_p,'r-','Linewidth',2);
hold on;plot(T_r_sym,p_sym*Con,'b-','Linewidth',2);
hold on;plot(T_r,p_gau,'g-.','Linewidth',2);
hold on;plot(L,Y,'r:','Linewidth',2);
axis([-0.1 0.2 0 13]);
xlabel('$$T$$','fontsize',20,'interpreter','latex');
ylabel('$$p(T,\mu)$$','fontsize',20,'interpreter','latex');
title(['(d): $$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$v$$= ',num2str(V),'; $$\mu$$= ',num2str(mu_n)],'fontsize',15,'interpreter','latex')
%print(Fig,'-depsc','figure_pdf_compare6.eps')