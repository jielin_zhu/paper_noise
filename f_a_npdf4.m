% compute PDF for case A for the fully normalized model and over the whole
% range of T
% use the full potential for the leading order approximation

function p = f_a_npdf4(mu,T,c0,c1,c2,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

F = f_nf0(mu,c1,c2);
%if isreal(F)==0 %check the validity of f0
    %error('Something wrong with f_nf0.m');
%end;

n1 = length(T);
psi_0 = zeros(1,n1);
psi_11 = zeros(1,n1);
[f11 f12 f13] = f_nf1(mu,c1,c2,epsilon,beta);
f_1 = f11+f12+f13;
if isreal(f_1)==0 %check the validity for f1
    error('Something wrong with f_nf1.m');
end;

exp_psi = zeros(1,n1);
R = f_n_root(mu,c1,c2);
index_Tl = min(find(T>R(2)));
for i=index_Tl:n1;
    %psi_0(i) = -2*T(i)^3/3+2*mu*T(i)+c1*T(i)^4/2+c2*mu*T(i)^2;
    psi_0(i) = -2*T(i)^3/3+2*mu*T(i)+c1*T(i)^4/2+c2*mu*T(i)^2+2*c0*T(i)^5/5; %including c0*T^4 term
    psi_11(i) = f_npsi11(T(i),mu,c1,c2);
    exp_psi(i) = exp((psi_0(i)+F)/epsilon^2+psi_11(i)*epsilon^(beta-2)+f_1);%to avoid large value in exp_psi0, we need to combine psi and f first before divided by epsilon^2
end;

%check the validity of psi_1;
%if isreal(psi_11)==0
    %error('Something wrong with f_npsi11.m');
%end;

%p = exp_psi.*exp(F/epsilon^2).*exp(f_1);
p = exp_psi;

%check the validity of p and p_sym
if isreal(psi_11)==0
    error('Something wrong with f_npsi11.m');
end;
