%find the matching coefficient for brownian motion locally near -sqrt(mu)
%and asymptotically
function C = f_npdf_sym_local_C(mu1,mu2,T,epsilon,beta)
%unlike other functions, T here is a point instead of a series

v = epsilon^beta;
time = (mu1-mu2)/v;
dT = 0.001;
T_total = -5:dT:1; %to compute the pdf for brownian motion

%compute pdf based on asymptotic approximation at mu_n
f1_sym = log(mu2)/4;
f2_sym = epsilon^2*(-5*mu2^(-3/2)/96);
f3_sym = epsilon^beta*(3/16*mu2^(-3/2));
f4_sym = epsilon^(2*beta-2)*(-mu2^(-3/2)/8);
psi0_sym = 2*mu2*T-2*T^3/3-4*mu2^(3/2)/3; %check consistence with symmetric case
psi1_sym = epsilon^(beta-2)*(2*log(sqrt(mu2)+T)-log(4*mu2));
psi2_sym = epsilon^beta*(-1/(T+sqrt(mu2))-sqrt(mu2)/(T+sqrt(mu2))^2)/4/mu2+epsilon^beta*3*mu2^(-3/2)/16;
psi3_sym = epsilon^(2*beta-2)*(1/mu2/(T+sqrt(mu2))+sqrt(mu2)/mu2/2/(T+sqrt(mu2))^2-5*mu2^(-3/2)/8);
p_asym = exp(psi0_sym/epsilon^2+psi1_sym+psi2_sym+psi3_sym+f1_sym+f2_sym+f3_sym+f4_sym);
%size(p_asym)
%pause;

%compute the pdf based on brownian motion, initial is given by asymptotic
%approximation
p_0 = zeros(length(T_total),1);
p_local = 0;
f1_sym = log(mu1)/4;
f2_sym = epsilon^2*(-5*mu1^(-3/2)/96);
f3_sym = epsilon^beta*(3/16*mu1^(-3/2));
f4_sym = epsilon^(2*beta-2)*(-mu1^(-3/2)/8);
for i=1:length(T_total);
    if T_total(i)<=-sqrt(mu1);
        p_0(i) = 0;
    else
        psi0_sym = 2*mu1*T_total(i)-2*T_total(i)^3/3-4*mu1^(3/2)/3; %check consistence with symmetric case
        psi1_sym = epsilon^(beta-2)*(2*log(sqrt(mu1)+T_total(i))-log(4*mu1));
        psi2_sym = epsilon^beta*(-1/(T_total(i)+sqrt(mu1))-sqrt(mu1)/(T_total(i)+sqrt(mu1))^2)/4/mu1+epsilon^beta*3*mu1^(-3/2)/16;
        psi3_sym = epsilon^(2*beta-2)*(1/mu1/(T_total(i)+sqrt(mu1))+sqrt(mu1)/mu1/2/(T_total(i)+sqrt(mu1))^2-5*mu1^(-3/2)/8);
        p_0(i) = exp(psi0_sym/epsilon^2+psi1_sym+psi2_sym+psi3_sym+f1_sym+f2_sym+f3_sym+f4_sym);
    end;
    p_tran = exp(-(T-T_total(i))^2/2/time/epsilon^2)/sqrt(2*time*pi)/epsilon*dT;
    p_local = p_local+p_0(i)*p_tran;
end;
%size(p_local)
%pause;

%p_asym
%p_local
%pause;
C = p_asym/p_local;