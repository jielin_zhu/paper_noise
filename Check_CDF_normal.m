%Generate PDF for a series of mu, but use the normalization constant as the
%integration of p just before the PDF becomes positive at T_r(1);

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = 1;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = -9;%2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.005;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.02;%3, size of the additional noise;
beta = 2.2;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
dt = 0.1;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 10000;
dC = 1/100; %length of bin for histgram;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for each mu(i) and  CDF from T_r
%Root = zeros(2,n);
%Root_sym = zeros(2,n);
%p_0 = f_a_pdf_test_3(mu_0,mu_0,T_r_0,A,B1,epsilon,beta);
%A_0 = trapz(T_r_0,p_0);
int_a = zeros(1,n);
int_sym = zeros(1,n); %cdf for symmetric case
%F1 = zeros(1,n); %check if it's 0 for the symmetric case
%F1_sym = zeros(1,n);
%F0 = zeros(1,n);
%F0_sym = zeros(1,n);
%F11 = zeros(1,n);
%F12 = zeros(1,n);
%F13 = zeros(1,n);
%Peak = zeros(1,n);
%Peak_sym = zeros(1,n);
%sim_cdf = zeros(1,n);
%int_gau = zeros(1,n);%cdf for gaussian approximation
%N = 1; %count the index of int_a when p_a(T_r(1)) becomes positive
for i=1:n;
    %mu(i)
    root = f_n_root(mu(i),c1,c2);
    %pause;
    %Root(1,i) = root(1);
    %Root(2,i) = root(2);
    %Root_sym(1,i) = sqrt(mu(i));
    %Root_sym(2,i) = -sqrt(mu(i));
    %root(1)
    %T_r = root(2)+0.001:0.0005:2; %define the domain of PDF for every different mu(i)
    T_r = root(2)+0.001:0.0005:root(3)-0.001; %specificly for c1>0 and c2<0
    T_r_sym = -sqrt(mu(i))+0.001:0.001:2; %the domain of the PDF for the symmetric case
    
    %m = length(T_r);
    %psi = zeros(1,m);
    %psi_sym = zeros(1,m);
    %F0(i) = f_nf0(mu(i),c1,c2);
    %for j=1:m;
        %psi(j) = (-2*T_r(j)^3/3+2*mu(i)*T_r(j)+c1*T_r(j)^4/2+c2*mu(i)*T_r(j)^2+F0)/epsilon^2;
        %psi_sym(j) = (2*mu(i)*T_r(j)-2*T_r(j)^3/3-4*mu(i)^(3/2)/3)/epsilon^2;
    %end;
    %Peak(i) = F0/epsilon^2;
    %Peak_sym(i) = -4*mu(i)^(3/2)/3/epsilon^2;
    %F0_sym(i) = -4*mu(i)^(3/2)/3;
    
    p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta); 
    %[p_a p_sym] = f_a_npdf_compare(mu(i),T_r,c1,c2,epsilon,beta);
    %p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta);
    p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    %[f1 f2 f3] = f_nf1(mu(i),c1,c2,epsilon,beta);
    %[p_b p_sym1] = f_b_npdf_compare(mu(i),T_r,c1,c2,epsilon,beta);
    %p_a(1)
    %pause;
    %figure;plot(T_r,p_a);grid on;
    %pause;
    %if p_a(1)<10^(-5);
        %N = N+1;
    %end;
    int_a(i) = trapz(T_r,p_a);
    int_sym(i) = trapz(T_r_sym,p_sym);
    %F1(i) = exp(f1+f2+f3);
    %F11(i) = exp(f1);
    %F12(i) = exp(f2);
    %F13(i) = exp(f3);
    %F1_sym(i) = exp(log(mu(i))/4);
    %figure(3);plot(T_r,p_a,'b-',T_r_sym,p_sym,'r-.');hold on;pause;
    %figure(4);plot(T_r,p_a/int_a(1),'b-',T_r_sym,p_sym/int_sym(1),'r-.');hold on;pause;
    
    %compute the CDF by Gaussian approximation
    %p_gau = f_gau_pdf(mu,v,dt,T_r,epsilon);
    %int_gau(i) = trapz(T_r,p_gau);
    
    %compare the CDF curve with histogram from numerical simulations
    %mu_1 = 0.5:(-v*dt):mu(i);
    %m = length(mu_1);
    %T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
    %[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
    %sim_cdf(i) = 1-NUM/loop;
    %C = T_min:dC:2;
    %T_hist = hist(h,C)./loop./dC;
    %plot(C,T_hist,'ro');
    %axis([0 2 0 max(p_a./int_a(i))]);
    %pause;
end;

%N
%pause;

%CDF after normalization
%INT_a = zeros(1,n);
%for i=1:n;
    %if i<=N;
        %INT_a(i) = 1;
    %else
        %INT_a(i) = int_a(i)/int_a(N);
    %end;
%end;

Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case

figure(1);
%plot(mu,int_a-int_sym,'k-','Linewidth',2);grid on;
plot(mu,int_a,'b-',mu,int_sym,'r-.','LineWidth',2);grid on;
%plot(mu,INT_a,'b-',mu,int_sym./int_sym(1),'r-.','LineWidth',2);grid on;
%hold on;plot(mu,sim_cdf,'k:','LineWidth',2);
%hold on;plot(mu,int_gau,'g:','LineWidth',2);
%axis([0 0.05 0.9 1.1])
%title(['$$\epsilon = $$',num2str(epsilon),'; $$\beta =$$ ',num2str(beta)],'fontsize',20,'interpreter','latex')
%print(Fig,'-depsc','figure_cdf_compare3.eps')
figure(2);
plot(T_r,p_a/int_a(1),'b-',T_r_sym,p_sym*Con,'r-.','Linewidth',2);grid on;
%axis([-0.2 0.2 0 6]);
figure(4);
plot(mu,int_a/int_a(1),'b-',mu,int_sym*Con,'r-.','LineWidth',2);grid on;hold on;

%Compare f_0, f_1 for symmetric and asymmetric case
%figure(2);
%plot(mu,F1,'b-',mu,F1_sym,'r-.','Linewidth',2);
%plot(mu,F0,'b-',mu,F0_sym,'r-.','Linewidth',2);
%grid on;
%figure(3);
%plot(mu,F0-F0_sym,'k-','Linewidth',2);grid on;
%figure(4);
%plot(mu,F1,'b-',mu,F1_sym,'r-.','Linewidth',2);hold on;
%plot(mu,F11,'k-',mu,F12,'k-.',mu,F13,'k:','Linewidth',2);
%figure(5);
%plot(mu,F1-F1_sym,'k-','Linewidth',2);grid on;

%check the difference of the stable equilibrium
%figure(2);
%plot(mu,Root(1,:),'b-',mu,Root_sym(1,:),'r-.','Linewidth',2);
%hold on;plot(mu,Root(2,:),'b-',mu,Root_sym(2,:),'r-.','Linewidth',2);grid on;
%figure(3);
%plot(mu,Root(1,:)-Root_sym(1,:),'k-','Linewidth',2);
%grid on;

%check the difference of psi0+f0;
%figure(2);
%plot(mu,Peak,'b-',mu,Peak_sym,'r-.','Linewidth',2);grid on;
%hold on;plot(mu,Root(2,:),'b-',mu,Root_sym(2,:),'r-.','Linewidth',2);grid on;
%figure(3);
%plot(mu,Peak-Peak_sym,'k-','Linewidth',2);
%grid on;