%compare skewness and variance between the original case and larger noise
%output the results under the original variable \bar{T} and \bar{\mu}

%load merged pdf based on full leading order approximation with
%epsilon=0.05 and v=0.0005
%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T_asym = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu_asym = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];
load file_PDF_asym_num6.dat
pdf_asym_num = file_PDF_asym_num6(1:m1-1,2:end);

%{
%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_asym_merge7.dat
[m2,n2] = size(file_PDF_asym_merge7);
T_asym2 = file_PDF_asym_merge7(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge7(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge7(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num7.dat
pdf_asym_num2 = file_PDF_asym_num7(1:m2-1,2:end);
%}
load file_PDF_sym_merge2.dat
[m2,n2] = size(file_PDF_sym_merge2);
T_sym = file_PDF_sym_merge2(1:m2-1,1);
mu_sym = file_PDF_sym_merge2(end,2:end);
pdf_sym_merge = file_PDF_sym_merge2(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_sym_num2.dat
pdf_sym_num = file_PDF_sym_num2(1:m2-1,2:end);
T_asym2 = T_sym;
mu_asym2 = mu_sym;
pdf_asym_merge2 = pdf_sym_merge;
pdf_asym_num2 = pdf_sym_num;

%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_oppo_merge8.dat
[m2,n2] = size(file_PDF_oppo_merge8);
T_asym3 = file_PDF_oppo_merge8(1:m2-1,1);
mu_asym3 = file_PDF_oppo_merge8(end,2:end);
pdf_asym_merge3 = file_PDF_oppo_merge8(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_oppo_num.dat
pdf_asym_num3 = file_PDF_oppo_num(1:m2-1,2:end);

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);

l1 = length(mu_asym);
SKN_asym_full = zeros(1,l1);
VAR_asym_full = zeros(1,l1);
SKN_asym_full_num = zeros(1,l1);
SKN_asym_full_num2 = zeros(1,l1);
VAR_asym_full_num = zeros(1,l1);
To_asym = (T_asym+1).*T_e;
P = zeros(1,l1);
for i=1:l1;
    %need to renormalize the pdf based on To
    %trapz(T_asym,pdf_asym_merge(:,i))  
    %C_asym = trapz(T_asym,pdf_asym_merge(:,i))/trapz(To_asym,pdf_asym_merge(:,i));
    %trapz(To_asym,pdf_asym_merge(:,i).*C_asym)
    %C_num = trapz(T_asym,pdf_asym_num(:,i))/trapz(To_asym,pdf_asym_num(:,i));
    %pause;
    [m,var,skew] = f_skewness(To_asym,pdf_asym_merge(:,i)./T_e);
    SKN_asym_full(i) = skew;
    VAR_asym_full(i) = var;
    [m,var,skew] = f_skewness(To_asym,pdf_asym_num(:,i)./T_e);
    SKN_asym_full_num(i) = skew;
    VAR_asym_full_num(i) = var;
    [m,var,skew] = f_skewness(T_asym,pdf_asym_num(:,i));
    SKN_asym_full_num2(i) = skew;
    P(i) = trapz(To_asym,pdf_asym_merge(:,i)./T_e);
end;
index_P = min(find(P<1));
mu_escape = mu_asym(index_P);

l2 = length(mu_asym2);
SKN_asym_noise = zeros(1,l2);
VAR_asym_noise = zeros(1,l2);
SKN_asym_noise_num = zeros(1,l2);
SKN_asym_noise_num2 = zeros(1,l2);
VAR_asym_noise_num = zeros(1,l2);
To_asym2 = (T_asym2+1).*T_e;
P2 = zeros(1,l2);
for i=1:l2;
    %need to renormalize the pdf based on To
    %C_asym = trapz(T_asym2,pdf_asym_merge2(:,i))/trapz(To_asym2,pdf_asym_merge2(:,i));
    %C_num = trapz(T_asym2,pdf_asym_num2(:,i))/trapz(To_asym2,pdf_asym_num2(:,i));
    [m,var,skew] = f_skewness(To_asym2,pdf_asym_merge2(:,i)./T_e);
    SKN_asym_noise(i) = skew;
    VAR_asym_noise(i) = var;
    [m,var,skew] = f_skewness(To_asym2,pdf_asym_num2(:,i)./T_e);
    SKN_asym_noise_num(i) = skew;
    VAR_asym_noise_num(i) = var;
    [m,var,skew] = f_skewness(T_asym2,pdf_asym_num2(:,i));
    SKN_asym_noise_num2(i) = skew;
    P2(i) = trapz(To_asym2,pdf_asym_merge2(:,i)./T_e);
end;
index_P2 = min(find(P2<1));
mu_escape2 = mu_asym2(index_P2);

l3 = length(mu_asym3);
SKN_asym_oppo = zeros(1,l3);
VAR_asym_oppo = zeros(1,l3);
SKN_asym_oppo_num = zeros(1,l3);
SKN_asym_oppo_num2 = zeros(1,l3);
VAR_asym_oppo_num = zeros(1,l3);
To_asym3 = (T_asym3+1).*T_e;
P3 = zeros(1,l3);
for i=1:l3;
    %need to renormalize the pdf based on To
    %C_asym = trapz(T_asym3,pdf_asym_merge3(:,i))/trapz(To_asym3,pdf_asym_merge3(:,i));
    %C_num = trapz(T_asym3,pdf_asym_num3(:,i))/trapz(To_asym3,pdf_asym_num3(:,i));
    [m,var,skew] = f_skewness(To_asym3,pdf_asym_merge3(:,i)./T_e);
    SKN_asym_oppo(i) = skew;
    VAR_asym_oppo(i) = var;
    [m,var,skew] = f_skewness(To_asym3,pdf_asym_num3(:,i)./T_e);
    SKN_asym_oppo_num(i) = skew;
    VAR_asym_oppo_num(i) = var;
    [m,var,skew] = f_skewness(T_asym3,pdf_asym_num3(:,i));
    SKN_asym_oppo_num2(i) = skew;
    P3(i) = trapz(To_asym3,pdf_asym_merge3(:,i)./T_e);
end;
index_P3 = min(find(P3<1));
mu_escape3 = mu_asym3(index_P3);
Y = -1:0.01:0;

Fig = figure;
plot(mu_asym.*8/3/A/B1+mu_e,SKN_asym_full,'b-','LineWidth',2);
%hold on;plot(mu_asym,SKN_asym_full_num,'k*--','Linewidth',2);
hold on;plot(mu_asym2.*8/3/A/B1+mu_e,SKN_asym_noise,'r-.','Linewidth',2);
%hold on;plot(mu_asym2,SKN_asym_noise_num,'k--','Linewidth',2);
hold on;plot(mu_asym3.*8/3/A/B1+mu_e,SKN_asym_oppo,'m--','Linewidth',2);
hold on;plot((mu_escape*8/3/A/B1+mu_e).*ones(size(Y)),Y,'b:','linewidth',2);
hold on;plot((mu_escape2*8/3/A/B1+mu_e).*ones(size(Y)),Y,'r:','linewidth',2);
hold on;plot((mu_escape3*8/3/A/B1+mu_e).*ones(size(Y)),Y,'m:','linewidth',2);
xlabel('$$\bar{\mu}$$','fontsize',40,'interpreter','latex');
ylabel('Skewness $$\gamma$$','fontsize',40,'interpreter','latex')
legend('(I)','(II)','(III)','location','southeast');
%title(['$$\epsilon$$ = ',num2str(epsilon), '; $$\beta$$ = ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
axis([1.2 1.6 -0.8 0]);
set(gca,'fontsize',18);
print(Fig,'-depsc','figure_skewness_asymmetry_To.eps')

%only compare the numerical results for different case based on normalized
%data or original ones
figure;
plot(mu_asym.*8/3/A/B1+mu_e,SKN_asym_full_num,'k-','Linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,SKN_asym_full_num2,'k--','Linewidth',2);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,SKN_asym_noise_num,'r-','Linewidth',2);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,SKN_asym_noise_num2,'r--','Linewidth',2);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,SKN_asym_oppo_num,'m-','Linewidth',2);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,SKN_asym_oppo_num2,'m--','Linewidth',2);
plot(mu_asym.*8/3/A/B1+mu_e,SKN_asym_full,'k.','LineWidth',2);
hold on;plot(mu_asym2.*8/3/A/B1+mu_e,SKN_asym_noise,'r.','Linewidth',2);
hold on;plot(mu_asym3.*8/3/A/B1+mu_e,SKN_asym_oppo,'m.','Linewidth',2);
[MIN,I_full1] = min(SKN_asym_full);
mu_full_min1 = mu_asym(I_full1)*8/3/A/B1+mu_e
[MIN,I_full2] = min(SKN_asym_full_num2);
mu_full_min2 = mu_asym(I_full2)*8/3/A/B1+mu_e
[MIN,I_noise1] = min(SKN_asym_noise);
mu_noise_min1 = mu_asym2(I_noise1)*8/3/A/B1+mu_e
[MIN,I_noise2] = min(SKN_asym_noise_num2);
mu_noise_min2 = mu_asym2(I_noise2)*8/3/A/B1+mu_e
[MIN,I_oppo1] = min(SKN_asym_oppo);
mu_oppo_min1 = mu_asym3(I_oppo1)*8/3/A/B1+mu_e
[MIN,I_oppo2] = min(SKN_asym_oppo_num2);
mu_oppo_min2 = mu_asym3(I_oppo2)*8/3/A/B1+mu_e
%axis([1.2 1.6 -0.8 0]);

for i=1:length(mu_sym);
    figure(7);plot(T_sym,pdf_asym_num2(:,i),'b-.');hold on;
    plot(T_sym,pdf_asym_merge2(:,i),'b-');hold off;
    title(['\mu=',num2str(mu_sym(i).*8/3/A/B1+mu_e)],'fontsize',10);
    pause;
end;
pause;

Fig2 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,VAR_asym_full,'b-','LineWidth',2);
%hold on;plot(mu_asym,SKN_asym_full_num,'k*--','Linewidth',2);
hold on;plot(mu_asym2.*8/3/A/B1+mu_e,VAR_asym_noise,'r-.','Linewidth',2);
%hold on;plot(mu_asym2,SKN_asym_noise_num,'k--','Linewidth',2);
hold on;plot(mu_asym3.*8/3/A/B1+mu_e,VAR_asym_oppo,'m--','Linewidth',2);
xlabel('$$\bar{\mu}$$','fontsize',40,'interpreter','latex');
ylabel('Variance $$\sigma^2$$','fontsize',40,'interpreter','latex');
axis([1.2 1.6 0 600]);
set(gca,'fontsize',18);
%print(Fig2,'-depsc','figure_variance_asymmetry_To.eps')