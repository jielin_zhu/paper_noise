%Generate PDF curve for the fully-normalized model for symmetric &
%asymmetric case based on asymptotic approximation, compared with leading
%order approximation

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.5;%1, normalized mu at the beginning;
mu_n = 0.03;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 3;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
dt = 0.01;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 10000;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for mu_n for symetric and asymmetric case
root = f_n_root(mu_n,c1,c2);
T_r = root(2)+0.001:0.001:2; %define the domain of PDF for every different mu(i)
[p_a p_sym] = f_a_npdf_compare(mu_n,T_r,c1,c2,epsilon,beta); 
int_p = trapz(T_r,p_a);
int_psym = trapz(T_r,p_sym);

%calculate leading order pdf for symmetric and asymmetric case
[p_a_LO p_sym_LO] = f_a_npdf_LO(mu_n,T_r,c1,c2,epsilon,beta); 
int_p_LO = trapz(T_r,p_a_LO);
int_psym_LO = trapz(T_r,p_sym_LO);

%generate figure result in .eps form
Fig1 = figure;
plot(T_r,p_a/int_p,'r-','Linewidth',2);
hold on;plot(T_r,p_a_LO/int_p_LO,'k-.','Linewidth',2);
axis([0 0.5 0 9]);
xlabel('$$T$$','fontsize',20,'interpreter','latex');
ylabel('$$p(T,\mu)$$','fontsize',20,'interpreter','latex');
title(['(d): $$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$\mu$$= ',num2str(mu_n)],'fontsize',20,'interpreter','latex')
print(Fig1,'-depsc','figure_pdf_LO_asym2.eps')

Fig2 = figure;
plot(T_r,p_sym/int_psym,'b-','Linewidth',2);
hold on;plot(T_r,p_sym_LO/int_psym_LO,'g-.','Linewidth',2);
axis([-0.1 0.5 0 7]);
xlabel('$$T$$','fontsize',20,'interpreter','latex');
ylabel('$$p(T,\mu)$$','fontsize',20,'interpreter','latex');
title(['(c): $$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$\mu$$= ',num2str(mu_n)],'fontsize',20,'interpreter','latex')
print(Fig2,'-depsc','figure_pdf_LO_sym2.eps')