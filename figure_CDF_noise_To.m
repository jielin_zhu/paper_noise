%load files for pdf of full asymmetric symmetric system with different
%amplitude of noise
%generate the figure to compare the CDF approximation above T2 in the full
%system based on matching asymptotics, numerical simulation with different
%parameter values
%output figures under the original variable \bar{T} and \bar{\mu}

%{
%load merged pdf based on full leading order approximation with
%epsilon=0.05 and beta=2.5
load file_PDF_asym_merge3.dat
[m1,n1] = size(file_PDF_asym_merge3);
T_asym = file_PDF_asym_merge3(1:m1-1,1);
mu_asym = file_PDF_asym_merge3(end,2:end);
pdf_asym_merge = file_PDF_asym_merge3(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num3.dat
pdf_asym_num = file_PDF_asym_num3(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.1 and
%beta=2.5
load file_PDF_asym_merge4.dat
[m2,n2] = size(file_PDF_asym_merge4);
T_asym2 = file_PDF_asym_merge4(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge4(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge4(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num4.dat
pdf_asym_num2 = file_PDF_asym_num4(1:m2-1,2:end);
%}

%load merged pdf based on full leading order approximation with
%epsilon=0.05 and v=0.0005
%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T_asym = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu_asym = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];
load file_PDF_asym_num6.dat
pdf_asym_num = file_PDF_asym_num6(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_asym_merge7.dat
[m2,n2] = size(file_PDF_asym_merge7);
T_asym2 = file_PDF_asym_merge7(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge7(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge7(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num7.dat
pdf_asym_num2 = file_PDF_asym_num7(1:m2-1,2:end);

%{
%show if the PDF agrees at each mu(i)
for i=2:length(mu_asym2);
    figure(1);
    plot(T_asym2,pdf_asym_merge2(:,i),'b-');hold on;
    plot(T_asym2,pdf_asym_num2(:,i),'r-.');hold on;
    %plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
    hold off;
    %axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu_asym2(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
%epsilon = 0.05;
%beta = 2.5;
%Ts = T2+10*epsilon;
%Td = (T2-2*epsilon).*ones(1,length(mu_asym));


%compute probability above T2 for small noise
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0,c1,c2);
    T2(i) = R(2);
end;
CDF_asym = 1;
CDF_asym_num = 1;
for i=1:length(mu_asym);
    index_T = min(find(T_asym>T2(i)));
    
    P_asym = trapz(T_asym(index_T:end),pdf_asym_merge(index_T:end,i));
    CDF_asym = [CDF_asym P_asym];
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
end;

%compute the skewness for the noise case
l1 = length(mu_asym);
SKN_asym_full = zeros(1,l1);
VAR_asym_full = zeros(1,l1);
SKN_asym_full_num = zeros(1,l1);
SKN_asym_full_num2 = zeros(1,l1);
VAR_asym_full_num = zeros(1,l1);
To_asym = (T_asym+1).*T_e;
P = zeros(1,l1);
for i=1:l1;
    %need to renormalize the pdf based on To
    %trapz(T_asym,pdf_asym_merge(:,i))  
    %C_asym = trapz(T_asym,pdf_asym_merge(:,i))/trapz(To_asym,pdf_asym_merge(:,i));
    %trapz(To_asym,pdf_asym_merge(:,i).*C_asym)
    %C_num = trapz(T_asym,pdf_asym_num(:,i))/trapz(To_asym,pdf_asym_num(:,i));
    %pause;
    [m,var,skew] = f_skewness(To_asym,pdf_asym_merge(:,i)./T_e);
    SKN_asym_full(i) = skew;
    VAR_asym_full(i) = var;
    [m,var,skew] = f_skewness(To_asym,pdf_asym_num(:,i)./T_e);
    SKN_asym_full_num(i) = skew;
    VAR_asym_full_num(i) = var;
    [m,var,skew] = f_skewness(T_asym,pdf_asym_num(:,i));
    SKN_asym_full_num2(i) = skew;
    P(i) = trapz(To_asym,pdf_asym_merge(:,i)./T_e);
end;
index_P = min(find(P<1));
mu_escape = mu_asym(index_P);

%compute probability above T2 for small noise
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym2(i),c0,c1,c2);
    T2(i) = R(2);
end;
CDF_asym2 = 1;
CDF_asym_num2 = 1;
for i=1:length(mu_asym2);
    index_T = min(find(T_asym2>T2(i)));
    
    P_asym = trapz(T_asym2(index_T:end),pdf_asym_merge2(index_T:end,i));
    CDF_asym2 = [CDF_asym2 P_asym];
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,i));
    CDF_asym_num2 = [CDF_asym_num2 P_asym_num];
end;

l2 = length(mu_asym2);
SKN_asym_noise = zeros(1,l2);
VAR_asym_noise = zeros(1,l2);
SKN_asym_noise_num = zeros(1,l2);
SKN_asym_noise_num2 = zeros(1,l2);
VAR_asym_noise_num = zeros(1,l2);
To_asym2 = (T_asym2+1).*T_e;
P2 = zeros(1,l2);
for i=1:l2;
    %need to renormalize the pdf based on To
    %C_asym = trapz(T_asym2,pdf_asym_merge2(:,i))/trapz(To_asym2,pdf_asym_merge2(:,i));
    %C_num = trapz(T_asym2,pdf_asym_num2(:,i))/trapz(To_asym2,pdf_asym_num2(:,i));
    [m,var,skew] = f_skewness(To_asym2,pdf_asym_merge2(:,i)./T_e);
    SKN_asym_noise(i) = skew;
    VAR_asym_noise(i) = var;
    [m,var,skew] = f_skewness(To_asym2,pdf_asym_num2(:,i)./T_e);
    SKN_asym_noise_num(i) = skew;
    VAR_asym_noise_num(i) = var;
    [m,var,skew] = f_skewness(T_asym2,pdf_asym_num2(:,i));
    SKN_asym_noise_num2(i) = skew;
    P2(i) = trapz(To_asym2,pdf_asym_merge2(:,i)./T_e);
end;
index_P2 = min(find(P2<1));
mu_escape2 = mu_asym2(index_P2);

Fig = figure;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym(2:end),'b-','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym_num(2:end),'k*--','linewidth',1.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,CDF_asym2(2:end),'r-.','linewidth',2);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,CDF_asym_num2(2:end),'k--','linewidth',1.5);
xlabel('$$\bar{\mu}$$','FontSize',40,'interpreter','latex');
ylabel('$$P(\bar{T}>\bar{T}_{unstable})$$','FontSize',32,'interpreter','latex');
axis([0*8/3/A/B1+mu_e 0.06*8/3/A/B1+mu_e CDF_asym2(end-1) 1.1]);
set(gca,'fontsize',18);
%print(Fig,'-depsc','figure_CDF_noise_To.eps')

Fig2 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,SKN_asym_full,'b-','LineWidth',2);
%hold on;plot(mu_asym,SKN_asym_full_num,'k*--','Linewidth',2);
hold on;plot(mu_asym2.*8/3/A/B1+mu_e,SKN_asym_noise,'r-.','Linewidth',2);
%hold on;plot(mu_asym2,SKN_asym_noise_num,'k--','Linewidth',2);
%hold on;plot(mu_asym3.*8/3/A/B1+mu_e,SKN_asym_oppo,'m--','Linewidth',2);
hold on;plot((mu_escape*8/3/A/B1+mu_e).*ones(size(Y)),Y,'b:','linewidth',2);
hold on;plot((mu_escape2*8/3/A/B1+mu_e).*ones(size(Y)),Y,'r:','linewidth',2);
%hold on;plot((mu_escape3*8/3/A/B1+mu_e).*ones(size(Y)),Y,'m:','linewidth',2);
xlabel('$$\bar{\mu}$$','fontsize',40,'interpreter','latex');
ylabel('Skewness $$\gamma$$','fontsize',40,'interpreter','latex')
%title(['$$\epsilon$$ = ',num2str(epsilon), '; $$\beta$$ = ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
axis([1.2 1.6 -0.8 0]);
set(gca,'fontsize',18);
%print(Fig,'-depsc','figure_skewness_asymmetry_To.eps')