%compare skewness and variance between the original case and larger noise

%load merged pdf based on full leading order approximation with
%epsilon=0.05 and v=0.0005
%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T_asym = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu_asym = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];
load file_PDF_asym_num6.dat
pdf_asym_num = file_PDF_asym_num6(1:m1-1,2:end);

%{
%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_asym_merge7.dat
[m2,n2] = size(file_PDF_asym_merge7);
T_asym2 = file_PDF_asym_merge7(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge7(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge7(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num7.dat
pdf_asym_num2 = file_PDF_asym_num7(1:m2-1,2:end);
%}
load file_PDF_sym_merge2.dat
[m2,n2] = size(file_PDF_sym_merge2);
T_sym = file_PDF_sym_merge2(1:m2-1,1);
mu_sym = file_PDF_sym_merge2(end,2:end);
pdf_sym_merge = file_PDF_sym_merge2(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_sym_num2.dat
pdf_sym_num = file_PDF_sym_num2(1:m2-1,2:end);
T_asym2 = T_sym;
mu_asym2 = mu_sym;
pdf_asym_merge2 = pdf_sym_merge;
pdf_asym_num2 = pdf_sym_num;

%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_oppo_merge8.dat
[m2,n2] = size(file_PDF_oppo_merge8);
T_asym3 = file_PDF_oppo_merge8(1:m2-1,1);
mu_asym3 = file_PDF_oppo_merge8(end,2:end);
pdf_asym_merge3 = file_PDF_oppo_merge8(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_oppo_num.dat
pdf_asym_num3 = file_PDF_oppo_num(1:m2-1,2:end);

l1 = length(mu_asym);
SKN_asym_full = zeros(1,l1);
VAR_asym_full = zeros(1,l1);
SKN_asym_full_num = zeros(1,l1);
VAR_asym_full_num = zeros(1,l1);
for i=1:l1;
    [m,var,skew] = f_skewness(T_asym,pdf_asym_merge(:,i));
    SKN_asym_full(i) = skew;
    VAR_asym_full(i) = var;
    [m,var,skew] = f_skewness(T_asym,pdf_asym_num(:,i));
    SKN_asym_full_num(i) = skew;
    VAR_asym_full_num(i) = var;
end;
l2 = length(mu_asym2);
SKN_asym_noise = zeros(1,l2);
VAR_asym_noise = zeros(1,l2);
SKN_asym_noise_num = zeros(1,l2);
VAR_asym_noise_num = zeros(1,l2);
for i=1:l2;
    [m,var,skew] = f_skewness(T_asym2,pdf_asym_merge2(:,i));
    SKN_asym_noise(i) = skew;
    VAR_asym_noise(i) = var;
    [m,var,skew] = f_skewness(T_asym2,pdf_asym_num2(:,i));
    SKN_asym_noise_num(i) = skew;
    VAR_asym_noise_num(i) = var;
end;
l3 = length(mu_asym3);
SKN_asym_oppo = zeros(1,l3);
VAR_asym_oppo = zeros(1,l3);
SKN_asym_oppo_num = zeros(1,l3);
VAR_asym_oppo_num = zeros(1,l3);
for i=1:l3;
    [m,var,skew] = f_skewness(T_asym3,pdf_asym_merge3(:,i));
    SKN_asym_oppo(i) = skew;
    VAR_asym_oppo(i) = var;
    [m,var,skew] = f_skewness(T_asym3,pdf_asym_num3(:,i));
    SKN_asym_oppo_num(i) = skew;
    VAR_asym_oppo_num(i) = var;
end;

Fig = figure;
plot(mu_asym,SKN_asym_full,'b-','LineWidth',2);
%hold on;plot(mu_asym,SKN_asym_full_num,'k*--','Linewidth',2);
hold on;plot(mu_asym2,SKN_asym_noise,'r-.','Linewidth',2);
%hold on;plot(mu_asym2,SKN_asym_noise_num,'k--','Linewidth',2);
hold on;plot(mu_asym3,SKN_asym_oppo,'m--','Linewidth',2);
xlabel('$$\mu$$','fontsize',40,'interpreter','latex');
ylabel('Skewness $$\gamma$$','fontsize',40,'interpreter','latex')
%title(['$$\epsilon$$ = ',num2str(epsilon), '; $$\beta$$ = ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
axis([0.005 0.04 -1 0]);
set(gca,'fontsize',18);
%print(Fig,'-depsc','figure_skewness_asymmetry.eps')

Fig2 = figure;
plot(mu_asym,VAR_asym_full,'b-','LineWidth',2);
%hold on;plot(mu_asym,SKN_asym_full_num,'k*--','Linewidth',2);
hold on;plot(mu_asym2,VAR_asym_noise,'r-.','Linewidth',2);
%hold on;plot(mu_asym2,SKN_asym_noise_num,'k--','Linewidth',2);
hold on;plot(mu_asym3,VAR_asym_oppo,'m--','Linewidth',2);
xlabel('$$\mu$$','fontsize',40,'interpreter','latex');
ylabel('Variance $$\sigma^2$$','fontsize',40,'interpreter','latex');
axis([0.005 0.04 0.001 0.008]);
set(gca,'fontsize',18);
%print(Fig2,'-depsc','figure_variance_asymmetry.eps')