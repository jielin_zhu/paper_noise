%estimate the quasi-stationary pdf for the symmetric system with Kramer's
%rate
%this is used only for the second stage of the two stage system in
%check_brownian.m

%introduce parameter values for the full system, at the end of the first
%stage, we assume no tipping happens
mu_0 = 0.03;
mu_n = 0.03;
epsilon = 0.04;
beta = 2.5;
mu_middle = 0.025;%epsilon^(4/3);
v = epsilon^beta;
dT = 0.001;
T_b = -sqrt(mu_middle)+dT;
T_b1 = -sqrt(mu_n)+dT;

%find the kramer's escape rate from mu_middle to mu_n
%time = (mu_middle-mu_n)/v
%r = mu_middle/pi*exp(-4*mu_middle^(3/2)/3/epsilon^2)
%P = time*r

%find the pdf based on potential well
%T_r = -sqrt(mu_middle):dT:1;
%p_LO_n = f_npdf_sym_LO(mu_middle,T_r,epsilon,beta);
%int_LO_n = trapz(T_r,p_LO_n);

%check the escape probability from numerical simulation for the full system
%and two-stage system
%generate the PDF numerically stop at mu_n
%loop = 50000;
%dC = 1/200; %length of bin for histgram;
%dt = 0.1;
%mu_1 = mu_0:(-v*dt):mu_n;
%m = length(mu_1);
%T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
%[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
%NUM counts how many simulations have the endpoint smaller than the
%unstable boundary
%C = T_min-dC:dC:2;
%T_hist = hist(h,C)./loop./dC;
%NUM

%generate the PDF numerically by using mu1 which stopps at mu_middle in the
%full system and mu2 which stopps at mu_n in the simple brownian motion
%mu1 = mu_0:(-v*dt):mu_middle;
%mu2 = mu1(length(mu1)):(-v*dt):mu_n;
%[h_check NUM_check] = f_conditional_hist2(loop,mu1,mu2,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
%T_check = hist(h_check,C)./loop./dC;
%NUM_check

%figure;
%plot(C,T_hist,'b-');hold on;
%plot(C,T_check,'k-');
%hold on;plot(T_r,p_LO_n/int_LO_n,'r-.');
%hold on;plot(T_r,p_LO_n/int_LO_n*(1-P),'r-');

%compare the kramer's rate with the pdf of the system with constant mu_0
%time = (mu_0-mu_n)/v
%r = mu_0/pi*exp(-4*mu_0^(3/2)/3/epsilon^2)
%P = time*r
%loop = 10000;
%dC = 1/200; %length of bin for histgram;
%dt = 0.1;
%mu_1 = mu_0:(-v*dt):mu_n;
%mu_2 = ones(1,length(mu_1)).*mu_0; %constant drifting rate
%T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
%[h,NUM] = f_ntem_histgram_count(loop,mu_2,dt,epsilon,-sqrt(mu_0),T_min,0.001,0);
%NUM

%fix mu_0 and compare P=r*t with numerical #of tipping for different time
%length
r = sqrt(mu_0)/pi*exp(-8*mu_0^(3/2)/3/epsilon^2)
time = 1:100:1001;
dt = 0.1;
loop = 10000;
T_min = -5;
P = zeros(1,length(time));
P_tran = zeros(1,length(time));
for i=1:length(time);
    P(i) = r*time(i);
    mu_2 = ones(1,time(i)/dt).*mu_0;
    [h,NUM] = f_ntem_histgram_count(loop,mu_2,dt,epsilon,-sqrt(mu_0),T_min,0.001,0);
    P_tran(i) = NUM/loop;
end;
%find slope of the escape number for numerical simulation
mean_time = mean(time);
mean_P = mean(P_tran);
r_num = sum((time-mean_time).*(P_tran-mean_P))/sum((time-mean_time).^2)
a_num = mean_P-r_num*mean_time;
P_reg = a_num+r_num.*time;
figure;
plot(time,P,'b-.');hold on;
plot(time,P_tran,'r-');hold on;
plot(time,P_reg,'k:');