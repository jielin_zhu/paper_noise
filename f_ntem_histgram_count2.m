%numerical simulation for normalized temperature 
%with initial T_0 as another parameter value

function [T_final,NUM] = f_ntem_histgram_count2(loop,mu,dt,epsilon,T_0,T_min,T_exit,c1,c2)
%return to series of T at mu_t
%A2,B2 represent the asymmetric term of the temperature model
%T_exit gives the smallest value of T to determine early escape
%T_min gives the value of unstable equilibrium at mu_n

r = f_n_root(mu(1),c1,c2);
%T_0 = f_T_cen(mu(1),A,B1);
%T_0 = r(1);
T_final = zeros(1,loop);
i1 = 1;
NUM = 0;
NUM_tipping = 0;
while (i1<loop+1)
    TE = f_ntemperature(mu,T_0,dt,epsilon,T_exit,c1,c2);
    n1 = length(TE);
    T_final(i1) = TE(n1);
    if TE(n1) <= T_min;
        NUM = NUM+1;
        if TE(n1) == T_exit;
            NUM_tipping = NUM_tipping+1;
        end;
    end;
    i1 = i1+1;
end;


NUM_tipping