% Draw potential well for small a_t
% generate figure figure_potential_l.eps

a = 0.3;

x = -2:0.01:2;
n = length(x);
U = zeros(1,n);
Y = -2.5:0.1:2.5;
m = length(Y);
L = zeros(1,m);

for i=1:n;
    U(i) = -a*x(i)+x(i)^3/3;
    %L(i) = sqrt(a);
end;

for i=1:m
    L(i) = sqrt(a);
end;

Fig = figure;plot(x,U,'LineWidth',2);hold on;plot(L,Y,'Color','r','LineWidth',2,'LineStyle',':');
axis([-2 2 -2 2]);
xlabel('T','FontSize',20);
%ylabel('U','FontSize',20);
title(['(b): $$\mu_{\tau}=$$ ',num2str(a)],'FontSize',20,'interpreter','latex');
print(Fig,'-depsc','figure_potential_l.eps')