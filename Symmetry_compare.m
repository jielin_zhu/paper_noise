%load files of PDF for different mu(i) and different symmetry

load file_PDF_qs_oppo.dat
[m1,n1] = size(file_PDF_qs_oppo);
T_oppo = file_PDF_qs_oppo(1:m1-1,1);
mu_oppo = file_PDF_qs_oppo(end,2:end);
pdf_qs_oppo = file_PDF_qs_oppo(1:m1-1,2:end);
load file_PDF_merge_oppo.dat
pdf_merge_oppo = file_PDF_merge_oppo(1:m1-1,2:end);
load file_PDF_num_oppo.dat
pdf_num_oppo = file_PDF_num_oppo(1:m1-1,2:end);
load file_PDF_qs_asym.dat
[m2,n2] = size(file_PDF_qs_asym);
T_asym = file_PDF_qs_asym(1:m2-1,1);
mu_asym = file_PDF_qs_asym(end,2:end);
pdf_qs_asym = file_PDF_qs_asym(1:m2-1,2:end);
load file_PDF_merge_asym.dat
pdf_merge_asym = file_PDF_merge_asym(1:m2-1,2:end);
load file_PDF_num_asym.dat
pdf_num_asym = file_PDF_num_asym(1:m2-1,2:end);
load file_PDF_qs_sym.dat
[m3,n3] = size(file_PDF_qs_sym);
T_sym = file_PDF_qs_sym(1:m3-1,1);
mu_sym = file_PDF_qs_sym(end,2:end);
pdf_qs_sym = file_PDF_qs_sym(1:m3-1,2:end);
load file_PDF_merge_sym.dat
pdf_merge_sym = file_PDF_merge_sym(1:m3-1,2:end);
load file_PDF_num_sym.dat
pdf_num_sym = file_PDF_num_sym(1:m3-1,2:end);

%{
%compare the merged pdf with original settings with changed parameters
load file_PDF_merge_asym2.dat
[m4,n4] = size(file_PDF_merge_asym2);
T_asym2 = file_PDF_merge_asym2(1:m4-1,1);
mu_asym2 = file_PDF_merge_asym2(end,2:end);
pdf_merge_asym2 = file_PDF_merge_asym2(1:m4-1,2:end);
%}

%{
%compare the merged pdf with original settings with different tau
load file_PDF_merge_asym4.dat
[m4,n4] = size(file_PDF_merge_asym4);
T_asym2 = file_PDF_merge_asym4(1:m4-1,1);
mu_asym2 = file_PDF_merge_asym4(end,2:end);
pdf_merge_asym2 = file_PDF_merge_asym4(1:m4-1,2:end);
%}

%compare the skewness and variance for numerical results with much smaller
%Tl
load file_PDF_num_asym4.dat
[m4,n4] = size(file_PDF_num_asym4);
pdf_num_asym2 = file_PDF_num_asym4(1:m4-1,2:end);
T_asym2 = file_PDF_num_asym4(1:m4-1,1);
load file_PDF_num_oppo2.dat
[m5,n5] = size(file_PDF_num_oppo2);
T_oppo2 = file_PDF_num_oppo2(1:m5-1,1);
pdf_num_oppo2 = file_PDF_num_oppo2(1:m5-1,2:end);

%{
size_1 = size(mu_asym)
size_2 = size(mu_asym2)
pause;
%}

%introduce parameter value T2 and Td for opposite-symmetry case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = 0;%-A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
beta = 2.2;
T2 = zeros(1,length(mu_oppo)); %need to be computed for each mu(i)
index_imag = find(mu_oppo<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_root(mu_oppo(i),c1,c2);
    T2(i) = R(2);
end;
Td = (T2-2*epsilon).*ones(1,length(mu_oppo));


%compare the value of skewness or variance based on different PDF
%approximation
SKN_asym = zeros(length(mu_asym),1);
SKN_asym_num = zeros(length(mu_asym),1);
SKN_asym_num2 = zeros(length(mu_asym),1);
SKN_oppo = zeros(length(mu_asym),1);
SKN_oppo_num = zeros(length(mu_asym),1);
SKN_oppo_num2 = zeros(length(mu_asym),1);
SKN_sym = zeros(length(mu_asym),1);
VAR_asym = zeros(length(mu_asym),1);
VAR_asym_num = zeros(length(mu_asym),1);
VAR_oppo = zeros(length(mu_asym),1);
VAR_oppo_num = zeros(length(mu_asym),1);
VAR_sym = zeros(length(mu_asym),1);
for i=1:length(mu_asym);
    [m,var,skew] = f_skewness(T_asym,pdf_merge_asym(:,i));
    SKN_asym(i) = skew;
    VAR_asym(i) = var;
    [m,var,skew] = f_skewness(T_asym,pdf_num_asym(:,i));
    SKN_asym_num(i) = skew;
    VAR_asym_num(i) = var;
    [m,var,skew] = f_skewness(T_asym2,pdf_num_asym2(:,i));
    SKN_asym_num2(i) = skew;
    [m,var,skew] = f_skewness(T_oppo,pdf_merge_oppo(:,i));
    SKN_oppo(i) = skew;
    VAR_oppo(i) = var;
    [m,var,skew] = f_skewness(T_oppo2,pdf_num_oppo2(:,i));
    SKN_oppo_num2(i) = skew;
    [m,var,skew] = f_skewness(T_oppo,pdf_num_oppo(:,i));
    SKN_oppo_num(i) = skew;
    VAR_oppo_num(i) = var;
    [m,var,skew] = f_skewness(T_sym,pdf_merge_sym(:,i));
    SKN_sym(i) = skew;
    VAR_sym(i) = var;
end;
figure(4);
plot(mu_asym,SKN_asym,'b-');hold on;
plot(mu_asym,SKN_oppo,'r-');hold on;
plot(mu_asym,SKN_sym,'m-');hold on;
%plot(mu_asym,SKN_asym_num,'k-.');hold on;
%plot(mu_asym,SKN_oppo_num,'k:');
%plot(mu_asym,SKN_asym_num2,'b-.');hold on;
%plot(mu_asym,SKN_oppo_num2,'r:');hold off;
axis([0 0.04 -1 1])
legend('original asymmetric','opposite asymmetric','symmetric');
figure(5);
plot(mu_asym,VAR_asym,'b-');hold on;
plot(mu_asym,VAR_oppo,'r-');hold on;
plot(mu_asym,VAR_sym,'m-');hold on;
plot(mu_asym,VAR_asym_num,'k-.');hold on;
plot(mu_asym,VAR_oppo_num,'k:');hold off;
axis([0 0.04 0 0.3])
legend('original asymmetric','opposite asymmetric','symmetric');
%}

%{
%only compare the merged pdf under different parameter settings
for i=3:2:length(mu_asym);
    figure(3);
    plot(T_asym,pdf_merge_asym(:,i),'b-');hold on;
    %plot(T_asym2,pdf_merge_asym2(:,i),'r-.');hold on;
    plot(T_asym2,pdf_merge_asym2(:,(i+1)/2),'r-.');hold on; %need to consider different size of tau
    plot(T_asym,pdf_num_asym(:,i),'k-.');hold on;
    plot(T2(i),0,'bo');hold on;plot(Td(i-1),0,'b^');hold on;plot(T2(i)+10*epsilon,0,'k*');hold on;plot(T2(i)+4*epsilon,0,'ko');hold on;plot(T2(i)+epsilon,0,'ro');hold on;
    if mu_asym(i)>0;
        plot(-sqrt(mu_asym(i)),0,'b*');
    end;
    hold off;
    title(['\mu = ',num2str(mu_asym(i))],'fontsize',14);
    pause;
end;
%}


%compare the pdf curve based on different bifurcation diagram at the same
%mu(i)
for i=2:length(mu_asym);
    figure(3);
    %plot(T_oppo,pdf_merge_oppo(:,i),'m-');hold on;
    plot(T_asym,pdf_merge_asym(:,i),'b-');hold on;
    %plot(T_sym,pdf_merge_sym(:,i),'r-');hold on;
    plot(T_asym2,pdf_num_asym2(:,i),'k-.');hold on;
    %plot(T_oppo2,pdf_num_oppo2(:,i),'k--');hold on;
    plot(T_sym,pdf_num_sym(:,i),'k:');hold on;
    plot(T2(i),0,'bo');hold on;plot(Td(i-1),0,'b^');hold on;
    if mu_asym(i)>0;
        plot(-sqrt(mu_asym(i)),0,'b*');hold on;
    else
        %compute the possible PDF near T3
        index_T = min(find(T_asym>0));
        norm_c = trapz(T_asym(index_T:end),pdf_merge_asym(index_T:end,i));
        T_neg = -2:0.001:0;
        X = f_a_npdf3(mu_asym(i),T_neg,c1,c2,epsilon,beta);
        int = trapz(T_neg,X);
        PDF_neg = (1-2*norm_c).*X./int;
        plot(T_neg,PDF_neg,'r--');
    end;
    hold off;
    title(['\mu = ',num2str(mu_asym(i))],'fontsize',14);
    legend('opposite asymmetric','original asymmetric','symmetric','location','northwest')
    pause;
end;
%}

%{
%generage CDF for T>-sqrt(mu) as the lower boundary for all three cases
CDF_oppo = 1;
CDF_oppo_num = 1;
CDF_asym = 1;
CDF_asym_num = 1;
CDF_sym = 1;
CDF_sym_num = 1;
for i=1:length(mu_asym);
    index_T = min(find(T_asym>-sqrt(mu_asym(i))));
    
    P_oppo = trapz(T_oppo(index_T:end),pdf_merge_oppo(index_T:end,i));
    CDF_oppo = [CDF_oppo P_oppo];
    P_oppo_num = trapz(T_oppo(index_T:end),pdf_num_oppo(index_T:end,i));
    CDF_oppo_num = [CDF_oppo_num P_oppo_num];
    
    P_asym = trapz(T_asym(index_T:end),pdf_merge_asym(index_T:end,i));
    CDF_asym = [CDF_asym P_asym];
    P_asym_num = trapz(T_asym(index_T:end),pdf_num_asym(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
    
    P_sym = trapz(T_sym(index_T:end),pdf_merge_sym(index_T:end,i));
    CDF_sym = [CDF_sym P_sym];
    P_sym_num = trapz(T_sym(index_T:end),pdf_num_sym(index_T:end,i));
    CDF_sym_num = [CDF_sym_num P_sym_num];
end;
figure(1);
plot(mu_asym,CDF_oppo(2:end),'mo');hold on;
plot(mu_asym,CDF_asym(2:end),'bo');hold on;
plot(mu_asym,CDF_sym(2:end),'ro');hold on;
plot(mu_asym,CDF_oppo_num(2:end),'m--');hold on;
plot(mu_asym,CDF_asym_num(2:end),'b--');hold on;
plot(mu_asym,CDF_sym_num(2:end),'r--');hold on;
axis([0 0.04 0.75 1]);
%}