%compute partial pdf for the range of T at mu=mu2 based on conditional simulation and initial
%asymptotic approximation given at mu=mu1

function p_one = f_npdf_sym_local_sim3(mu1,mu2,T,T_min,T_full,p,epsilon,beta,dt,loop);
%mu1, initial condition where asymptotic approximation alone provides good
%approximation
%mu2, where the system stops
%dt, length of small time step
%T, series of the partial temperature range
%T_min, value that defines early escape in simulation
%T_full, the whole range of T for the histogram
%p, pdf at mu=mu1 in the range T
%loop, rough number of numerical simulation

v = epsilon^beta;
mu = mu1:(-v*dt):mu2;
%p(1)
p_max = max(p);
num_T0 = zeros(1,length(T));%generate number of T0 the same as value T1(i)
for i=1:length(T);
    %N = dT1(i)*p_11(i)*loop;
    N = p(i)/p_max*loop; %make loop to be the number of the simulation given T0 = T1(end)
    num_T0(i) = round(N);
end;
%num_T0
SUM_full = sum(num_T0) %used to adjust the number of realizations for full-system simulation

T0 = ones(1,num_T0(1)).*T(1);
for i=2:length(T);
    S = ones(1,num_T0(i)).*T(i);
    T0 = [T0 S];
end;
%figure(3);plot(T,num_T0);pause;
%generate each T_n(i) based on T0(i), epsilon, beta, mu1 and mu2 by the full system;
T_n = zeros(1,length(T0));
%T_min = -5;
for i=1:length(T0);
    %TE = f_ntemperature(mu,T0(i),dt,epsilon,T(1),0.001,0);
    TE = f_ntemperature(mu,T0(i),dt,epsilon,T_min,0.001,0);
    T_n(i) = TE(end);
end;
%p_one = hist(T_n,T)./length(T0);
p_one = hist(T_n,T_full)./length(T0)./(T(2)-T(1));