%output the file of PDF based on quasi-stationary, merged process and numerical
%simulation for each stage

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = 0;%-A2/(6*A)^3;
c1 = 4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = -2*B2/(3*A*B1);%2*B2/(3*A*B1);

%constants for the full system
mu_0 = 0.05;
epsilon = 0.05;
beta = 2.2;
v = epsilon^beta;
mu_middle = 0.04;%mu_n+v*5%0.04; %0.03;%epsilon^(4/3)
m = 20; %same as update tail information after 20 time steps
dt = 0.1;
tau = m*dt; %time length for the tail distribution approximation; 
mu_n = -2.3381*v^(2/3)%0;%mu_middle-tau*v;
T_min = -5;
Tl = T_min-0.1;
Th = 1;
NT = 5000; %define the number of T(i) between Tl and Th

mu = mu_middle:(-v*dt):mu_n;
T2 = zeros(1,length(mu)); %need to be computed for each mu(i)
index_imag = find(mu<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_root(mu(i),c1,c2);
    T2(i) = R(2);
end;
Ts = T2+10*epsilon; %introduce Ts and Tm as two series
Ts(index_imag) = 0.2;
Ts(min(index_imag)-m:min(index_imag)-1) = 0.2; %need Ts cover the whole range before mu(i) become negative
Tm = T2+epsilon;
dT = 0.01;
T = -3:dT:Ts(1)+0.5;%-1.5:dT:Ts(1)+0.5;%T2(1)-0.5:dT:Ts(1)+0.5; %use a fixed range of T
Td = (T2-2*epsilon).*ones(1,length(mu));%T2-0.1; %new boundary for the mass to escape and not come back

%find the tail at mu_middle
x = T2(1):0.001:Ts(1);
x2 = x; %for the tail approximation only with convection-diffusion process
f_total = f_a_npdf2(mu_middle,T,c1,c2,epsilon,beta); %dimension 1*n
int_0 = trapz(T,f_total);
f = f_a_npdf2(mu_middle,x,c1,c2,epsilon,beta)./int_0; %pdf on the tail x

%compare the numerical simulation by setting up T0 at mu_middle
hT = (Th-Tl)/NT;
T_hist = Tl:hT:Th; 
f_0 = f_a_npdf2(mu_middle,T_hist,c1,c2,epsilon,beta);
c_norm = trapz(T_hist,f_0);
f_0 = f_0./c_norm;
index_pos = find(f_0>.00001); %index of p that satisfies p>.00001
Tl = T_hist(1) + hT.*(min(index_pos)-1); %lower boundary of T where p>.00001, same as T(index_pos(1))
Th = T_hist(1) + hT.*(max(index_pos)-1); %higher boundary of T where p>.00001
loop = 250000; %provide number of realizations for each p(i)
T0_initial = Tl; %one more extra
for i=index_pos(1):index_pos(end);
    k = i-index_pos(1)+1; %index number for related new T
    j = round(loop.*f_0(i).*hT);
   if j >= 1;
    xtemp =(Tl+(k-1).*hT).*(ones(1,j)); %count all the initials starting at T1+k*hT
    T0_initial = [T0_initial xtemp];
end;
end;
T0 = T0_initial(2:end);
size(T0)
pause;
dW = randn(length(mu),length(T0));
Tv = zeros(length(mu),length(T0)); %define the matrix to record all the change of T at each mu(i)
Tv(1,:) = T0;
index_Ts = min(find(T0>Ts(1)));

%{
CDF_asym = 1; %compute the probability for 1-(T>T2)
CDF_sim = 1;
CDF_qs = 1; %probability based only on quasi-stationary approximation
prob_asym_un = 0; %compute the probability for Td<T<T2
prob_sim_un = 0;
prob_asym_un2 = 1; %compute the probability for Td<T
prob_sim_un2 = 1;
prob_asym_un3 = 1; %compute the probability for Td<T
prob_sim_un3 = 1;
%}
mu_s = mu_middle;
prob_exit = 0; %cumulative after each stage
p_merge = zeros(1,length(T));
tau_s = 0:0.01:tau;
tau_new = tau_s(1:length(tau_s)-1); %eliminate the singularity at tau_s(end) = tau
T_new = T';

%generate initial file elements
T_file = T'; %dimension n*1
p_qs_file = [T_file f_total'./int_0];
p_merge_file = [T_file f_total'./int_0];
p_num_file = [T_file f_total'./int_0];
mu_file = [0 mu_middle];

for i=2:length(mu);
    
    %update the numerical simulation
    Tv(i,:) = Tv(i-1,:)+((c0.*Tv(i-1,:).^4+c1.*Tv(i-1,:).^3-Tv(i-1,:).^2)+c2.*mu(i).*Tv(i-1,:)+mu(i)).*dt+epsilon.*sqrt(dt).*dW(i,:);
    %tic
    if(mod(i-1,m)==0);
        mu_i = mu(i)
        T2_i = T2(i)
        
        nhist_full = hist(Tv(i,:),T)./length(T0)./dT;%dimension 1*n
        %nhxva = hist(Tv(i,1:index_Ts-1),T)./length(T0)./dT;
        %Tv(i,:) = sort(Tv(i,:));
        %index_Ts = min(find(Tv(i,:)>Ts(i)));
        
        %{
        %use solution of advection-diffusion equation for the leading order approximation without too many asymptotic terms
        brown = zeros(length(T),length(x));
        %Size_b = size(brown)
        %Size_f = size(f)
        for j=1:length(T);
            for k=1:length(x);
                brown(j,k) = exp(-(T(j)-mu(i-m)*tau-x(k))^2/2/tau/epsilon^2)/sqrt(2*pi*tau)/epsilon;
            end;
        end;
        p0_n = zeros(length(T),1);
        for j=1:length(T);
            p0_n(j) = trapz(x,brown(j,:).*f);
        end;
        
    %define -p0_{T} for tau(0:tau) and T(Tl:Th)
    p0_T = zeros(length(T),length(tau_s)); %acturally -p0_T
    p0 = zeros(length(T),length(tau_s)); %p0 for each T_i,tau_j
    F1 = zeros(length(T),length(tau_s));
    F2 = zeros(length(T),length(tau_s));
    F3 = zeros(length(T),length(tau_s));
    F4 = zeros(length(T),length(tau_s));
    F5 = zeros(length(T),length(tau_s));
    F6 = zeros(length(T),length(tau_s));
    for k=2:length(tau_s);
        for j=1:length(T);
            p0_T_integ = -exp(-(T(j)-mu(i-m)*tau_s(k)-x).^2./2./tau_s(k)./epsilon^2).*(T(j)-mu(i-m)*tau_s(k)-x)./tau_s(k)./epsilon^3./sqrt(2*pi*tau_s(k)).*f;
            p0_T(j,k) = trapz(x,p0_T_integ);
        
            p0_integ = exp(-(T(j)-mu(i-m)*tau_s(k)-x).^2./2./tau_s(k)./epsilon^2)./epsilon./sqrt(2*pi*tau_s(k)).*f;
            p0(j,k) = trapz(x,p0_integ);
        
            F1(j,k) = 2*T(j)/epsilon*p0(j,k);
            F2(j,k) = T(j)^2/epsilon*p0_T(j,k);
            F3(j,k) = epsilon^(beta-1)*tau_s(k)*p0_T(j,k);
            F4(j,k) = -mu(i-m)/epsilon*p0(j,k);
            F5(j,k) = -mu(i-m)/epsilon*T(j)*p0_T(j,k);
        end;
    end;
    F = F1+F2+F3+F4+F5;
    
    %compute p1(T(i),tau) by matrix operation
    p1_n = zeros(length(T),1);
    tau_new = tau_s(1:length(tau_s)-1); %eliminate the singularity at tau_s(end) = tau
    T_new = T';
    for k=1:length(T);
        %construct the matrix for exponential parts    
        A1 = repmat([1./sqrt(2.*pi.*(tau-tau_new))./epsilon 0],length(T),1);    
        A2 = repmat(-(T(k)-mu(i-m)*tau-T_new).^2,1,length(tau_s));
        A3 = repmat([1./(tau-tau_new)./2./epsilon^2 0],length(T),1);
        A = A1.*exp(A2.*A3);
        p1_integ = A.*F;
        p1_int_tau = trapz(T_new,p1_integ);
        p1_n(k) = trapz(tau_s,p1_int_tau);
    end;
    
    %add one more term as the approximation on order epsilon^2
    %add p1_T(j,i) and p1_T(j,i) to estimate p2_n
    p1_T = zeros(length(T),length(tau_s));
    p1_full = zeros(length(T),length(tau_s));
    for k=2:length(tau_s); %consider tau_s(end) later
        for j=1:length(T);
            tau_p = tau_s(1:k-1); %integration region for tau
            A1 = repmat([1./sqrt(2.*pi.*(tau_s(k)-tau_p))/epsilon 0],length(T),1); 
            A2_1 = repmat((T(j)-T_new),1,length(tau_p)+1); %matrix T-z
            A2_2 = repmat([-mu_middle.*tau_p 0],length(T),1); %matrix -mu*tau
            A2 = -(A2_1-A2_2).^2;
            A3 = repmat([1./(tau_s(k)-tau_p)./2./epsilon^2 0],length(T),1);
            A4 = -(A2_1-A2_2);
            A5 = repmat([1./(tau_s(k)-tau_p)./epsilon^2 0],length(T),1);
            A = A1.*exp(A2.*A3).*A4.*A5;
            p1T_integ = A.*F(:,1:k);
            p1T_int_tau = trapz(T_new,p1T_integ);
            p1_T(j,k) = trapz(tau_s(1:k),p1T_int_tau);
        
            %compute p1(j,i)
            B = A1.*exp(A2.*A3);
            p1_integ = B.*F(:,1:k);
            p1_int_tau = trapz(T_new,p1_integ);
            p1_full(j,k) = trapz(tau_s(1:k),p1_int_tau);
        end;
    end;
    %find F=F1+F2+F3 for p2_n
    for k=2:length(tau_s);
        for j=1:length(T);
            F1(j,k) = 2*T(j)/epsilon*p1_full(j,k);
            F2(j,k) = T(j)^2/epsilon*p1_T(j,k);
            F3(j,k) = epsilon^(beta-1)*tau_s(k)*p1_T(j,k);
            F4(j,k) = -mu(i-m)/epsilon*c2*p1_full(j,k);
            F5(j,k) = -mu(i-m)/epsilon*c2*T(j)*p1_T(j,k);
            F6(j,k) = c1/epsilon^2*(3*T(j)^2*p0(j,k)+T(j)^3*p0_T(j,k))+epsilon^(beta-2)*c2*tau_s(k)*(p0(j,k)+T(j)*p0_T(j,k));
        end;
    end;
    F = F1+F2+F3+F4+F5+F6;
    
    %compute p2(T(i),tau) by matrix operation
    p2_n = zeros(length(T),1);
    for k=1:length(T);
        %construct the matrix for exponential parts    
        A1 = repmat([1./sqrt(2.*pi.*(tau-tau_new))./epsilon 0],length(T),1);    
        A2 = repmat(-(T(k)-mu_middle*tau-T_new).^2,1,length(tau_s));
        A3 = repmat([1./(tau-tau_new)./2./epsilon^2 0],length(T),1);
        A = A1.*exp(A2.*A3);
        p2_integ = A.*F;
        p2_int_tau = trapz(T_new,p2_integ);
        p2_n(k) = trapz(tau_s,p2_int_tau);
    end;  
    
    p_n = p0_n+epsilon.*p1_n+epsilon^2.*p2_n;
    
    %compute the escaping rate as prob(T<Td)
    index_Td = min(find(T>Td(i-m)))-1;
    prob_exit = prob_exit+trapz(T(1:index_Td),p_n(1:index_Td));
    
    
    if mu(i)>0;
    %merge p_n(T>Td) with quasi-stationary approximation
    index_Tm = min(find(T>Tm(i-m)));
    p_qs = f_a_npdf2(mu(i),T,c1,c2,epsilon,beta);
    p_merge(index_Td+1:index_Tm-1) = p_n(index_Td+1:index_Tm-1);
    merge_coeff = prob_exit+trapz(T(index_Td+1:index_Tm-1),p_n(index_Td+1:index_Tm-1));
    p_merge(index_Tm:end) = p_qs(index_Tm:end)./trapz(T(index_Tm:end),p_qs(index_Tm:end)).*(1-merge_coeff);
    %make sure the total probability is 1
    check_norm = trapz(T,p_merge)+prob_exit;
    p_merge = p_merge./check_norm;
    prob_exit = prob_exit./check_norm;
    %check_norm2 = trapz(T,p_merge)+prob_exit
    else
        p_merge = p_n'; %use the left tail as the final approximation
    end;
    
    %expand each file elements
    p_qs_file = [p_qs_file p_qs'./int_0];
    p_merge_file = [p_merge_file p_merge'];
        %}
    p_num_file = [p_num_file nhist_full'];
    mu_file = [mu_file mu(i)];    
    
    %{
    %define x and f for the next stage
    index_xh = min(find(T>Ts(i)));
    x = T(index_Td:index_xh-1);
    f = p_merge(index_Td:index_xh-1);
    K = i-m; %mark the index for the last stage
    %}
    
    %{
    %check the pdf right before mu<0
    if mod(i-1,100)==0%(i+m<length(mu)) && (mu(i+m)<0);
    figure(5);
    plot(T,p_merge,'b-');hold on;
    %plot(T,p_qs./int_0,'r-.');hold on;
    plot(T,nhxva,'k-.');hold on;
    plot(T,p_n,'m-');hold on;
    plot(T,nhist_full,'k:');hold on;
    plot(T2(K+m),0,'ro');hold on;plot(Ts(K),0,'b*');hold on;plot(Tm(K),0,'k*');hold on;plot(Ts(K+m),0,'bo');hold on;
    plot(T(index_Td),0,'r^');hold off;
    title(['\mu = ',num2str(mu(i))],'fontsize',14);
    grid on;
    pause;
    end;
    %}
    
    %{
    %find the probability for T>T2 by 1-prob(T<T2)
    index_T2 = min(find(T>T2(i)));
    X1 = trapz(T(index_Td+1:index_T2-1),p_merge(index_Td+1:index_T2-1))+prob_exit; %for merged pdf
    %CDF_asym = [CDF_asym 1-X1];
    X2 = trapz(T,p_qs)./int_0;
    CDF_qs = [CDF_qs X2];
    %X3 = trapz(T(1:index_T2-1),nhist_full(1:index_T2-1));
    %X3 = length(find(Tv(i,:)<T2(i)))./length(T0);
    %CDF_sim = [CDF_sim 1-X3];
    mu_s = [mu_s mu(i)];
    
    %find the probability for T>T2 by prob(T>T2)
    H1 = trapz(T(index_T2:end),p_merge(index_T2:end))
    CDF_asym = [CDF_asym H1];
    %H2 = trapz(T(index_T2:end),nhist_full(index_T2:end));
    H2 = length(find(Tv(i,:)>T2(i)))./length(T0);
    CDF_sim = [CDF_sim H2];
    
    check_CDF_asym = X1+H1
    %check_CDF_sim = X3+H2
    %MIN_sim = min(Tv(i,:))
    %check_NUM = find(isnan(Tv(i,:))>0)
    
    %find the probability for Td<T<T2
    Y1 = trapz(T(index_Td+1:index_T2-1),p_merge(index_Td+1:index_T2-1));
    prob_asym_un = [prob_asym_un Y1];
    %Y2 = trapz(T(index_Td+1:index_T2-1),nhist_full(index_Td+1:index_T2-1));
    Y2 = length(find(Tv(i,:)>Td(i-m)))./length(T0)-H2;
    prob_sim_un = [prob_sim_un Y2];
    
    %find the probability for Td<T
    Z1 = trapz(T(index_Td+1:end),p_merge(index_Td+1:end));
    prob_asym_un2 = [prob_asym_un2 Z1];
    %Z2 = trapz(T(index_Td+1:end),nhist_full(index_Td+1:end));
    Z2 = length(find(Tv(i,:)>Td(i-m)))./length(T0);
    prob_sim_un2 = [prob_sim_un2 Z2];
    
    %{
    figure(4);
    plot(T,p_merge,'b-');hold on;
    plot(T,p_n,'r-.');hold on;
    plot(T,p_qs./trapz(T,p_qs),'k:');hold on;
    plot(T,nhist_full,'m.');hold on;
    plot(T2(i),0,'ro');hold on;plot(Ts(i-m),0,'b*');hold on;plot(Tm(i-m),0,'k*');grid on;
    title(['\mu = ',num2str(mu(i))]);
    hold off;

    pause;
    %}
    
    %figure(5);
    %plot(mu(i),H1,'bo');hold on;plot(mu(i),H2,'k*');hold on;plot(mu(i),X2,'ro');hold on;
    %pause;
    %}
    end;
    %toc
end;


%rename the file based on symmetry
%file_qs = [p_qs_file;mu_file];
%savefile_qs = 'file_PDF_qs_asym3.dat';
%save(savefile_qs, 'file_qs','-ascii')
%file_merge = [p_merge_file;mu_file];
%savefile_merge = 'file_PDF_merge_asym4.dat';
%save(savefile_merge, 'file_merge','-ascii')
file_num = [p_num_file;mu_file];
savefile_num = 'file_PDF_num_oppo2.dat';
save(savefile_num, 'file_num','-ascii')
%}