%only consider the numerical results of symmetric bifurcation with noise

%constants for the symmetric system, the same as the asymmetric one
epsilon = 0.2;
%beta = 2.5;
v = 0.005;%epsilon^beta;
beta = log(v)/log(epsilon)
mu_middle = 0.2;%mu_n+v*5%0.04; %0.03;%epsilon^(4/3)
dt = 0.1; 
mu_n = -2.3381*v^(2/3);%0;%mu_middle-tau*v;
T_min = -5;
Tl = T_min-0.1;
Th = 1.5;
NT = 5000; %define the number of T(i) between Tl and Th

mu = mu_middle:(-v*dt):mu_n;
dT = 0.01;
T = -3:dT:1.5;
T2 = zeros(1,length(mu)); %need to be computed for each mu(i)
index_imag = find(mu<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    %R = f_n_root(mu(i),c1,c2);
    T2(i) = -sqrt(mu(i));%R(2);
end;

%{
%initial probability
f_total = f_npdf_sym7(mu_middle,T,epsilon,beta); %dimension 1*n
int_0 = trapz(T,f_total);

%compare the numerical simulation by setting up T0 at mu_middle
hT = (Th-Tl)/NT;
T_hist = Tl:hT:Th; 
f_0 = f_npdf_sym7(mu_middle,T_hist,epsilon,beta);
c_norm = trapz(T_hist,f_0);
f_0 = f_0./c_norm;
index_pos = find(f_0>.00001); %index of p that satisfies p>.00001
Tl = T_hist(1) + hT.*(min(index_pos)-1); %lower boundary of T where p>.00001, same as T(index_pos(1))
Th = T_hist(1) + hT.*(max(index_pos)-1); %higher boundary of T where p>.00001
loop = 250000; %provide number of realizations for each p(i)
T0_initial = Tl; %one more extra
for i=index_pos(1):index_pos(end);
    k = i-index_pos(1)+1; %index number for related new T
    j = round(loop.*f_0(i).*hT);
   if j >= 1;
    xtemp =(Tl+(k-1).*hT).*(ones(1,j)); %count all the initials starting at T1+k*hT
    T0_initial = [T0_initial xtemp];
   end;
end;
T0 = T0_initial(2:end);
size(T0)
pause;
dW = randn(length(mu),length(T0));
Tv = zeros(length(mu),length(T0)); %define the matrix to record all the change of T at each mu(i)
Tv(1,:) = T0;

%generate initial file elements
T_file = T'; %dimension n*1
p_num_file = [T_file f_total'./int_0];
mu_file = [0 mu_middle];

for i=2:length(mu);
    
    if mod(i,20)==0;
        mu(i)
    end;
    
    %update the numerical simulation
    Tv(i,:) = Tv(i-1,:)+((-Tv(i-1,:).^2)+mu(i)).*dt+epsilon.*sqrt(dt).*dW(i,:);
    nhist_full = hist(Tv(i,:),T)./length(T0)./dT;
    
    p_num_file = [p_num_file nhist_full'];
    mu_file = [mu_file mu(i)]; 
    
end;

file_num = [p_num_file;mu_file];
savefile_num = 'file_PDF_sym_num5.dat';
save(savefile_num, 'file_num','-ascii')
%}

load file_PDF_sym_num5.dat
[m1,n1] = size(file_PDF_sym_num5);
T_sym = file_PDF_sym_num5(1:m1-1,1);
mu_sym = file_PDF_sym_num5(end,2:end);
pdf_sym_num = file_PDF_sym_num5(1:m1-1,2:end);

CDF_sym_num = 1;
CDF_sym_num_escape = 1; %consider all the probability for the whole T(>T_min)
for i=1:length(mu_sym);
    index_T = min(find(T_sym>T2(i)));
    
    P_sym_num = trapz(T_sym(index_T:end),pdf_sym_num(index_T:end,i));
    CDF_sym_num = [CDF_sym_num P_sym_num];
    
    %size(T_sym)
    %size(pdf_sym_num)
    %pause;
    P_sym_num_escape = trapz(T_sym(2:end),pdf_sym_num(2:end,i));
    CDF_sym_num_escape = [CDF_sym_num_escape P_sym_num_escape];
end;
Fig = figure;
plot(mu_sym,CDF_sym_num(2:end),'k--','linewidth',2);hold on;
plot(mu_sym,CDF_sym_num_escape(2:end),'b--','linewidth',2);hold on;
grid on;
print(Fig,'-depsc','figure_CDFforPeriodic.eps')