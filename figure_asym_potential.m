%generate the figure to show potential changes between small and large mu
%for the asymmetric case

%generate the figure in Section model to show the bifurcation structure,
%delay effect of the drifting rate and early tipping caused by large noise

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%case for large mu
mu1 = 0.2;
T = -2:0.01:2;
U1 = -(c0.*T.^5./5+c1.*T.^4./4-T.^3./3+c2.*mu1.*T.^2./2+mu1.*T);
%generate its stable and unstable equilibria
R = f_n_fullroot(mu1,c0,c1,c2);
T1_s = R(1);
U1_s = -(c0.*T1_s.^5./5+c1.*T1_s.^4./4-T1_s.^3./3+c2.*mu1.*T1_s.^2./2+mu1.*T1_s);
T1_uns = R(2);
U1_uns = -(c0.*T1_uns.^5./5+c1.*T1_uns.^4./4-T1_uns.^3./3+c2.*mu1.*T1_uns.^2./2+mu1.*T1_uns);

%case for small mu
mu2 = 0.01;
U2 = -(c0.*T.^5./5+c1.*T.^4./4-T.^3./3+c2.*mu2.*T.^2./2+mu2.*T);
%generate its stable and unstable equilibria
R = f_n_fullroot(mu2,c0,c1,c2);
T2_s = R(1);
U2_s = -(c0.*T2_s.^5./5+c1.*T2_s.^4./4-T2_s.^3./3+c2.*mu2.*T2_s.^2./2+mu2.*T2_s);
T2_uns = R(2);
U2_uns = -(c0.*T2_uns.^5./5+c1.*T2_uns.^4./4-T2_uns.^3./3+c2.*mu2.*T2_uns.^2./2+mu2.*T2_uns);

Fig = figure;
plot(T,U1,'b-.','linewidth',2);hold on;plot(T,U2,'r-','linewidth',2);
hold on;plot(T1_s,U1_s,'o','MarkerFace','b','Markersize',8);
hold on;plot(T1_uns,U1_uns,'o','MarkerEdge','b','Markersize',8,'linewidth',1.5);
hold on;plot(T2_s,U2_s,'o','MarkerFace','r','Markersize',8);
hold on;plot(T2_uns,U2_uns,'o','MarkerEdge','r','Markersize',8,'linewidth',1.5);
xlabel('$$T$$','FontSize',40,'interpreter','latex');
ylabel('$$U$$','FontSize',40,'interpreter','latex');
legend('\mu=0.2','\mu=0.01','location','northwest');
set(gca,'fontsize',18);
axis([-1 1 -0.8 0.8]);
print(Fig,'-depsc','figure_asym_potential.eps')