% grnerate skewness curve for different stop point a_t for symmetric and
% asymmetric case for the temperature model

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.002;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.02;%3, size of the additional noise;
beta = 2.2;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.01;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 10000;

mu = mu_0:-0.001:mu_n;
n = length(mu);
SKN_asym = zeros(1,n);
VAR_asym = zeros(1,n);
SKN_sym = zeros(1,n);
VAR_sym = zeros(1,n);
SKN_stat = zeros(1,n);
Var_stat = zeros(1,n);

N=1; %need to normalize the pdf first
int_p = zeros(1,n);
for i=1:n;
    root = f_n_root(mu(i),c1,c2);
    T_r = root(2)+0.001:0.001:2;
    [p_a p_sym] = f_a_npdf_compare(mu(i),T_r,c1,c2,epsilon,beta);
    Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);
    if p_a(1)<10^(-8);
        N = N+1;
    end;
    int_p(i) = trapz(T_r,p_a);
    [m,var,skew] = f_skewness(T_r,p_a/int_p(N-1));
    SKN_asym(i) = skew;
    VAR_asym(i) = var;
    [m,var,skew] = f_skewness(T_r,p_sym*Con);
    SKN_sym(i) = skew;
    VAR_sym(i) = var;
    [p_a_LO p_sym_LO] = f_a_npdf_LO(mu(i),T_r,c1,c2,epsilon,beta); 
    [m, var, skew] = f_skewness(T_r,p_sym_LO);
    SKN_stat(i) = skew;
    Var_stat(i) = var;
end;

Fig = figure;
plot(mu,SKN_sym,'r-.',mu,SKN_stat,'b:','LineWidth',2);grid on;
hold on;plot(mu,SKN_asym,'k-','Linewidth',2);
xlabel('$$\mu$$','fontsize',20,'interpreter','latex');
ylabel('$$\mbox{Skewness} \gamma$$','fontsize',20,'interpreter','latex')
title(['$$\epsilon$$ = ',num2str(epsilon), '; $$\beta$$ = ',num2str(beta)],'fontsize',20,'interpreter','latex')
%print(Fig,'-depsc','figure_skewness.eps')

Fig2 = figure;
plot(mu,VAR_sym,'r-.',mu,Var_stat,'b:','LineWidth',2);grid on;
hold on;plot(mu,VAR_asym,'k-','Linewidth',2);
xlabel('$$\mu$$','fontsize',20,'interpreter','latex');
ylabel('$$\mbox{Skewness} \sigma^2$$','fontsize',20,'interpreter','latex')
title(['$$\epsilon$$ = ',num2str(epsilon), '; $$\beta$$ = ',num2str(beta)],'fontsize',20,'interpreter','latex')