%check the potential well for different value of c1 and c2 at a relatively
%small value of mu

%introduce parameter value T2 and Td for opposite-symmetry case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = 0;%-A2/(6*A)^3;


%mu = 0.04;
T = -1:0.001:1;
l = length(T);
beta = 2.2;
epsilon=0.05;
mu_s = 0.04:(-epsilon^beta*2):epsilon^beta*2;

for j=1:length(mu_s);
    mu = mu_s(j);
%for original asymmetric case
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
root = f_n_root(mu,c1,c2);
T1 = root(2)+0.001:0.001:root(3)-0.001;
%{
p_a = f_a_npdf(mu,T1,1,-1,epsilon,beta);
p_0 = f_a_npdf(0.1,T1,1,-1,epsilon,beta);
int_a = trapz(T1,p_0);
%}
l = length(T);
U1 = zeros(1,l);
for i=1:l;
    U1(i) = -(-2*T(i)^3/3+2*mu*T(i)+c1*T(i)^4/2+c2*mu*T(i)^2);
end;
ph1 = c1.*T.^3-T.^2+c2.*mu.*T+mu; %sketch the phase portrait for both asymmetric cases
%size(T)
%size(U1)


%for opposite asymmetric case
c1 = 4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = -2*B2/(3*A*B1);%2*B2/(3*A*B1);
root = f_n_root(mu,c1,c2);
T2 = root(2)+0.001:0.001:2;
%{
p_opp = f_a_npdf(mu,T2,-1,1,epsilon,beta);
p_opp_0 = f_a_npdf(0.1,T2,-1,1,epsilon,beta);
int_opp = trapz(T2,p_opp_0);
%}
%l = length(T2);
U2 = zeros(1,l);
for i=1:l;
    U2(i) = -(-2*T(i)^3/3+2*mu*T(i)+c1*T(i)^4/2+c2*mu*T(i)^2);
end;
ph2 = c1.*T.^3-T.^2+c2.*mu.*T+mu; %sketch the phase portrait for both asymmetric cases

%for c1=c2=0;
T3 = -sqrt(mu):0.001:2;
%{
p_sym = f_npdf_sym(mu,T3,epsilon,beta);
p_sym2 = f_npdf_sym2(mu,T3,epsilon,beta);
T30 = -sqrt(0.1):0.001:2;
p_sym_0 = f_npdf_sym(0.1,T30,epsilon,beta);
int_sym = trapz(T30,p_sym_0);
%}
%Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);
%l = length(T3);
U3 = zeros(1,l);
for i=1:l;
    U3(i) = -mu*T(i)+T(i)^3/3;
end;

%also draw the two equilibrium
Y = min(U1):0.001:max(U2);
m = length(Y);
V1 = ones(1,m).*sqrt(mu);
V2 = ones(1,m).*(-sqrt(mu));

%{
%compare the CDF curve with histogram from numerical simulations
loop = 10000;
v = epsilon^beta;
dt = 0.01;
mu_1 = 0.5:(-v*dt):mu;
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
%[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,1,-1);
%[h_opp,NUM_opp] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,-1,1);
[h_sym,NUM_sym] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,-0.001,0);
%NUM/loop
%NUM_opp/loop
%NUM_sym/loop
dC = 1/100;
C = T_min:dC:2;
%Hist = hist(h,C)./loop./dC;
%Hist_opp = hist(h_opp,C)./loop./dC;
Hist_sym = hist(h_sym,C)./loop./dC;
%}

%also sketch the unstable boundary for our approximation
H = 0:0.1:15;
l = length(H);
L = ones(1,l).*(-sqrt(mu));

figure(1);
plot(T,U1,'b-.','Linewidth',2);hold on;
plot(T,U2,'k-.','Linewidth',2);hold on;
plot(T,U3,'r-','Linewidth',2);hold on;
plot(V1,Y,'r:','Linewidth',2);hold on;
plot(V2,Y,'r:','Linewidth',2);
legend('original asymmetric','opposite asymmetric','symmetric')
title(['$$\mu=$$ ',num2str(mu)],'FontSize',20,'interpreter','latex');
hold off;
%axis([-0.5 0.5 -0.005 0.005]);

figure(2);
plot(T,ph1,'b-.','Linewidth',2);hold on;grid on;
plot(T,ph2,'k-.','Linewidth',2);hold off;
legend('original asymmetric','opposite asymmetric')
title(['$$\mu=$$ ',num2str(mu)],'FontSize',20,'interpreter','latex');

pause;
end;

%{
figure;
plot(T1,p_a/int_a,'b-.','Linewidth',2);hold on;
plot(T2,p_opp/int_opp,'k-.','Linewidth',2);hold on;
plot(T3,p_sym/int_sym,'g','Linewidth',2);hold on;
plot(T3,p_sym2/int_sym,'r','Linewidth',2);
hold on;plot(L,H,'r:','Linewidth',2);
%hold on;plot(C,Hist,'bo');hold on;plot(C,Hist_opp,'ko');
hold on;plot(C,Hist_sym,'ro');
title(['$$\epsilon = $$',num2str(epsilon),'; $$\beta =$$ ',num2str(beta),'; $$\mu=$$ ',num2str(mu)],'fontsize',20,'interpreter','latex')
axis([-0.4 0.6 0 5]);
%}