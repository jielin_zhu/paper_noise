%check bifurcation structure for a more-generalized model with saddle-node
%bifurcation and slowly varying bifurcation parameter as dot(x)=Ax^2+B mu
%x+C+mu(tau)

%given values of parameter A,B,C and verify the validity for bifurcation
%structure
A = -1;
B = 4;
C = -0.5;
if A^2+B^2*A*C<0
    error('Wrong choices for A,B,C');
end;

%define range of mu and generate original bifurcation structure
x = -10:0.01:10;
mu = -(A.*x.^2+C)./(B.*x+1);
figure(1);
plot(mu,x);hold on;

%compute the larger critical value of mu
if B==0;
    mu_e = -C;
else
    mu_e = (2*A+2*sqrt(A^2+B^2*A*C))/B^2;
end;
x_e = -B*mu_e/2/A;
plot(mu_e,x_e,'r*');

%try to normalize the system
D = -1/A
E = 1/sqrt(A^2+B^2*A*C)
y = (x-x_e)./D;
a = (mu-mu_e)./E;
figure(2);plot(a,y,'r-','linewidth',2);hold on;plot(0,0,'r*');
axis([0 1 -1 1]);
%compare the normalized bifurcation structure with symmetric case
a_sym = a(find(a>0));
y_pos = sqrt(a_sym);
y_neg = -sqrt(a_sym);
plot(a_sym,y_pos,'k-','linewidth',2);hold on;plot(a_sym,y_neg,'k-.','linewidth',2);

c2 = B/sqrt(A^2+B^2*A*C)