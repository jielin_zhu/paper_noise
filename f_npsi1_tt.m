function psi_tt = f_npsi1_tt(T,mu,c1,c2)

%T is the specific value of normalized temperature;
%mu is the specific value of the drifting parameter;
%c1,c2 are coefficients for the fully normalized temperature model

root = f_n_root(mu,c1,c2);
R1 = root(1);
R2 = root(2);
R3 = root(3);

%P1 = -c2+(2+c2*(T+R1))/(T-R2)+(2+c2*(T+R1))/(T-R3);
%P2 = c1*(T-R2)*(T-R3);
P1 = -(2+c2*(R1+R2))/(T-R2)^2+(2+c2*(R1+R3))/(T-R3)^2;
P2 = c1*(R3-R2);

%Particularly for c1<0 and c2>0
%root = f_n_root(mu,c1,c2);
%T1 = root(1);
%T2 = -root(2);
%T3 = -root(3);
%P1 = -c2+(2+c2*(T+T1))/(T+T2)+(2+c2*(T+T1))/(T+T3);
%P2 = c1*(T+T2)*(T+T3);

psi_tt = P1/P2;