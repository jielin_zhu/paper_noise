% return to a time series based only on Brownian motion

function B = f_brownian(mu,T_0,dt,epsilon);
%mu is slowly decreasing with drifting rate v;
%T_0 is the initial value for the time series
%dt is the time step
%epsilon is the amplitude of the white noise 

n = length(mu);
B = zeros(1,n);
dW = randn(1,n);
B(1) = T_0;

for i=2:n;
    B(i) = B(i-1)+epsilon*sqrt(dt)*dW(i);
end;