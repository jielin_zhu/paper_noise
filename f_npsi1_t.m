function psi1T = f_npsi1_t(T,mu,c1,c2)
%mu is the specific value in the series of mu_tao:dmu:0;
%T can be one specific T or a series
%c1,c2 are coefficients for the fully normalized temperature model

root = f_n_root(mu,c1,c2);
R1 = root(1);
R2 = root(2);
R3 = root(3);
%T1 is the stable equilibrium for mu

psi1T1 = 2+c2.*(T+R1);
psi1T2 = -c1.*(T-R2);
psi1T3 = T-R3;

%Particularly for the case c1<0 and c2>0
%root = f_n_root(mu,c1,c2);
%T1 = root(1);
%T2 = -root(2);
%T3 = -root(3);
%T1 is the stable equilibrium for mu
%psi1T1 = 2+c2.*(T+T1);
%psi1T2 = -c1.*(T+T2);
%psi1T3 = T+T3;

psi1T = psi1T1./psi1T2./psi1T3;
