%Generate PDF for a series of mu, but use the normalization constant as the
%integration of p just before the PDF becomes positive at T_r(1);

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -0.001;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 0;%2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.2;%1, normalized mu at the beginning;
mu_n = 0.04;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 2.2;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.1;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 50000;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for mu_n for symetric and asymmetric case
root = f_n_root(mu_n,c1,c2);
T_r = -0.2:0.001:2; %define the domain of PDF for every different mu(i)
%T_r = 0:0.001:2;
p_sym = f_npdf_sym(mu_n,T_r,epsilon,beta);
T_r_neg = -0.25:0.001:0;
p_extra = f_npdf_sym_neg(mu_n,T_r_neg,epsilon,beta);
%[p_a p_sym] = f_a_npdf_compare(mu_n,T_r,c1,c2,epsilon,beta); 
%[p_a_LO p_sym_LO] = f_a_npdf_LO(mu_n,T_r,c1,c2,epsilon,beta); 
%[p_b p_sym1] = f_b_npdf_compare(mu_n,T_r,c1,c2,epsilon,beta); 
%int_p_b = trapz(T_r,p_b);
%int_sym = trapz(T_r,p_sym);
%int_extra = trapz(T_r,p_extra);
%Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case
figure(1);
plot(T_r,p_sym,'r-','Linewidth',2);pause;
hold on;plot(T_r_neg,p_extra,'b-.','Linewidth',2);
pause;

%Find the proper normalization constant for asymmetric case, which is the
%integral of p_sym for mu just before early tipping happens
%N=1;
%P_0=0; %p_asym at T_r(1) for each mu(i);
%while (N<n) && (P_0<10^(-5))
    %N=N+1;
    %root = f_n_root(mu(N),c1,c2);
    %T_r1 = root(2)+0.001:0.001:2;
    %[P P_sym] = f_a_npdf_compare(mu(N),T_r1,c1,c2,epsilon,beta);
    %P_0=P(1);
%end;
%N
%pause;
%int_p = trapz(T_r1,P);

%compare the CDF curve with histogram from numerical simulations
mu_1 = 0.5:(-v*dt):mu_n;
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
dC = 1/200;
C = T_min:dC:2;
T_hist = hist(h,C)./loop./dC;

%calculate pdf based on gaussian approximation
p_gau = f_gau_pdf(mu_1,v,dt,T_r,epsilon);

%generate figure result in .eps form
Fig = figure;
%plot(C,T_hist,'Marker','o','Color','k');
%hold on;plot(T_r,p_a/int_p,'r-','Linewidth',2);
%hold on;plot(T_r,p_b/int_p_b,'b-.','Linewidth',2);
plot(T_r,p_sym/int_sym,'r-','Linewidth',2);
hold on;plot(T_r,p_extra/int_extra,'b-','Linewidth',2);
hold on;plot(T_r,p_gau,'g-.','Linewidth',2);
%axis([-0.1 0.3 0 7]);
xlabel('$$T$$','fontsize',20,'interpreter','latex');
ylabel('$$p(T,\mu)$$','fontsize',20,'interpreter','latex');
title(['$$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$v$$= ',num2str(V),'; $$\mu$$= ',num2str(mu_n)],'fontsize',15,'interpreter','latex')
