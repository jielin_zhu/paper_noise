%check conditional pdf given at mu = mu_middle, the value of x is near the
%unstable branch based on numerical simulations

%introduce initial parameter values
mu_0 = 0.1;
%mu_n = 0.025;
epsilon = 0.05;
beta = 2.5;
mu_middle = 0.03;%epsilon^(4/3);
v = epsilon^beta;
mu_n = mu_middle-v*0.5;
dT = 0.001;
T_min = -5;
dC = 1/200; %length of bin for histgram;
C = T_min-dC:dC:2;

%first we generate the numerical pdf at mu_middle to pick the next T_0
%with stable and unstable equilibria
loop = 10000;
dt = 0.1;
%mu_2 = mu_0:(-v*dt):mu_middle;
%[h_middle,NUM] = f_ntem_histgram_count2(loop,mu_2,dt,epsilon,sqrt(mu_0),-sqrt(mu_middle),T_min,0.001,0);
%H_middle = hist(h_middle,C)./loop./dC;
Y = 0:0.01:10;
%T_stable = ones(1,length(Y)).*sqrt(mu_middle);
%T_unstable = ones(1,length(Y)).*(-sqrt(mu_middle));
%figure;
%plot(C,H_middle,'b-');hold on;
%plot(T_stable,Y,'r--');hold on;plot(T_unstable,Y,'r--');hold on;
%axis([-1 1 0 7]);
%pause;

mu_1 = mu_middle:(-v*dt):mu_n;
m = length(mu_1)
T_0 = -0.4;%-sqrt(mu_middle);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count2(loop,mu_1,dt,epsilon,T_0,-sqrt(mu_n),T_min,0.001,0);
%NUM counts how many simulations have the endpoint smaller than the
%unstable boundary
%NUM
T_hist = hist(h,C)./loop./dC;
size(T_hist)
pause;
T_stable = ones(1,length(Y)).*sqrt(mu_n);
T_unstable = ones(1,length(Y)).*(-sqrt(mu_n));

figure;
plot(C,T_hist,'k-.');hold on;
plot(T_stable,Y,'r--');hold on;plot(T_unstable,Y,'r--');hold on;
plot(T_0,0,'ko');hold on;
grid on;

%compare the final pdf with pdf of Brownian motion centered at T_0
time = (mu_middle-mu_n)/v;
T = -5:dT:1;
T_cen = T_0;%-0.08
p_b = zeros(1,length(T)); %define the PDF similar to Brownian motion
for i=1:length(T);
    p_b(i) = exp(-(T(i)-T_cen)^2/2/epsilon^2/time)/epsilon/sqrt(2*pi*time);
end;
plot(T,p_b,'r-');
pause;

%generate the pdf at mu_n by conditional pdf for T<-T_b and
%quasi-stationary for T>-T_b
T1 = -5:dT:-sqrt(mu_n)+dT; %range of T for conditional approximation
T2 = -sqrt(mu_n)+dT:dT:1; %range of T for quasi-stationary approximation
p_n1 = f_npdf_sym_local(mu_middle,mu_n,T1,T1,epsilon,beta);
p_n2 = f_npdf_sym6(mu_n,T2,epsilon,beta);
%normalization coeficient
C1 = trapz(T1,p_n1./p_n1(end).*p_n2(1));
C2 = trapz(T2,p_n2);
C3 = C1+C2;
figure;
plot(T1,p_n1./p_n1(end).*p_n2(1)./C3,'b-');hold on;
plot(T2,p_n2./C3,'r-');hold on;
%compare this approximation with numerical simulation
mu_3 = mu_0:(-v*dt):mu_n;
[h_middle,NUM] = f_ntem_histgram_count2(loop,mu_3,dt,epsilon,sqrt(mu_0),-sqrt(mu_n),T_min,0.001,0);
H_n = hist(h_middle,C)./loop./dC;
plot(C,H_n,'k-.');hold on;
%compare with the approximation only based on asymptotic approximation
T_3 = -5:dT:1;
p_sym_n = f_npdf_sym6(mu_n,T_3,epsilon,beta);
int_n = trapz(T_3,p_sym_n);
plot(T_3,p_sym_n./int_n,'b-.');

%use new function f_npdf_sym_local_one to see if the results still fit for
%one time stage (five time steps)
p_sym_middle = f_npdf_sym6(mu_middle,T_3,epsilon,beta);
p_sym_one = f_npdf_sym_local_one(mu_middle,mu_n,T_3,p_sym_middle,epsilon,beta);
int_one = trapz(T_3,p_sym_one);
figure;
plot(C,H_n,'k-.');hold on;
plot(T_3,p_sym_one/int_one,'r-');hold on;
plot(T_3,p_sym_n./int_n,'b-.');hold on;
plot(-sqrt(mu_n),0,'k*');hold on;
plot(-sqrt(mu_middle),0,'ro');