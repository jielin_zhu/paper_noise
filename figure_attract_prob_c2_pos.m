%check the CDF of numerical simulations with different asymmetric
%bifurcation structure corresponding to the change of A

%numerical simulations for the opposite asymmetry system
load file_PDF_full_num_A0.dat
[m1,n1] = size(file_PDF_full_num_A0);
T_asym = file_PDF_full_num_A0(1:m1-1,1);
mu_asym = file_PDF_full_num_A0(end,2:end);
pdf_asym_num = file_PDF_full_num_A0(1:m1-1,2:end);

%numerical simulations with smaller c2 c2 = origin c2*0.5
load file_PDF_asymc2_1.dat
[m2,n2] = size(file_PDF_asymc2_1);
T_asym2 = file_PDF_asymc2_1(1:m2-1,1);
mu_asym2 = file_PDF_asymc2_1(end,2:end);
pdf_asym_num2 = file_PDF_asymc2_1(1:m2-1,2:end);


%numerical simulations with smaller c2 c2 = origin c2+c1*0.5(c1<0)
load file_PDF_asymc2_3.dat
[m3,n3] = size(file_PDF_asymc2_3);
T_asym3 = file_PDF_asymc2_3(1:m3-1,1);
mu_asym3 = file_PDF_asymc2_3(end,2:end);
pdf_asym_num3 = file_PDF_asymc2_3(1:m3-1,2:end);
%}

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);%[SP(3) 0.2 0.07]; %different values of A
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = [-A2./(6.*A).^3 -A2./(6.*A).^3 -A2./(6.*A).^3];
c1 = [-4*A2/(6*A)^2 -4*A2/(6*A)^2 -4*A2/(6*A)^2]; %needs to be negative to fits the log term
c2 = [2*B2/(3*A*B1) 2*B2/(3*A*B1)*0.5 2*B2/(3*A*B1)+2.5];%2*B2/(3*A*B1)-c1(1)*0.5];
epsilon = 0.05;
%beta = 2.5;
v=0.0005;

%compute the CDF for the original case
CDF_asym_num = 1;
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0(1),c1(1),c2(1));
    T2(i) = R(2);
end;
Tb = zeros(2,length(mu_asym)); %bifurcation diagram of the system
for i=1:min(index_imag-1);
    R = f_n_fullroot(mu_asym(i),c0(1),c1(1),c2(1));
    Tb(1,i) = R(1);
    Tb(2,i) = R(2);
end;
for i=1:length(mu_asym);
    
    index_T = min(find(T_asym>T2(i)));
    
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
end;
%generate the concentration region for P=0.95
%two series for upper and lower bound of T at each mu(i)
%satisfying prob(T<Tl)=prob(T>Th)=0.05
Tl = zeros(length(mu_asym),1);
Th = zeros(length(mu_asym),1);
bound = 0.025;
for i=1:length(mu_asym);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T_asym,pdf_asym_num(:,i)); %computing the escaping probability below Td
    while j1<length(T_asym) && prob_upper<bound;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T_asym(end-j1:end)',pdf_asym_num(end-j1:end,i));
    end;
    %prob_upper
    Th(i) = T_asym(end-j1);
    while j2<length(T_asym) && prob_lower<bound; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T_asym,pdf_asym_num(:,i));
        prob_lower = X+trapz(T_asym(1:j2)',pdf_asym_num(1:j2,i));
    end;
    %prob_lower
    Tl(i) = T_asym(j2);
end;


%compute the CDF for the case with smaller c2
CDF_asym_num2 = 1;
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
Tb2 = zeros(2,length(mu_asym2)); %bifurcation diagram of the system
for i=1:min(index_imag-1);
    R = f_n_fullroot(mu_asym2(i),c0(2),c1(2),c2(2));
    Tb2(1,i) = R(1);
    Tb2(2,i) = R(2);
end;
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym2(i),c0(2),c1(2),c2(2));
    T2(i) = R(2);
end;
for i=1:length(mu_asym2);
    
    index_T = min(find(T_asym2>T2(i)));
    
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,i));
    CDF_asym_num2 = [CDF_asym_num2 P_asym_num];
    
    %{
    figure(5);
    plot(T_asym2,pdf_asym_num2(:,i));
    title(['\mu=',num2str(mu_asym2(i))]);
    hold off;
    pause;
    %}
end;
Tl2 = zeros(length(mu_asym2),1);
Th2 = zeros(length(mu_asym2),1);
bound = 0.025;
for i=1:length(mu_asym2);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T_asym2,pdf_asym_num2(:,i)); %computing the escaping probability below Td
    while j1<length(T_asym2) && prob_upper<bound;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T_asym2(end-j1:end)',pdf_asym_num2(end-j1:end,i));
    end;
    %prob_upper
    Th2(i) = T_asym2(end-j1);
    while j2<length(T_asym2) && prob_lower<bound; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T_asym2,pdf_asym_num2(:,i));
        prob_lower = X+trapz(T_asym2(1:j2)',pdf_asym_num2(1:j2,i));
    end;
    %prob_lower
    Tl2(i) = T_asym2(j2);
end;

%compute the CDF for the case with smaller c2
CDF_asym_num3 = 1;
T2 = zeros(1,length(mu_asym3)); %need to be computed for each mu(i)
index_imag = find(mu_asym3<0);
Tb3 = zeros(2,length(mu_asym3)); %bifurcation diagram of the system
for i=1:min(index_imag-1);
    R = f_n_fullroot(mu_asym3(i),c0(3),c1(3),c2(3));
    Tb3(1,i) = R(1);
    Tb3(2,i) = R(2);
end;
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym3(i),c0(3),c1(3),c2(3));
    T2(i) = R(2);
end;
for i=1:length(mu_asym3);
    
    index_T = min(find(T_asym3>T2(i)));
    
    P_asym_num = trapz(T_asym3(index_T:end),pdf_asym_num3(index_T:end,i));
    CDF_asym_num3 = [CDF_asym_num3 P_asym_num];
    
    %{
    figure(5);
    plot(T_asym2,pdf_asym_num2(:,i));
    title(['\mu=',num2str(mu_asym2(i))]);
    hold off;
    pause;
    %}
end;
Tl3 = zeros(length(mu_asym3),1);
Th3 = zeros(length(mu_asym3),1);
bound = 0.025;
for i=1:length(mu_asym3);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T_asym3,pdf_asym_num3(:,i)); %computing the escaping probability below Td
    while j1<length(T_asym3) && prob_upper<bound;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T_asym3(end-j1:end)',pdf_asym_num3(end-j1:end,i));
    end;
    %prob_upper
    Th3(i) = T_asym3(end-j1);
    while j2<length(T_asym3) && prob_lower<bound; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T_asym3,pdf_asym_num3(:,i));
        prob_lower = X+trapz(T_asym3(1:j2)',pdf_asym_num3(1:j2,i));
    end;
    %prob_lower
    Tl3(i) = T_asym3(j2);
end;

Fig = figure;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym_num(2:end),'b*--','linewidth',1.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,CDF_asym_num2(2:end),'ro--','linewidth',1.5);hold on;
axis([mu_e mu_asym(1)*8/3/A/B1+mu_e 0.4 1.1]);
set(gca,'fontsize',18);
print(Fig,'-depsc','figure_CDF_pos_c2_1.eps')

Fig2 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,(Tl+1).*T_e,'b--','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Th+1).*T_e,'b--','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Tb(1,:)+1).*T_e,'b-','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Tb(2,:)+1).*T_e,'b-.','linewidth',1.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,(Tl2+1).*T_e,'r--','linewidth',2.5);hold on;
plot(mu_asym2(1:5:end).*8/3/A/B1+mu_e,(Tl2(1:5:end)+1).*T_e,'r^','linewidth',2.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,(Th2+1).*T_e,'r--','linewidth',2.5);hold on;
plot(mu_asym2(1:5:end).*8/3/A/B1+mu_e,(Th2(1:5:end)+1).*T_e,'r^','linewidth',2.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,(Tb2(1,:)+1).*T_e,'r-','linewidth',2.5);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,(Tb2(2,:)+1).*T_e,'r-.','linewidth',2.5);hold on;
axis([mu_e mu_asym(1)*8/3/A/B1+mu_e -100 380]);
set(gca,'fontsize',18);
print(Fig2,'-depsc','figure_attract_prob_pos_c2_1.eps')

Fig3 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym_num(2:end),'b*--','linewidth',1.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,CDF_asym_num3(2:end),'ro--','linewidth',1.5);hold on;
axis([mu_e mu_asym(1)*8/3/A/B1+mu_e 0.4 1.1]);
set(gca,'fontsize',18);
print(Fig3,'-depsc','figure_CDF_pos_c2_3.eps')

Fig4 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,(Tl+1).*T_e,'b--','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Th+1).*T_e,'b--','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Tb(1,:)+1).*T_e,'b-','linewidth',1.5);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,(Tb(2,:)+1).*T_e,'b-.','linewidth',1.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,(Tl3+1).*T_e,'r--','linewidth',2.5);hold on;
plot(mu_asym3(1:5:end).*8/3/A/B1+mu_e,(Tl3(1:5:end)+1).*T_e,'r^','linewidth',2.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,(Th3+1).*T_e,'r--','linewidth',2.5);hold on;
plot(mu_asym3(1:5:end).*8/3/A/B1+mu_e,(Th3(1:5:end)+1).*T_e,'r^','linewidth',2.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,(Tb3(1,:)+1).*T_e,'r-','linewidth',2.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,(Tb3(2,:)+1).*T_e,'r-.','linewidth',2.5);hold on;
axis([mu_e mu_asym(1)*8/3/A/B1+mu_e -150 400]);
set(gca,'fontsize',18);
print(Fig4,'-depsc','figure_attract_prob_pos_c2_3.eps')