%Compare the shape of the PDF based on leading order Brownian motion for
%alpha>4/3 and the PDF given by quasi-stationary solution
%seperate the numerical simulation by two different systems and see the
%final behaviour

%introduce all the parameters for the symmetric system
mu_0 = 0.1;
mu_n = 0.03;
epsilon = 0.05;
beta = 2.5;
mu_middle = 0.035;%epsilon^(4/3);
v = epsilon^beta;
dT = 0.001;
T_b = -sqrt(mu_middle)+dT;
T_b1 = -sqrt(mu_n)+dT;

%define the amplitude coefficient by initial pdf
T_r1 = T_b1:dT:1;
[p_0 p_LO_0] = f_npdf_sym5(mu_0,T_r1,epsilon,beta);
int_0 = trapz(T_r1,p_0);
int_LO_0 = trapz(T_r1,p_LO_0);

%compute the time-dependent PDF for T below unstable branch using brownian
%motion
time = (mu_middle-mu_n)/v;
T = -5:dT:1;
p_b = zeros(1,length(T)); %define the PDF similar to Brownian motion
for i=1:length(T);
    p_b(i) = exp(-(T(i)-sqrt(mu_middle))^2/2/epsilon^2/time)/epsilon/sqrt(2*pi*time);
end;
int_b = trapz(T,p_b)

%compute the time-dependent pdf at mu_n near the unstable branch and
%combine it with the asymptotic approximation
T1 = -5:dT:-sqrt(mu_n)+dT;
p_partial = f_npdf_sym_local(mu_middle,mu_n,T1,T,epsilon,beta);
int_partial = trapz(T1,p_partial/int_0)
%T_k = -0.11;
%Con = f_npdf_sym_local_C(mu_middle,mu_n,T_k,epsilon,beta);
%p_maj = f_npdf_sym6(mu_n,T,epsilon,beta);
%p_com = zeros(1,length(T)); %matching the local approximation with the asymptotic one
%for i=1:length(T);
    %if T(i)<T_k;
        %p_com(i) = p_partial(i)*Con/int_0;
    %else
        %p_com(i) = p_maj(i)/int_0;
    %end;
%end;
%int_con = trapz(T,p_com)

%check the time-dependent PDF with a different time length
%time2 = (0.2-mu_n)/v;
%p_2 = zeros(1,length(T));
%for i=1:length(T);
    %p_2(i) = exp(-T(i)^2/2/epsilon^2/time2)/epsilon/sqrt(2*pi*time2);
%end;


%generate the normalized PDF based on quasi-stationary solution at
%mu_n
T_r = T_b:dT:1;
[p_sym p_LO] = f_npdf_sym5(mu_n,T_r1,epsilon,beta);
int_LO = trapz(T_r1,p_LO);
p_sym(1) %check whether at mu_middle the early tipping started
%int_sym = trapz(T_r,p_sym);

%generate the normalized PDF based on quasi-stationary solution at
%mu_middle
T_r2 = -sqrt(mu_0):dT:1;
p_sym_n = f_npdf_sym4(mu_middle,T_r,epsilon,beta);
p_LO_n = f_npdf_sym_LO(mu_middle,T_r2,epsilon,beta);
p_sym_n(1) %check whether at mu_middle the early tipping started
%int_sym = trapz(T_r,p_sym);
%[p_0 p_LO_0] = f_npdf_sym5(mu_0,T_r,epsilon,beta);
%int_0 = trapz(T_r,p_0);
int_LO_n = trapz(T_r2,p_LO_n);


%generate the PDF numerically stop at mu_n
loop = 10000;
dC = 1/200; %length of bin for histgram;
dt = 0.1;
mu_1 = 0.1:(-v*dt):mu_n;
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
%NUM counts how many simulations have the endpoint smaller than the
%unstable boundary
C = T_min-dC:dC:2;
T_hist = hist(h,C)./loop./dC;
NUM

%generate the PDF numerically by using mu1 which stopps at mu_middle in the
%full system and mu2 which stopps at mu_n in the simple brownian motion
mu1 = 0.1:(-v*dt):mu_middle;
mu2 = mu1(length(mu1)):(-v*dt):mu_n;
%[h_check NUM_check] = f_conditional_hist(loop,mu1,mu2,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
[h_check NUM_check] = f_conditional_hist2(loop,mu1,mu2,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
T_check = hist(h_check,C)./loop./dC;
NUM_check

%generate the histgram to compare with the asymptotic approximation at
%mu_middle
[h_middle NUM_middle] = f_ntem_histgram_count(loop,mu1,dt,epsilon,-sqrt(mu_middle),T_min,0.001,0);
hist_middle = hist(h_middle,C)./loop/dC;
NUM_middle

%generate the Gaussian approximation
%p_gau = f_gau_pdf(mu_1,v,dt,T,epsilon);

%try normal distribution to fit the left tail, try variance computed from
%the asymptotic approximation at mu_n
T_r3 = -5:dT:1;
p_normal = zeros(1,length(T_r3));
[mean,sigma,gamma] = f_skewness(T_r1,p_sym/int_0);
sigma = 0.08;
for i=1:length(T_r3);
    p_normal(i) = exp(-(T_r3(i)-mean)^2/2/sigma^2)/sqrt(2*pi*sigma^2);
end;
%note: larger variance doesn't provide better fit for the left tail

%also sketch the unstable boundary for our approximation
Y = 0:0.1:15;
l = length(Y);
L = ones(1,l).*(-sqrt(mu_n));

figure(1);
%plot(T,p_b,'b-');
%hold on;
plot(C,T_hist,'k-.');
hold on;plot(C,T_check,'r:');
%hold on;plot(T_r1,p_sym/int_0,'k-');
%hold on;plot(T_r1,p_LO/int_LO,'b-');
hold on;plot(T_r2,p_LO_n/int_LO_n,'r-');
hold on;plot(T,p_b,'b-.');
hold on;plot(T1,p_partial/int_0,'b-');
%hold on;plot(T_r3,p_normal,'b-');
%hold on;plot(T,p_gau,'b-');
%hold on;plot(T,p_com,'k-');
%hold on;plot(T_k,0,'k*');
hold on;plot(L,Y,'r:');
%axis([-0.4 0.6 0 7]);

%figure(2);
%plot(C,hist_middle,'k-.');
%hold on;plot(T_r,p_sym_n/int_0,'b-');
%hold on;plot(T_r2,p_LO_n/int_LO_n,'r-');
%hold on;plot(L,Y,'r:');
axis([-1 1 0 7]);
