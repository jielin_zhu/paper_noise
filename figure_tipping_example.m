%generate the figure in Section model to show the bifurcation structure,
%delay effect of the drifting rate and early tipping caused by large noise

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%set up parameter values as drifting rate and noise
epsilon = 0.05;
beta = 3;
v = epsilon^beta;
dt = 0.1;
mu_s = 1:(-v*dt):-1;
m = length(mu_s);
%n = max(find(mu>0));
mu = 0:0.001:1;
n = length(mu);

%{
T_phase = -2:0.01:1;
mu_phase = -(c0.*T_phase.^4+c1.*T_phase.^3-T_phase.^2)./(c2.*T_phase+1);
figure(1);
plot(mu_phase,T_phase);
pause;
%}

%compute equilibirum of the normalized temperature asymmetric system
T = zeros(2,n);
for i=1:n;
    R = f_n_fullroot(mu(i),c0,c1,c2);
    %pause;
    T(1,i) = R(1);
    T(2,i) = R(2);
    %T(1,i) = R(3);
end;

%compute equilibrium of the system with symmetric branch by making T =
%sqrt(B1*mu/96/A)
T1 = zeros(2,n);
for i=1:n;
    T1(1,i) = sqrt(mu(i));
    T1(2,i) = -sqrt(mu(i));
end;

%find the trajectory of the system without noise
T_s = zeros(1,m);
T_s(1) = T(1,end);
%{
c0 = 0;
c1 = 0;
c2 = 0;
%}
for i=2:m;
    K1 = c0*T_s(i-1)^4+c1*T_s(i-1)^3-T_s(i-1)^2+c2*mu_s(i-1)*T_s(i-1)+mu_s(i-1);
    X_pre = T_s(i-1)+K1*dt;
    K2 = c0*X_pre^4+c1*X_pre^3-X_pre^2+c2*mu_s(i)*X_pre+mu_s(i);
    T_s(i) = T_s(i-1)+(K1+K2)*dt/2;
end;
index_escape = find(T_s<-2);
T_s(index_escape) = -2;
Y = -2:0.1:2;
mu_p = v^(2/3)*(-2.33810).*ones(length(Y),1);

%generate simulation with noise
T_n = zeros(m,1);
T_n(1) = T_s(1);
dW = randn(m,1);
for i=2:m;
    T_n(i) = T_n(i-1)+(c0*T_n(i-1)^4+c1*T_n(i-1)^3-T_n(i-1)^2+c2*mu_s(i-1)*T_n(i-1)+mu_s(i-1))*dt+epsilon*sqrt(dt)*dW(i);
end;
index_escape = find(T_n<-2);
T_n(index_escape) = -2;

%need to compare the bifurcation diagram along symmetric, asymmetric and
%opposite symmetric case
%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = 0;%-A2/(6*A)^3;
c1 = 4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = -2*B2/(3*A*B1);%2*B2/(3*A*B1);
T_oppo = zeros(2,n);
for i=1:n;
    R = f_n_fullroot(mu(i),c0,c1,c2);
    %pause;
    %T_oppo(1,i) = R(1);
    T_oppo(2,i) = R(2);
    T_oppo(1,i) = R(3);
end;
T_n_oppo = zeros(m,1);
T_n_oppo(1) = T_s(1);
dW = randn(m,1);
for i=2:m;
    T_n_oppo(i) = T_n_oppo(i-1)+(c0*T_n_oppo(i-1)^4+c1*T_n_oppo(i-1)^3-T_n_oppo(i-1)^2+c2*mu_s(i-1)*T_n_oppo(i-1)+mu_s(i-1))*dt+epsilon*sqrt(dt)*dW(i);
end;

Fig = figure;
plot(mu,T1(1,:),'m-','LineWidth',2);
hold on;plot(mu,T1(2,:),'m-.','LineWidth',2);
hold on;plot(mu,T(1,:),'k-','LineWidth',2);hold on;plot(mu,T(2,:),'k-.','LineWidth',2);
hold on;plot(mu,T_oppo(1,:),'g-','LineWidth',2);hold on;plot(mu,T_oppo(2,:),'g-.','LineWidth',2);hold on;
hold on;plot(mu_s,T_s,'r--','linewidth',1.5);hold on;
hold on;plot(mu_s,T_n,'b:','linewidth',1);hold on;
hold on;plot(mu_s,T_n_oppo,'b:','linewidth',1);
%hold on;plot(mu_p,Y,'g-');
xlabel('$$\mu$$','FontSize',40,'interpreter','latex');
ylabel('$$T$$','FontSize',40,'interpreter','latex');
set(gca,'fontsize',18);
axis([-0.02 0.2 -0.8 0.8]);
%print(Fig,'-depsc','figure_tipping_example.eps')