% compute PDF for the symmetric case for alpha that larger than the
% limitation beta+3alpah/2<4

function p_sym = f_npdf_sym2(mu,T,epsilon,beta)
% p returns to series of pdf with domain of fully normalized temperature
% mu is the ending point for series of mu
% epsilon is the amplitude of the nosie
% T is the series as domain of the pdf, changing with different mu and
% lower boundary is -T2(mu)
% beta reflect the relationship of drift rate and noise

n1 = length(T);
f1_sym = log(mu)/4;%-epsilon^(beta-2)*log(4)
f2_sym = epsilon^2*(-5*mu^(-3/2)/96);
f3_sym = epsilon^4*(-5/256*mu^(-3));
%exp_psi0_sym = zeros(1,n1);
psi0_sym = zeros(1,n1);
psi1_sym = zeros(1,n1);
psi3_sym = zeros(1,n1);
p_sym = zeros(1,n1);

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    psi1_sym(i) = epsilon^(beta-2)*(2*log((sqrt(mu)+T(i)))-log(mu));
    psi3_sym(i) = epsilon^beta*(-1/(T(i)+sqrt(mu))-sqrt(mu)/(T(i)+sqrt(mu))^2)/4/mu+epsilon^beta*3*mu^(-3/2)/8;
    %exp_psi0_sym(i) = exp(psi0_sym(i)/epsilon^2);
    p_sym(i) = exp(psi0_sym(i)/epsilon^2+psi1_sym(i)+psi3_sym(i)+f1_sym+f2_sym+f3_sym);
end;

if isreal(psi1_sym)==0
    error('Something wrong with psi0_sym, doulbe check T_r');
end;

figure(5);
plot(T,psi3_sym);
%figure(6);
%plot(T,exp(psi3_sym));%axis([min(T) max(T) -5 5]);
pause;