%check whether the CDF based on the asymptotic approximation for the
%symmetric case fits with the results from numerical simulations

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -0.001;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 0;%2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.02;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 2.2;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.1;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 1000;
dC = 1/200; %length of bin for histgram;
dT = 0.001;

mu = mu_0:-0.02:mu_n;
%mu = 0.3;
n = length(mu);

%generate related bifurcation diagram
%mu_1 = mu_0:-0.001:0;
%m = length(mu_1);
%T = zeros(3,m); %bifurcation diagram for the normalized truncated model
%T1 = zeros(2,m); %bifurcation diagram for the normalized symmetric model
%for i=1:m;
    %R = f_n_root(mu_1(i),c1,c2);
    %pause;
    %T(1,i) = R(1);
    %T(2,i) = R(2);
    %T(3,i) = R(3);
    %T1(1,i) = sqrt(mu_1(i));
    %T1(2,i) = -sqrt(mu_1(i));
%end;
%figure;
%plot(mu_1,T1(1,:),'Color','r','LineWidth',2,'LineStyle','-.');
%hold on;plot(mu_1,T1(2,:),'Color','r','LineWidth',2,'LineStyle',':');
%hold on;plot(mu_1,T(1,:),'Color','k','LineWidth',2);
%hold on;plot(mu_1,T(2,:),'Color','b','LineStyle','--','LineWidth',2);
%hold on;plot(mu_1,T(3,:),'g-.','Linewidth',2);grid on;
%axis([0 0.1 -0.6 1.4]);
%xlabel('$$ \mu$$','fontsize',20,'interpreter','latex');
%ylabel('$$ T $$','fontsize',20,'interpreter','latex');
%pause;

%calculate CDF
%p_0 = f_a_pdf_test_3(mu_0,mu_0,T_r_0,A,B1,epsilon,beta);
%A_0 = trapz(T_r_0,p_0);
%int_a = zeros(1,n);
int_sym = zeros(1,n); %cdf for symmetric case
int_origin = zeros(1,n);
sim_cdf = zeros(1,n);
int_gau = zeros(1,n);%cdf for gaussian approximation
Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case
%N = 1;
for i=1:n;
    %mu(i)
    root = f_n_root(mu(i),c1,c2);
    %root(1)
    %if c1>0 && c2<=0
        %T_r = root(2)+0.001:0.001:root(3)-0.001; %define the domain of PDF for every different mu(i)
    %else
        %T_r = root(2)+0.001:0.001:2; %for the case c1<0 and c2>0
    %end;
    %T_r_sym = -sqrt(mu(i))+dT:dT:2;
    T_r_sym = -0.12:dT:2;
    %p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta); 
    %[p_a p_sym] = f_a_npdf_compare(mu(i),T_r,c1,c2,epsilon,beta); 
    %p_a = f_a_npdf(mu(i),T_r,c1,c2,epsilon,beta); 
    %p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    p_sym2 = f_npdf_sym4(mu(i),T_r_sym,epsilon,beta);
    %int_a(i) = trapz(T_r,p_a);
    %int_a(i) = trapz(T_r,p_a);
    int_sym(i) = trapz(T_r_sym,p_sym2);
    int_origin(i) = trapz(T_r_sym,p_sym);
    %figure;plot(T_r_sym,p_sym/int_origin(1),'r-');hold on;plot(T_r_sym,p_sym2/int_sym(1),'b-.');hold on;
    
    %compare the CDF curve with histogram from numerical simulations
    mu_1 = 0.5:(-v*dt):mu(i);
    m = length(mu_1);
    T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
    [h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
    %[NUM mu(i)]
    sim_cdf(i) = 1-NUM/loop;
    C = T_min:dC:2;
    T_hist = hist(h,C)./loop./dC;
    %plot(C,T_hist,'ro');
    %axis([0 2 0 max(p_a./int_a(i))]);
    %pause;
    
    %compute the CDF by Gaussian approximation
    p_gau = f_gau_pdf(mu_1,v,dt,T_r_sym,epsilon);
    int_gau(i) = trapz(T_r_sym,p_gau);
end;

Fig = figure;
%plot(mu,int_a/int_a(1),'b-','LineWidth',2);
%plot(mu,int_a/max(int_a)*max(int_sym)*Con,'b-','LineWidth',2);
%hold on;
%plot(mu,int_sym*Con,'r-.','LineWidth',2);
plot(mu,int_sym/int_sym(1),'b-.','LineWidth',2);hold on;
plot(mu,int_origin/int_origin(1),'r-.','Linewidth',2);hold on;
%plot(mu,int_origin*Con,'r-','Linewidth',2);
%plot(mu,INT_a,'b-',mu,int_sym*Con,'r-.','LineWidth',2);
hold on;plot(mu,sim_cdf,'k:','LineWidth',2);
hold on;plot(mu,int_gau,'g:','LineWidth',2);
title(['$$\epsilon = $$',num2str(epsilon),'; $$\beta =$$ ',num2str(beta),'; $$v$$= ',num2str(V)],'fontsize',20,'interpreter','latex')
%axis([0 0.05 0.85 1.05])
xlabel('$$\mu$$','fontsize',20,'interpreter','latex');

figure;
plot(T_r_sym,p_sym/int_origin(1),'r-.','Linewidth',2);
hold on;plot(T_r_sym,p_sym2/int_sym(1),'b-.','Linewidth',2);
hold on;plot(C,T_hist,'ko');
axis([-0.1 0.3 0 12]);
%print(Fig,'-depsc','figure_cdf_compare4.eps')
