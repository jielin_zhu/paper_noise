%find roots of the fully normalized temperature model
function T = f_n_root(mu,c1,c2)

% T gives the absolute value for three root
% mu is the normalized version of one specific mu_t

% T = zeros(3,1); %sure there're three real roots
C1 = c1;
C2 = -1;
C3 = c2*mu;
C4 = mu;
C = [C1 C2 C3 C4];
root = roots(C);

%separate how to order the roots by the sign of c1 and c2
if isreal(root)==0
    error('The values for c1 and c2 are inproper');
else
    if c1>0 && c2<=0;
        R = sort(root,'descend');
        T(1) = R(2); %stable equilibrium
        T(2) = R(3); %unstable equilibrium
        T(3) = R(1);
    else
        T = sort(root,'descend');
    end;
end;