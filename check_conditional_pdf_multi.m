% between mu_middle and mu_n, use multiple f_npdf_sym_local_one to update
% the pdf until mu_n

tic
%introduce initial parameter values
mu_0 = 0.05;
%mu_n = 0.025;
epsilon = 0.05;
beta = 2.5;
v = epsilon^beta;
mu_n = 0.025;
mu_middle = 0.04;%mu_n+v*5 %0.03;%epsilon^(4/3);
%pause;
%dT = 0.0001;
T_min = -5;
%T = T_min:dT:1;
%redifine range T to make sure enough small steps between -sqrt(mu_middle)
%and -sqrt(mu_n)
%T1 = T_min:0.001:-sqrt(mu_middle);
%T2 = -sqrt(mu_middle)+0.0001:0.0001:-sqrt(mu_n);
%T3 = -sqrt(mu_n)+0.0001:0.001:1;
%T = [T1 T2 T3];
%T = T_min:0.001:1; %without using transit matrix, smaller steps between two unstable equilibria is unnecessary
dC = 1/200; %length of bin for histgram;
C = T_min-dC:dC:2;
T = T_min-dC:0.001:2;
loop = 10000;
loop1 = 1000;
loop2 = 1000;
dt = 0.1;

%numerical simulation to see whether mu_middle is proper
%mu_2 = mu_0:(-v*dt):mu_middle;
%[h_middle,NUM_middle] = f_ntem_histgram_count(loop,mu_2,dt,epsilon,-sqrt(mu_middle),T_min,0.001,0);
%NUM_middle
%pause

%estimate pdf by multiple conditional pdf and asymptotic approximation
%mu = mu_middle:(-v):mu_n; %set the stages by v*0.5
mu = [mu_middle mu_n];
p_previous = f_npdf_sym6(mu_middle,T,epsilon,beta);%pdf at mu_middle
%figure;plot(T,p_previous,'b-');hold on;plot(-sqrt(mu_middle),0,'ro');axis([-sqrt(mu_middle)-0.05 -sqrt(mu_middle)+0.05 0 0.1]); %check whether mu_middle is good enough
%pause;
p_previous_full = f_npdf_sym6(mu_middle,T,epsilon,beta);
p_previous_brownian = p_previous;
T_s = -sqrt(mu)+0.02;
for i=2:length(mu);
    %p_sym = f_npdf_sym_local_one(mu(i-1),mu(i),T,T_s(i),p_previous,epsilon,beta);
    p_sym = f_npdf_sym_local_sim(mu(i-1),mu(i),T,T_s(i),T_min,p_previous,epsilon,beta,dt,loop1);
    p_sym_full = f_npdf_sym_local_sim2(mu(i-1),mu(i),T,T_min,p_previous_full,epsilon,beta,dt,loop2); %only use conditional probability
    p_brownian = f_npdf_sym_local_brownian(mu(i-1),mu(i),T,T_s(i),p_previous_brownian,epsilon,beta,dt,loop1);
    p_previous = p_sym;
    p_previous_full = p_sym_full;
    p_previous_brownian = p_brownian;
    int_p = trapz(T,p_sym);
    int_p_full = trapz(T,p_sym_full);
    int_p_brownian = trapz(T,p_brownian);
    %figure(1);plot(T,p_sym./int_p,'b-.');hold on;plot(T_s(i),0,'ko');pause;
end;
%also count the probability of T smaller than -sqrt(mu_n);
N = 1;
while N <length(T) && T(N)<-sqrt(mu_n);
    N = N+1;
end;
prob_tip = trapz(T(1:N),p_sym(1:N))/int_p
prob_tip_full = trapz(T(1:N),p_sym_full(1:N))/int_p_full
prob_tip_brownian = trapz(T(1:N),p_brownian(1:N))/int_p_brownian
toc

%compare with numerical simulation
mu_3 = mu_0:(-v*dt):mu_n;
[h_n,NUM,NUM_tipping] = f_ntem_histgram_count3(loop,mu_3,dt,epsilon,sqrt(mu_0),-sqrt(mu_n),T_min,0.001,0);
H_n = hist(h_n,C)./loop./dC;
M = 1;
while M <length(C) && C(M)<-sqrt(mu_n);
    M = M+1;
end;
prob_sim = trapz(C(1:M),H_n(1:M))%NUM/loop
prob_early = NUM_tipping/loop

%compare with the results based only on asymptotic approximation
p_sym_n = f_npdf_sym6(mu_n,T,epsilon,beta);
int_n = trapz(T,p_sym_n);

figure;
plot(T,p_sym./int_p,'b-');hold on;
plot(T,p_sym_full./int_p_full,'r--');hold on;
%plot(T,smooth(p_sym./int_p),'b-');hold on;
plot(C,H_n,'k:');hold on;
%plot(T,p_sym_n./int_n,'r--');hold on;
plot(T,p_brownian./int_p_brownian,'m-.');hold on;
plot(-sqrt(mu_n),0,'k*');hold on;
plot(-sqrt(mu_middle),0,'ro');