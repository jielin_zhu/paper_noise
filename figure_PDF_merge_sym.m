%load files for pdf of simplified symmetric system
%generate the figure to compare the PDF approximation above T2 in the full
%system based on matching asymptotics, numerical simulation and gaussian
%approximation

%{
load file_PDF_asym_merge.dat
[m1,n1] = size(file_PDF_asym_merge);
T_asym = file_PDF_asym_merge(1:m1-1,1);
mu_asym = file_PDF_asym_merge(end,2:end);
pdf_asym_merge = file_PDF_asym_merge(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num.dat
pdf_asym_num = file_PDF_asym_num(1:m1-1,2:end);
load file_PDF_sym_merge.dat
[m2,n2] = size(file_PDF_sym_merge);
T_sym = file_PDF_sym_merge(1:m2-1,1);
mu_sym = file_PDF_sym_merge(end,2:end);
pdf_sym_merge = file_PDF_sym_merge(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_sym_gau.dat
pdf_sym_gau = file_PDF_sym_gau(1:m2-1,2:end);
%}

%load merged pdf based on full leading order approximation
load file_PDF_asym_merge3.dat
[m1,n1] = size(file_PDF_asym_merge3);
T_asym = file_PDF_asym_merge3(1:m1-1,1);
mu_asym = file_PDF_asym_merge3(end,2:end);
pdf_asym_merge = file_PDF_asym_merge3(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num3.dat
pdf_asym_num = file_PDF_asym_num3(1:m1-1,2:end);

%change to files with setting epsilon=0.05,beta=2.5
%{
load file_PDF_asym_merge2.dat
[m1,n1] = size(file_PDF_asym_merge2);
T_asym = file_PDF_asym_merge2(1:m1-1,1);
mu_asym = file_PDF_asym_merge2(end,2:end);
pdf_asym_merge = file_PDF_asym_merge2(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num2.dat
pdf_asym_num = file_PDF_asym_num2(1:m1-1,2:end);
%}
load file_PDF_sym_merge2.dat
[m2,n2] = size(file_PDF_sym_merge2);
T_sym = file_PDF_sym_merge2(1:m2-1,1);
mu_sym = file_PDF_sym_merge2(end,2:end);
pdf_sym_merge = file_PDF_sym_merge2(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_sym_num2.dat
pdf_sym_num = file_PDF_sym_num2(1:m2-1,2:end);
load file_PDF_sym_gau2.dat
pdf_sym_gau = file_PDF_sym_gau2(1:m2-1,2:end);

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
beta = 2.5;
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0,c1,c2);
    T2(i) = R(2);
end;
Ts = T2+10*epsilon;
Td = (T2-2*epsilon).*ones(1,length(mu_asym));

%{
%check the pdf at each mu(i)
for i=2:length(mu_asym);
    if mu_asym(i)>0
    R = f_n_root(mu_asym(i),c1,c2);
    T1 = R(1);
    T2 = R(2);
    T_s = sqrt(mu_asym(i));
    else
        T_s = 0.2;
    end;
    figure(1);
    plot(T_asym,pdf_asym_merge(:,i),'b-');hold on;
    plot(T_asym,pdf_asym_num(:,i),'r-.');hold on;
    plot(T_sym,pdf_sym_merge(:,i),'k-');hold on;
    plot(T_sym,pdf_sym_gau(:,i),'m:');hold on;
    plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
    hold off;
    axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu_asym(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}
    
CDF_asym = 1;
CDF_asym_num = 1;
CDF_sym = 1;
CDF_sym_gau = 1;
CDF_sym_num = 1;
for i=1:length(mu_asym);
    index_T = min(find(T_asym>T2(i)));
    
    P_asym = trapz(T_asym(index_T:end),pdf_asym_merge(index_T:end,i));
    CDF_asym = [CDF_asym P_asym];
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
    %{
    P_sym = trapz(T_sym(index_T:end),pdf_sym_merge(index_T:end,i));
    CDF_sym = [CDF_sym P_sym];
    P_sym_gau = trapz(T_sym(index_T:end),pdf_sym_gau(index_T:end,i));
    CDF_sym_gau = [CDF_sym_gau P_sym_gau];
    %}
end;
%separate the symmeric case from asymmetric one using the same T2
for i=1:length(mu_sym);
    index_T = min(find(T_sym>T2(i)));
    
    P_sym = trapz(T_sym(index_T:end),pdf_sym_merge(index_T:end,i));
    CDF_sym = [CDF_sym P_sym];
    P_sym_gau = trapz(T_sym(index_T:end),pdf_sym_gau(index_T:end,i));
    CDF_sym_gau = [CDF_sym_gau P_sym_gau];
    P_sym_num = trapz(T_sym(index_T:end),pdf_sym_num(index_T:end,i));
    CDF_sym_num = [CDF_sym_num P_sym_num];
end;
%also show the region where WKB breaks
Y = 0.4:0.01:1.1;
X = epsilon^(4/3).*ones(1,length(Y));

Fig = figure;
plot(mu_asym,CDF_asym(2:end),'b-','linewidth',2);hold on;
plot(mu_sym,CDF_sym(2:end),'r-.','linewidth',2);hold on;
plot(mu_asym,CDF_asym_num(2:end),'k*--','linewidth',1.5);hold on;
plot(mu_sym,CDF_sym_num(2:end),'k--','linewidth',2);hold on;
plot(mu_sym,CDF_sym_gau(2:end),'mo:','linewidth',1.5);hold on;
plot(X,Y,'b:','linewidth',2);hold on;
xlabel('$$\mu$$','FontSize',40,'interpreter','latex');
ylabel('$$Prob(T>-T_2)$$','FontSize',32,'interpreter','latex');
%title(['$$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta)],'fontsize',20,'interpreter','latex')
%legend('Asymmetric','Symmetric','Numerical','Gaussian')
axis([0 0.03 0.4 1.1]);
set(gca,'fontsize',18);
%print(Fig,'-depsc','figure_CDF_full.eps')

%generate PDF curve for relatively large or small mu
index_asym1 = max(find(mu_asym>0.015));
index_sym1 = max(find(mu_sym>0.015));
%generate unstable branch for asymmetric and symmetric case;
Y = 0:0.01:7;
T_asym2 = T2(index_asym1).*ones(length(Y),1);
Fig2 = figure;
%plot(T_asym,pdf_asym_merge(:,index_asym1),'b-','linewidth',2);hold on;
%plot(T_asym,pdf_asym_num(:,index_asym1),'ro','linewidth',1.5);hold on;
plot(T_sym,pdf_sym_merge(:,index_sym1),'k--','linewidth',2);hold on;
plot(T_sym,pdf_sym_gau(:,index_sym1),'m-.','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);
%plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
hold off;
axis([-0.3 0.4 0 7]);
title(['$$\mu$$= ',num2str(mu_asym(index_asym1))],'fontsize',20,'interpreter','latex');
%print(Fig2,'-depsc','figure_PDF_merge1.eps')

index_asym2 = max(find(mu_asym>0.005));
index_sym2 = max(find(mu_sym>0.005));
%generate unstable branch for asymmetric and symmetric case;
Y = 0:0.01:7;
%T_asym2 = T2(index_asym2).*ones(length(Y),1);
T_asym2 = -sqrt(mu_sym(index_sym2)).*ones(length(Y),1);
t=2;
xd = -sqrt((mu_sym(index_sym2)+mu*t))-2*epsilon;
xm = -sqrt((mu_sym(index_sym2)+mu*t))+epsilon;
xs = -sqrt((mu_sym(index_sym2)+mu*t))+10*epsilon;
Fig3 = figure;
%plot(T_asym,pdf_asym_merge(:,index_asym2),'b-','linewidth',2);hold on;
%plot(T_asym,pdf_asym_num(:,index_asym2),'ro','linewidth',1.5);hold on;
plot(T_sym,pdf_sym_merge(:,index_sym2),'k--','linewidth',2);hold on;
plot(T_sym,pdf_sym_gau(:,index_sym2),'m-.','linewidth',2);hold on;
plot(T_sym,pdf_sym_num(:,index_sym2),'ro','linewidth',2);hold on;
plot(T_asym2,Y,'b:','linewidth',2);hold on;
%plot(Ts(index_asym2-1),0,'k*','markersize',8,'linewidth',2);hold on;
%plot(Td(index_asym2),0,'b*','markersize',8,'linewidth',2);
%plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;plot(T_s,0,'k*');
plot(xd,0,'ro','markerface','r','markersize',8);hold on;plot(xm,0,'b^','markerface','b','markersize',8);hold on;plot(xs,0,'ko','markerface','k','markersize',8);
hold off;
xlabel('$$x$$','fontsize',40,'interpreter','latex');
ylabel('$$p(x,a)$$','fontsize',40,'interpreter','latex');
axis([-0.5 0.45 0 5]);
set(gca,'fontsize',18);
title(['$$a$$= ',num2str(mu_sym(index_sym2))],'fontsize',40,'interpreter','latex');
print(Fig3,'-depsc','figure_PDF_merge_sym.eps')


%{
%compare the above results with previous results for the asymmetric case
load file_PDF_merge_asym.dat
[m3,n3] = size(file_PDF_merge_asym);
T_pre = file_PDF_merge_asym(1:m3-1,1);
mu_pre = file_PDF_merge_asym(end,2:end);
pdf_asym_pre = file_PDF_merge_asym(1:m3-1,2:end); %each column is pdf at mu(i)
load file_PDF_num_asym.dat
pdf_num_pre = file_PDF_num_asym(1:m3-1,2:end);
CDF_asym_pre = 1;
CDF_num_pre = 1;
T2_pre = zeros(1,length(mu_pre)); %need to be computed for each mu(i)
index_imag = find(mu_pre<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_root(mu_pre(i),c1,c2);
    T2_pre(i) = R(2);
end;
for i=1:length(mu_pre);
    index_T = min(find(T_pre>T2_pre(i)));
    
    P_asym_pre = trapz(T_pre(index_T:end),pdf_asym_pre(index_T:end,i));
    CDF_asym_pre = [CDF_asym_pre P_asym_pre];
    P_asym_num_pre = trapz(T_pre(index_T:end),pdf_num_pre(index_T:end,i));
    CDF_num_pre = [CDF_num_pre P_asym_num_pre];
end;
plot(mu_pre,CDF_asym_pre(2:end),'b*');hold on;
plot(mu_pre,CDF_num_pre(2:end),'k*');hold off;
%}