%check the asymptotic pdf for symmetric case with negative T

%introduce all the parameters for the symmetric system
mu_0 = 0.1;
mu_n = 0.03;
epsilon = 0.05;
beta = 2.5;
v = epsilon^beta;
dT = 0.001;
T_b = -sqrt(mu_n)+dT;

T1 = -1:dT:0; %negative T
T2 = T_b:dT:1; %work for T near stable branch
p_neg = zeros(1,length(T1));
for i=1:length(T1);
    psi = (2*mu_n*T1(i)-2*T1(i)^3/3+4*mu_n^(3/2)/3)/epsilon^2+log(mu_n)/4+epsilon^(beta-2)*2*log(abs(T1(i)-sqrt(mu_n)));
    p_neg(i) = exp(psi);
end;
[p_sym p_LO] = f_npdf_sym5(mu_n,T2,epsilon,beta);%asymptotic pdf for T near stable branch
int_p = trapz(T2,p_sym);

figure;
plot(T1,p_neg/int_p,'b-');
hold on;plot(T2,p_sym/int_p,'r-.');

%check the numerical results for initial T is near the unstable branch
%generate the PDF numerically stop at mu_n
loop = 10000;
dC = 1/200; %length of bin for histgram;
dt = 0.1;
time = 10;
mu_1 = ones(1,);
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,-sqrt(mu_n),T_min,0.001,0);
%NUM counts how many simulations have the endpoint smaller than the
%unstable boundary
C = T_min-dC:dC:2;
T_hist = hist(h,C)./loop./dC;