%compute partial pdf at mu=mu2 based on numerical simulation and initial
%asymptotic approximation by using one-stage conditional pdf approximation
%given pdf at mu=mu1

function p_one = f_npdf_sym_local_sim(mu1,mu2,T,T_s,T_min,p,epsilon,beta,dt,loop);
%mu1, initial condition where asymptotic approximation alone provides good
%approximation
%mu2, where the system stops
%dt, length of small time step
%T, series of the whole temperature range, which should be separated by
%T_s
%T_min, value that defines early escape in simulation
%p, pdf at mu=mu1 in the range T
%loop, rough number of numerical simulation

p_one = zeros(1,length(T));
%separate T by -sqrt(mu2);
%dT = T(2)-T(1);
v = epsilon^beta;
mu = mu1:(-v*dt):mu2;
fprintf('mu2= %d \n',mu2);
fprintf('T_s= %d \n',T_s);
%pause;
m=1;
n = 1;
while m<length(T) && T(m)<=T_s;%+0.0001;
    m = m+1;
    if T(m)>-sqrt(mu1)
        n = n+1;
    end;
end;
%probability ratio for T<-sqrt(mu2)
%R = trapz(T(1:m),p(1:m))/trapz(T,p)
%pause;
%m
%n
%pause;
%test how many small fragments between -sqrt(mu1) and -sqrt(mu2) where the
%conditional probability actually work
%frag = (sqrt(mu1)-sqrt(mu2))/dT
%pause;

T1 = zeros(1,m);
T2 = zeros(1,length(T)-m+1);
p_11 = zeros(m,1); %pdf at mu1
for i=1:m; %assign T1 completely first then compute p_tran
    T1(i) = T(i);
    p_11(i) = p(i);
end;
%T1(end)
%p_11(end)
%generate series of T0 for numerical simulation
%dT1 = zeros(1,m+1); %generate the difference between each T1(i)
%for i=1:m;
    %dT1(i) = T1(i+1)-T1(i);
%end;
%dT1(m+1) = dT1(m);
num_T0 = zeros(1,m);%generate number of T0 the same as value T1(i)
for i=1:m;
    %N = dT1(i)*p_11(i)*loop;
    N = p_11(i)/p_11(end)*loop; %make loop to be the number of the simulation given T0 = T1(end)
    num_T0(i) = round(N);
end;
SUM = sum(num_T0);
%fprintf('total number of realizations for the left tail = %d \n',SUM);
%[C I] = max(num_T0);
%fprintf('the largest number of realizations is %d, starting at T = %d  \n',C,T(I));
%figure(4);
%plot(T1,num_T0,'b-');hold on;plot(-sqrt(mu2),0,'ro');
%axis([-sqrt(mu1) T1(end) 0 max(num_T0)]);
%pause;
%generate series of T0 following the pdf p_11
T0 = ones(1,num_T0(1)).*T1(1);
for i=2:m;
    S = ones(1,num_T0(i)).*T1(i);
    T0 = [T0 S];
end;
%generate each T_n(i) based on T0(i), epsilon, beta, mu1 and mu2 by the full system;
T_n = zeros(1,length(T0));
NUM_min = 0;
for i=1:length(T0);
    TE = f_ntemperature(mu,T0(i),dt,epsilon,T_min,0.001,0);
    T_n(i) = TE(end);
    if T_n(i)==T_min;
        NUM_min = NUM_min+1;
    end;
end;
%fprintf('number of realizations that reaches early tipping = %d \n',NUM_min);
figure(3);plot(T,hist(T_n,T),'k-');hold on;plot(-sqrt(mu2),0,'ro');
pause;
%p_1 = hist(T_n,T)./(length(T0)-NUM_min)./(T(2)-T(1));
p_1 = hist(T_n,T)./length(T0)./(T(2)-T(1));
p_1(1)
if p_1(m)==0
    error('not enough loop # to keep the histogram positive');
end;
trapz(T,p_1)
%pause;
%axis([-sqrt(mu1)-0.05 0 0 max(p_1)]);
%pause;
for i=1:length(T)-m+1;
    T2(i) = T(m-1+i);
end;
p_2 = f_npdf_sym6(mu2,T2,epsilon,beta); %quasi-stationary only, p_2(1) is used to connect the two pieces
%T1(end)
fprintf('matching at %d where quasi-stationary pdf is %d \n',T2(1),p_2(1));
%p_2(1)
%pause;
%compare p_1 and p_2 to be one series under T
for i=1:length(T);
    if i<=m;
        p_one(i) = p_1(i)/p_1(m)*p_2(1);
    else
        p_one(i) = p_2(i-m+1);
    end;
end;

%compare the partial conditional with the full conditional
%loop1 = 1000;
%p_full = f_npdf_sym_local_sim2(mu1,mu2,T,T_min,p,epsilon,beta,dt,loop1);

%see the behaviour of the whole p_1 and compare with the matching one with
%quasi-stationary approximation
int_p_one = trapz(T,p_one);
%figure(1);
%plot(T,p_1,'r-.');hold on;
%plot(T,p_one./int_p_one,'b-');hold on;
%plot(T,p_full,'k:');hold on;
%plot(-sqrt(mu2),0,'ro');hold on;
%plot(T_s,0,'k*');
%pause;

p_one = p_one./int_p_one; %including the normalization part