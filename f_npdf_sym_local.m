%compute partial pdf at mu=mu_n based on brownian motion and initial
%asymptotic approximation
function p_local = f_npdf_sym_local(mu1,mu2,T1,T2,epsilon,beta);
%mu1, initial mu for brownian motion where asymptotic pdf still works
%mu2, where the system stops
%T1, temperature series near -sqrt(mu1)
%T2, all possible temperature values for the initial pdf, which is the end
%of the first stage

v = epsilon^beta;
time = (mu1-mu2)/v; %time length for the brownian motion
%p_local = zeros(1,length(T)); %final pdf
p_tran = zeros(length(T1),length(T2)); %transient pdf based on brownian motion
p_0 = zeros(length(T2),1);

%amplitude coefficient for p_0
f1_sym = log(mu1)/4;
f2_sym = epsilon^2*(-5*mu1^(-3/2)/96);
f3_sym = epsilon^beta*(3/16*mu1^(-3/2));
f4_sym = epsilon^(2*beta-2)*(-mu1^(-3/2)/8);

%compute p_0 based on asymptotic approximation, separate the values besides
%-sqrt(mu1);
for j=1:length(T2);
    if T2(j)<=-sqrt(mu1);
        p_0(j) = 0;
    else
        psi0_sym = 2*mu1*T2(j)-2*T2(j)^3/3-4*mu1^(3/2)/3; %check consistence with symmetric case
        psi1_sym = epsilon^(beta-2)*(2*log(sqrt(mu1)+T2(j))-log(4*mu1));
        psi2_sym = epsilon^beta*(-1/(T2(j)+sqrt(mu1))-sqrt(mu1)/(T2(j)+sqrt(mu1))^2)/4/mu1+epsilon^beta*3*mu1^(-3/2)/16;
        psi3_sym = epsilon^(2*beta-2)*(1/mu1/(T2(j)+sqrt(mu1))+sqrt(mu1)/mu1/2/(T2(j)+sqrt(mu1))^2-5*mu1^(-3/2)/8);
        p_0(j) = exp(psi0_sym/epsilon^2+psi1_sym+psi2_sym+psi3_sym+f1_sym+f2_sym+f3_sym+f4_sym);
    end;
    for i=1:length(T1);
        p_tran(i,j) = exp(-(T1(i)-T2(j))^2/2/time/epsilon^2)/sqrt(2*time*pi)/epsilon*(T2(2)-T2(1));
    end;
end;

%figure(3);
%plot(T,p_0,'b-');
%hold on;plot(T,p_tran(:,length(T)),'r-.');
%pause;

p_local = p_tran*p_0;

%figure(4);
%plot(T,p_local,'k--');
%pause;