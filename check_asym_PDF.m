%compare the quasi-stationary pdf with different asymmetry bifurcation
%structure at the same mu
%and also compare the potential well

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = 0.07;%SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c0 = [0 -0.4731 -A2/(6*A)^3];
c1 = [-0.001 -1.1233 -4*A2/(6*A)^2]; %needs to be negative to fits the log term
c2 = [0 6.7396 2*B2/(3*A*B1)];

%constants for the full system
mu_0 = 0.2;
epsilon = 0.05;
%beta = 2.5;
%v = epsilon^beta;
v = 0.0001;
beta = log(v)/log(epsilon)
mu_n = [0.2 0.1 0.05];
dT = 0.01;
T = -3:dT:2; %range for temperature distribution

%generate bifurcation diagram
mu_bif = zeros(length(c0),length(T));
for i=1:length(c0);
    mu_bif(i,:) = (T.^2-c1(i).*T.^3-c0(i).*T.^4)./(c2(i).*T+1);
end;
figure;
plot(mu_bif(1,:),T,'b-','linewidth',2);hold on;plot(mu_bif(2,:),T,'r--','linewidth',2);hold on;plot(mu_bif(3,:),T,'k:','linewidth',2);
axis([0 0.2 -0.5 1]);

%{
%generate quasi-stationary PDF for different c0,c1,c2 at different mu_n
for i=1:length(mu_n);
    PDF = zeros(length(c0),length(T));
    U = zeros(length(c0),length(T));
    T1_s = zeros(size(c0)); %local max
    T1_uns = zeros(size(c0)); %local min
    for j=1:length(c0);
        f_total = f_a_npdf4(mu_n(i),T,c0(j),c1(j),c2(j),epsilon,beta); %including c0*T^4
        int_0 = trapz(T,f_total);
        PDF(j,:) = f_total./int_0;
        
        U(j,:) = -(c0(j).*T.^5./5+c1(j).*T.^4./4-T.^3./3+c2(j).*mu_n(i).*T.^2./2+mu_n(i).*T);
        
        R = f_n_fullroot(mu_n(i),c0(j),c1(j),c2(j));
        T1_s(j) = R(1);
        T1_uns(j) = R(2);
    end;
    U1 = -(c0.*T1_s.^5./5+c1.*T1_s.^4./4-T1_s.^3./3+c2.*mu_n(i).*T1_s.^2./2+mu_n(i).*T1_s);
    U2 = -(c0.*T1_uns.^5./5+c1.*T1_uns.^4./4-T1_uns.^3./3+c2.*mu_n(i).*T1_uns.^2./2+mu_n(i).*T1_uns);
    
    figure(1);
    plot(T,PDF(1,:),'b-','linewidth',2);hold on;plot(T,PDF(2,:),'r--','linewidth',2);hold on;plot(T,PDF(3,:),'k:','linewidth',2);
    hold off;
    title('PDF Comparison');
    axis([-0.5 1 0 10])
    
    figure(2);
    plot(T,U(1,:),'b-','linewidth',2);hold on;plot(T1_s(1),U1(1),'b*','markersize',6);hold on;plot(T1_uns(1),U2(1),'bo','markersize',6);hold on;
    plot(T,U(2,:),'r--','linewidth',2);hold on;plot(T1_s(2),U1(2),'r*','markersize',6);hold on;plot(T1_uns(2),U2(2),'ro','markersize',6);hold on;
    plot(T,U(3,:),'k:','linewidth',2);hold on;plot(T1_s(3),U1(3),'k*','markersize',6);hold on;plot(T1_uns(3),U2(3),'ko','markersize',6);
    hold off;
    title('Potential Well Comparison');
    
    pause;
end;
%}