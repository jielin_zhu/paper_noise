function [m,var,gamma] = f_skewness(x,pdf)

%return to the mean, standard deviation, and skewness of the pdf, which is a non-dimension measure
%x is the domain of pdf

F = x .* pdf;
mean = trapz(x,F);
m = mean;
F2 = (x-m).*(x-m).*pdf;
%trapz(x,F2)
%pause;
var = trapz(x,F2);%trapz(x,F2)-mean^2;
sigma = sqrt(var);
pre = (x-mean)./sigma.*(x-mean)./sigma.*(x-mean)./sigma.*pdf;
gamma = trapz(x,pre);

%figure(1);plot(x,var);pause;


%Use a step-by-step way to calculate mean, var, and skewness
%n = length(x);
%F1 = zeros(1,n);
%F2 = zeros(1,n);
%F3 = zeros(1,n);

%for i=1:n
    %F1(i) = x(i)*pdf(i);
    %F2(i) = x(i)^2*pdf(i);
%end;

%mean = trapz(x,F1);
%sigma2 = trapz(x,F2)-mean^2;

%for i=1:n;
    %F3(i) = ((x(i)-mean)/sqrt(sigma2))^3*pdf(i);
%end;

%gamma = trapz(x,F3);