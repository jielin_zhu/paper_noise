%leading order approximation of the PDF for the symmetric case
%redefine the pdf for T<-sqrt(mu) to make it exponentially
%decay(optional)

function p_LO = f_npdf_sym_LO(mu,T,epsilon,beta);

n1 = length(T);
psi0_sym = zeros(1,n1);
p_LO = zeros(1,n1);

psi0_min = 2*mu*(-sqrt(mu))-2*(-sqrt(mu))^3/3-4*mu^(3/2)/3;

for i=1:n1;
    psi0_sym(i) = 2*mu*T(i)-2*T(i)^3/3-4*mu^(3/2)/3; %check consistence with symmetric case
    if T(i)>=-sqrt(mu);
        p_LO(i) = exp(psi0_sym(i)/epsilon^2);
    else
        p_LO(i) = exp((2*psi0_min-psi0_sym(i))/epsilon^2); %not working for the value below unstable equilibrium
    end;
end;

%figure(3);plot(T,psi0_sym/epsilon^2);
%figure(4);plot(T,p_LO);
%pause;