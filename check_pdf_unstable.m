%check the pdf for x below unstable branch for a smaller

epsilon = 0.05;
beta = 2.5;
v = epsilon^beta;
a0 = 0.1;
an = 0;
dt = 0.01;
a_t = a0:(-0.01):an;%a0:(-v*dt):an;
x = -0.5:0.01:1;

for i=1:length(a_t);
    p_t = (sqrt(a_t(i))+x).^2.*exp(1./epsilon.^2.*(2.*a_t(i).*x-2./3.*x.^3-4./3.*a_t(i).^(3/2))+1./4.*log(a_t(i))-epsilon.^(beta-2).*log(a_t(i)));
    %Int_p = trapz(x,p_t);
    C = 2/sqrt(2*pi*epsilon^2)*4^(-epsilon^(beta-2));
    p_t = p_t.*C;
    %figure(1);
    %plot(x,p_t);
    %title(['a=',num2str(a_t(i))]);hold off;
    %pause;
end;

%consider the time step of each iteration based on the leading order
%approximation of the convection-diffusion process
a_mid = epsilon;
t=8;
a_m = a_mid:(-v*t):an;
P = exp(-a_m.^2.*t./2)