%only compare the pdf based on simulation (original) and the simulated pdf starting
%from quasi-stationary pdf
%set the loop number to be the same

%introduce parameter values
mu_0 = 0.05;
%mu_n = 0.025;
epsilon = 0.05;
beta = 2.5;
v = epsilon^beta;
mu_n = 0.02;%0.04-v*5
mu_middle = 0.04;%mu_n+v*5%0.04; %0.03;%epsilon^(4/3);
T_min = -5;
dC = 1/200; %length of bin for histgram;
%T = T_min-dC:dC:2; %used for both quasi-stationary pdf and simulated one
%T = T_min-dC:0.001:1;
T = T_min-dC:0.001:1;
T1 = T;%-4:0.001:1; %to get rid of the early tipping in partial conditional case
loop = 50000;
loop1 = 1000;
loop2 = 5000;
dt = 0.1;

%check the conditional pdf for part of the series of T
%mu1 = 0.04;
%mu2 = 0.035;
%T2 = T_min-dC:0.001:-sqrt(mu2)+0.04;
%p_sym_previous2 = f_npdf_sym6(mu1,T2,epsilon,beta);
%p_sym_partial2 = f_npdf_sym_local_sim3(mu1,mu2,T2,T_min,T,p_sym_previous2,epsilon,beta,dt,loop2);
%T3 = -sqrt(mu2):0.001:-sqrt(mu2)+0.02;
%p_sym_previous3 = f_npdf_sym6(mu1,T3,epsilon,beta);
%p_sym_partial3 = f_npdf_sym_local_sim3(mu1,mu2,T3,T_min,T,p_sym_previous3,epsilon,beta,dt,loop2);
%figure(5);
%plot(T,p_sym_partial2,'b-');hold on;
%plot(T,p_sym_partial3,'r-.');hold on;
%plot(-sqrt(mu_n),0,'ro');
%N1 = 1;
%while N1 <length(T) && T(N1)<-sqrt(mu2);
    %N1 = N1+1;
%end;
%prob_tip_partial2 = trapz(T(1:N1),p_sym_partial2(1:N1))
%prob_tip_partial3 = trapz(T(1:N1),p_sym_partial3(1:N1))
%break;

%under the same range series T, find the index that is close to unstable
%branch
N = 1;
while N <length(T) && T(N)<-sqrt(mu_n);
    N = N+1;
end;
N1 = 1;
while N1 <length(T1) && T1(N1)<-sqrt(mu_n);
    N1 = N1+1;
end;

%simulation based on quasi-stationary pdf at mu_middle over the whole rage
%of T and only the left tail
p_previous_partial = f_npdf_sym6(mu_middle,T1,epsilon,beta);
p_previous_full = f_npdf_sym6(mu_middle,T,epsilon,beta);
p_previous_past = f_npdf_sym6(mu_middle,T1,epsilon,beta);
%p_previous_brownian = f_npdf_sym6(mu_middle,T,epsilon,beta);
%use multiple stages to check the consistency
mu_1 = mu_middle:-0.005:mu_n;%[mu_middle mu_n];%mu_middle:-0.001:mu_n;
T_s = [0 -sqrt(mu_1(2))+0.012 -sqrt(mu_1(3))+0.015 -sqrt(mu_1(4))+0.022 -sqrt(mu_1(5))+0.032]%-sqrt(mu_1)+0.02;%[0 0.2];%-sqrt(mu_1)+0.02;
T_l = [0 -sqrt(mu_1(2))-0.2 -sqrt(mu_1(3))-0.02 -sqrt(mu_1(4))-0.02 -sqrt(mu_1(5))-0.01];
%tic
for i=2:length(mu_1);
    p_sym_full = f_npdf_sym_local_sim2(mu_1(i-1),mu_1(i),T,T_min,p_previous_full,epsilon,beta,dt,loop1); 
    p_previous_full = p_sym_full;
    
    %p_sym_partial = f_npdf_sym_local_sim(mu_1(i-1),mu_1(i),T1,T_s(i),T_min,p_previous_partial,epsilon,beta,dt,loop2);
    p_sym_partial = f_npdf_sym_local_sim_cor(mu_1(i-1),mu_1(i),T1,T_l(i),T_s(i),T_min,p_previous_partial,epsilon,beta,dt,loop2);
    p_previous_partial = p_sym_partial;
    %int_p_partial = trapz(T,p_sym_partial);
    
    %original one keeping the T_min prob.
    p_sym_past = f_npdf_sym_local_sim(mu_1(i-1),mu_1(i),T1,T_s(i),T_min,p_previous_past,epsilon,beta,dt,loop2);
    p_previous_past = p_sym_past;
    
    %p_brownian = f_npdf_sym_local_brownian(mu_1(i-1),mu_1(i),T,T_s(i),p_previous_brownian,epsilon,beta,dt,loop2);
    %p_previous_brownian = p_brownian;
    %int_p_brownian = trapz(T,p_brownian);
    
    figure;%compare the full and partial conditional pdf at each mu(i)
    plot(T,p_sym_full,'k:');hold on;
    plot(T1,p_sym_partial,'b-');hold on;
    plot(T_s(i),0,'k*');hold on;
    plot(-sqrt(mu_1(i)),0,'ro');hold on;
    plot(T_l(i),0,'b*');
    
    prob_tip_full = trapz(T(1:N),p_sym_full(1:N))%/int_p_full
    prob_tip_partial = trapz(T(1:N1),p_sym_partial(1:N1))%./int_p_partial
    prob_tip_past = trapz(T(1:N1),p_sym_past(1:N1))
    prob_early_full = trapz(T(1:10),p_sym_full(1:10))
    prob_early_partial = trapz(T(1:10),p_sym_partial(1:10))
    prob_early_past = trapz(T(1:10),p_sym_past(1:10))
    pause;
end;
%prob_tip_brownian = trapz(T(1:N),p_brownian(1:N))%/int_p_brownian
%toc

%simulation based on the same T0 start from mu_0
%tic
mu_3 = mu_0:(-v*dt):mu_n;
[h_n,NUM,NUM_tipping] = f_ntem_histgram_count3(loop,mu_3,dt,epsilon,sqrt(mu_0),-sqrt(mu_n),T_min,0.001,0);
H_n = hist(h_n,T)./loop./(T(2)-T(1));
trapz(T,H_n)
prob_sim = trapz(T(1:N),H_n(1:N))
%toc

%asymptotic approximation at mu_n
p_sym = f_npdf_sym6(mu_n,T,epsilon,beta);
int_p = trapz(T,p_sym);

%compare the final pdf in figure
figure;
%plot(T,p_sym_full,'b-');hold on;
plot(T,p_sym_partial,'k-.');hold on;
%plot(T,H_n,'k:');hold on;
%plot(T,p_brownian,'r:');hold on;
plot(-sqrt(mu_n),0,'ro');
title(['$$\mu_{mid}= $$',num2str(mu_middle),'; $$ \mu_n=$$',num2str(mu_n),'; $$loop = $$',num2str(loop)],'fontsize',15,'interpreter','latex');