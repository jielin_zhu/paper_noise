%Generate PDF curve for the fully-normalized model for symmetric &
%asymmetric case based on asymptotic approximation, compared with leading
%order approximation

%Call the constant(may also be changed) for original temperature model
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);

%calculate the normalized coefficient and the bifurcation point
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;

%new coefficients for the fully normalized model, related to A,B1,A2,B2
c1 = -4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);

%Call the input parameter for fully normalized temperature model
mu_0 = 0.1;%1, normalized mu at the beginning;
mu_n = 0.005;%2, normalized mu at the end of the simulation(no need to be smaller than mu_e);
epsilon = 0.05;%3, size of the additional noise;
beta = 2.5;%relationship of drift rate and noise;
v = epsilon^beta;%10^(-4);% %4, rate of drift term;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.01;%5, length of time step
%T_min = -0.5;%f_T_neg(mu_n,A,B1);%-0.5; %lower boundary for numerical simulation of temperature, can choose f_T_neg
loop = 10000;

mu = mu_0:-0.001:mu_n;
%mu = 0.3;
n = length(mu);

%calculate pdf for mu_n for symetric and asymmetric case
root = f_n_root(mu_n,c1,c2);
T_r = root(2)+0.001:0.001:2; %define the domain of PDF for every different mu(i)
%[p_a p_sym] = f_a_npdf_compare(mu_n,T_r,c1,c2,epsilon,beta); 
%int_p = trapz(T_r,p_a);

%calculate leading order pdf for symmetric and asymmetric case
[p_a_LO p_sym_LO] = f_a_npdf_LO(mu_n,T_r,c1,c2,epsilon,beta); 
%int_p_LO = trapz(T_r,p_a_LO);
%int_psym_LO = trapz(T_r,p_sym_LO);
Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2);%normalization coefficient for symmetric case

%Find the proper normalization constant for asymmetric case, which is the
%integral of p_sym for mu just before early tipping happens
N=1;
P_0=0; %p_asym at T_r(1) for each mu(i);
while (N<n) && (P_0<10^(-5))
    N=N+1;
    root = f_n_root(mu(N),c1,c2);
    T_r1 = root(2)+0.001:0.001:2;
    [P_LO P_sym_LO] = f_a_npdf_LO(mu_n,T_r1,c1,c2,epsilon,beta); 
    P_0=P_LO(1);
end;
%N
%pause;
int_p_LO = trapz(T_r1,P_LO);

%compare the CDF curve with histogram from numerical simulations
mu_1 = 0.1:(-v*dt):mu_n;
m = length(mu_1);
T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
[h,NUM] = f_ntem_histgram_count(loop,mu_1,dt,epsilon,root(2),T_min,c1,c2);
dC = 1/200;
C = T_min:dC:2;
T_hist = hist(h,C)./loop./dC;

Fig = figure;
plot(C,T_hist,'Marker','o','Color','k');
hold on;plot(T_r,p_a_LO/int_p_LO,'r-','Linewidth',2);
hold on;plot(T_r,p_sym_LO*Con,'b-','Linewidth',2);
axis([-0.1 0.2 0 10]);
title(['(a): $$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$v$$= ',num2str(V),'; $$\mu$$= ',num2str(mu_n)],'fontsize',15,'interpreter','latex')
%print(Fig,'-depsc','figure_pdf_compare6.eps')