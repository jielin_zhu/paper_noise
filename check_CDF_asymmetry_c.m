%check the CDF of numerical simulations with different asymmetric
%bifurcation structure corresponding to the change of A

%numerical simulations for the opposite asymmetry system
load file_PDF_full_num_c0.dat
[m1,n1] = size(file_PDF_full_num_c0);
T_asym = file_PDF_full_num_c0(1:m1-1,1);
mu_asym = file_PDF_full_num_c0(end,2:end);
pdf_asym_num = file_PDF_full_num_c0(1:m1-1,2:end);

%numerical simulations with smaller c1 c1 = 0.001
load file_PDF_full_num_c1.dat
[m2,n2] = size(file_PDF_full_num_c1);
T_asym2 = file_PDF_full_num_c1(1:m2-1,1);
mu_asym2 = file_PDF_full_num_c1(end,2:end);
pdf_asym_num2 = file_PDF_full_num_c1(1:m2-1,2:end);

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);%[SP(3) 0.2 0.07]; %different values of A
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = [0 0];%-A2./(6.*A).^3;
c1 = [1.1233 0.001];%-4.*A2./(6.*A).^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = [-6.7396 -6.7396];%2.*B2./(3.*A.*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
%beta = 2.5;
v=0.0005;

%compute the CDF for the original case
CDF_asym_num = 1;
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0(1),c1(1),c2(1));
    T2(i) = R(3);
end;
for i=1:length(mu_asym);
    
    index_T = min(find(T_asym>T2(i)));
    
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
end;

%compute the CDF for the case with larger A
CDF_asym_num2 = 1;
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym2(i),c0(2),c1(2),c2(2));
    T2(i) = R(3);
end;
for i=1:length(mu_asym2);
    
    index_T = min(find(T_asym2>T2(i)));
    
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,i));
    CDF_asym_num2 = [CDF_asym_num2 P_asym_num];
end;

Fig = figure;
plot(mu_asym,CDF_asym_num(2:end),'b*--','linewidth',1.5);hold on;
plot(mu_asym2,CDF_asym_num2(2:end),'ro--','linewidth',1.5);hold on;
