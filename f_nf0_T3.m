function F = f_nf0_T3(mu,c1,c2)
% mu is the ending point for the process

n = 5000;
T_tao = zeros(1,n);
dmu = mu/n;
muv = 0:dmu:mu;
m = length(muv);

dF = zeros(size(muv));
for i=1:m;
    T_root = f_n_fullroot(muv(i),0,c1,c2);%f_n_root(muv(i),c1,c2); % root for the truncated function
    T_tao(i) = T_root(1);
    %[muv(i) T_tao(i)];
    dF1 = -2*T_tao(i);
    dF2 = -c2*T_tao(i)^2;
    dF(i) = dF1 + dF2;
end;

F = trapz(muv,dF); %Numerically estimate f_0 by Trapezoidal rule
