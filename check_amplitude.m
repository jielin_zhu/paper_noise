%check f11 for different values of c1 and c2

%define initial coefficients
%c1 = 1;
%c2 = -1;
mu_0 = 0.02;
mu_n = 0.005;
epsilon = 0.02;
beta = 2.2;
v = epsilon^beta;
V = double(vpa(v,2)); %approximation of 5 decimal place
dt = 0.01;

%define the require series of mu
mu = mu_0:(-0.005):mu_n;
n = length(mu);

%F1 = zeros(1,n); %check if it's 0 for the symmetric case
%F1_sym = zeros(1,n);
%F11 = zeros(1,n);
%F12 = zeros(1,n);
%F13 = zeros(1,n);
%F0 = zeros(1,n);
%F0_sym = zeros(1,n);
int_a = zeros(1,n);
int_opp = zeros(1,n); %CDF for c1=-1&c2=1;
int_sym = zeros(1,n); %CDF for symmetric case;
Con = 2*4^(-epsilon^(beta-2))/sqrt(2*pi*epsilon^2); %amplitude coefficient for the symmetric case
for i=1:n;
    %[f1 f2 f3] = f_nf1(mu(i),c1,c2,epsilon,beta);
    %F1(i) = exp(f1+f2+f3);
    %F11(i) = f1;%exp(f1);
    %F12(i) = f2;%exp(f2);
    %F13(i) = f3;%exp(f3);
    %F1_sym(i) = exp(log(mu(i))/4);
    %F0(i) = f_nf0(mu(i),c1,c2);
    %F0_sym(i) = -4*mu(i)^(3/2)/3;
    
    %check f1 for each mu(i) before the integration
    %dmu = 0.001;
    %mu_s = mu(i):dmu:0.16;
    %m = length(mu_s);
    %psi11_TT = zeros(1,m);
    %for j=1:m;
        %root = f_n_root(mu_s(j),c1,c2);
        %T = root(1);
        %psi11_TT(j) = f_npsi1_tt(T,mu_s(j),c1,c2);
    %end;
    %figure(1);hold on;plot(mu_s,psi11_TT,'r-','Linewidth',2);pause;
    
    %compute p_a and related CDF
    root = f_n_root(mu(i),1,-1);
    T_r = root(2)+0.001:0.0005:root(3)-0.001; %specificly for c1>0 and c2<0
    %T_r = root(2)+0.001:0.0005:0.8;
    p_a = f_a_npdf(mu(i),T_r,1,-1,epsilon,beta); %for the case c1=1&c2=-1
    int_a(i) = trapz(T_r,p_a);
    
    %compute PDF and CDF for c1=-1&c2=1;
    root = f_n_root(mu(i),-1,1);
    T_r_opp = root(2)+0.001:0.0005:2; %define the domain of PDF for every different mu(i)
    p_opp = f_a_npdf(mu(i),T_r_opp,-1,1,epsilon,beta);
    int_opp(i) = trapz(T_r_opp,p_opp);
    
    %compute PDF and CDF for the symmetric case;
    T_r_sym = -sqrt(mu(i))+0.001:0.001:2; %the domain of the PDF for the symmetric case
    p_sym = f_npdf_sym(mu(i),T_r_sym,epsilon,beta);
    int_sym(i) = trapz(T_r_sym,p_sym);
    
    %compute PDF for the gaussian approximation
    mu_1 = 0.5:(-v*dt):mu_n;
    p_gau = f_gau_pdf(mu_1,v,dt,T_r_sym,epsilon);
    
    %only compute the PDF based on psi0 and f0 for c1=1&c2=-1
    %m = length(T_r);
    %psi = zeros(1,m);
    %c1=1;c2=-1;
    %F0 = f_nf0(mu(i),c1,c2);
    %[f11 f12 f13] = f_nf1(mu(i),c1,c2,epsilon,beta);
    %F1 = f11+f12+f13;
    %for j=1:m;
        %psi(j) = (-2*T_r(j)^3/3+2*mu(i)*T_r(j)+c1*T_r(j)^4/2+c2*mu(i)*T_r(j)^2+F0)/epsilon^2; %psi0
        %psi(j) = f_npsi11(T_r(j),mu(i),c1,c2);
    %end;
    %LO = exp((psi));
    %SO = exp(psi*epsilon^(beta-2)+F1);
    %SO = exp(psi*epsilon^(beta-2));
    
    %only compute the PDF based on psi0 and f0 for c1=-1&c2=1
    %m = length(T_r_opp);
    %psi_opp = zeros(1,m);
    %c1=-1;c2=1;
    %F0 = f_nf0(mu(i),c1,c2);
    %[f11 f12 f13] = f_nf1(mu(i),c1,c2,epsilon,beta);
    %F1_opp = f11+f12+f13;
    %for j=1:m;
        %psi_opp(j) = (-2*T_r_opp(j)^3/3+2*mu(i)*T_r_opp(j)+c1*T_r_opp(j)^4/2+c2*mu(i)*T_r_opp(j)^2+F0)/epsilon^2; %psi0
        %psi_opp(j) = f_npsi11(T_r_opp(j),mu(i),c1,c2);
    %end;
    %LO_opp = exp((psi_opp));
    %SO_opp = exp(psi_opp*epsilon^(beta-2)+F1_opp);
    %SO_opp = exp(psi_opp*epsilon^(beta-2));
    
    %only compute the PDF based on psi0 and f0 for c1=c2=0
    %m = length(T_r_sym);
    %psi_sym = zeros(1,m);
    %F0 = f_nf0(mu(i),c1,c2);
    %F1_sym = log(mu(i))/4;
    %for j=1:m;
        %psi_sym(j) = (2*mu(i)*T_r_sym(j)-2*T_r_sym(j)^3/3-4*mu(i)^(3/2)/3)/epsilon^2; %psi0
        %psi_sym(j) = epsilon^(beta-2)*(2*log((sqrt(mu(i))+T_r_sym(j)))-log(mu(i)));
    %end;
    %LO_sym = exp(psi_sym);
    %SO_sym = exp(psi_sym+F1_sym);
    %SO_sym = exp(psi_sym);
    
    %check the difference on the original PDF first
    %figure(1);
    %plot(T_r,p_a,'b-',T_r_opp,p_opp,'k-.',T_r_sym,p_sym,'r:','Linewidth',2);hold on;
    %title(['\mu=',num2str(mu(i))]);
    %pause;
    %figure(2)
    %plot(T_r,LO,'b-',T_r_opp,LO_opp,'k-.',T_r_sym,LO_sym,'r:','Linewidth',2);hold on;
    %plot(T_r,SO,'b-',T_r_opp,SO_opp,'k-.',T_r_sym,SO_sym,'r:','Linewidth',2);hold on;
    %title(['\mu=',num2str(mu(i))]);
    %figure(3)
    %plot(T_r,psi,'b-',T_r_opp,psi_opp,'k-.',T_r_sym,psi_sym,'r:','Linewidth',2);hold on;
    %title(['\mu=',num2str(mu(i))]);
    %figure(5);
    %plot(T_r,p_a/int_a(1),'b-',T_r_opp,p_opp/int_opp(1),'k-.',T_r_sym,p_sym*Con,'r:','Linewidth',2);hold on;
    %title(['\mu=',num2str(mu(i))]);
    %pause;
end;

%also sketch the unstable boundary for our approximation
Y = 0:0.1:15;
l = length(Y);
L = ones(1,l).*root(2);

%figure(1);
%plot(mu,F11,'k-.','Linewidth',2);grid on; hold on;
%figure(2);
%plot(mu,exp(F11),'b-.','Linewidth',2);grid on; hold on;
%figure(2);
%plot(mu,F1/F1(1));grid on;
%plot(mu,F1,'b-',mu,F1_sym,'r-.','Linewidth',2);hold on;
%plot(mu,F11,'k-',mu,F12,'k-.',mu,F13,'k:','Linewidth',2);
%figure(4);
%plot(mu,int_a/int_a(1),'b-','Linewidth',2);hold on;grid on;
%plot(mu,int_opp/int_opp(1),'k-.',mu,int_sym*Con,'r:','Linewidth',2);
%axis([0 0.1 0.9 1.05]);
figure(5);
plot(T_r,p_a/int_a(1),'b-.','Linewidth',2);hold on;
plot(T_r_opp,p_opp/int_opp(1),'k-.','Linewidth',2);hold on;
plot(T_r_sym,p_sym*Con,'r-','Linewidth',2);hold on;
plot(T_r_sym,p_gau,'g:','Linewidth',2);hold on;
plot(L,Y,'r:','Linewidth',2);
axis([-0.1 0.2 0 12])
title(['$$\epsilon$$= ',num2str(epsilon),'; $$\beta$$= ',num2str(beta),'; $$v$$= ',num2str(V),'; $$\mu$$= ',num2str(mu_n)],'fontsize',15,'interpreter','latex');

%generate related bifurcation diagram for three 'symmetric cases'
mu_1 = 1:-0.01:0;
m = length(mu_1);
T = zeros(3,m); %bifurcation diagram for the case c1=1&c2=-1
T_opp = zeros(3,m); %bifurcation diagram for the case c1=-1&c2=1;
T_sym = zeros(2,m); %bifurcation diagram for the case c1=c2=0;
for i=1:m;
    R = f_n_root(mu_1(i),1,-1);
    %pause;
    T(1,i) = R(1);
    T(2,i) = R(2);
    T(3,i) = R(3);
    R_opp = f_n_root(mu_1(i),-1,1);
    T_opp(1,i) = R_opp(1);
    T_opp(2,i) = R_opp(2);
    T_opp(3,i) = R_opp(3);
    T_sym(1,i) = sqrt(mu_1(i));
    T_sym(2,i) = -sqrt(mu_1(i));
end;
figure(6);
plot(mu_1,T_sym(1,:),'Color','r','LineWidth',2,'LineStyle','-');
hold on;plot(mu_1,T_sym(2,:),'Color','r','LineWidth',2,'LineStyle',':');
%hold on;plot(mu_1,T(1,:),'b-','LineWidth',2);
%hold on;plot(mu_1,T(2,:),'b:','LineWidth',2);
hold on;plot(mu_1,T(3,:),'b-.','LineWidth',2);
%hold on;plot(mu_1,T_opp(1,:),'k-','LineWidth',2);
%hold on;plot(mu_1,T_opp(2,:),'k:','LineWidth',2);
hold on;plot(mu_1,T_opp(3,:),'k-','LineWidth',2);
%axis([0 0.1 -0.6 1.4]);
xlabel('$$ \mu$$','fontsize',20,'interpreter','latex');
ylabel('$$ T $$','fontsize',20,'interpreter','latex');