%compare the bifurcation structure for the original temperature model,
%symmetric canonical model and opposite asymmetry model by comparing the
%distance between the stable and unstable equilibrium, and the height
%between the local max and local min of the potential well

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);%[SP(3) 0.2 0.07]; %different values of A
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = [-A2./(6.*A).^3 0 0];
c1 = [-4*A2/(6*A)^2 0 4*A2/(6*A)^2]; %needs to be negative to fits the log term
c2 = [2*B2/(3*A*B1) 0 -2*B2/(3*A*B1)];%[2*B2/(3*A*B1) 2*B2/(3*A*B1)*0.5 2*B2/(3*A*B1)+2.5];%2*B2/(3*A*B1)-c1(1)*0.5];
epsilon = 0.05;
%beta = 2.5;
v=0.0005;

mu_asym = 0.05:(-0.001):0;
Tb = zeros(2,length(mu_asym)); %bifurcation diagram of the system
for i=1:length(mu_asym)-1;
    R = f_n_fullroot(mu_asym(i),c0(1),c1(1),c2(1));
    Tb(1,i) = R(1);
    Tb(2,i) = R(2);
end;
Tb2 = zeros(2,length(mu_asym)); %bifurcation diagram of the system
for i=1:length(mu_asym)-1;
    Tb2(1,i) = sqrt(mu_asym(i));
    Tb2(2,i) = -sqrt(mu_asym(i));
end;
Tb3 = zeros(2,length(mu_asym)); %bifurcation diagram of the system
for i=1:length(mu_asym)-1;
    R = f_n_fullroot(mu_asym(i),c0(3),c1(3),c2(3));
    Tb3(1,i) = R(2);
    Tb3(2,i) = R(3);
end;

%consider the difference between Tb(1,:) and Tb(2,:), and the height
%between the local max and local min of the potential
D = abs(Tb(1,:)-Tb(2,:));
D2 = abs(Tb2(1,:)-Tb2(2,:));
D3 = abs(Tb3(1,:)-Tb3(2,:));
Fig5 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,D,'b-','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,D2,'r--','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,D3,'k-.','linewidth',2);hold on;
xlabel('\mu','fontsize',40);
ylabel('Distance','fontsize',40);
%print(Fig5,'-depsc','figure_distance_neg_c2.eps')
U1s = -(c0(1).*Tb(1,:).^5./5+c1(1).*Tb(1,:).^4./4-Tb(1,:).^3./3+c2(1).*mu_asym.*Tb(1,:).^2./2+mu_asym.*Tb(1,:));
U1uns = -(c0(1).*Tb(2,:).^5./5+c1(1).*Tb(2,:).^4./4-Tb(2,:).^3./3+c2(1).*mu_asym.*Tb(2,:).^2./2+mu_asym.*Tb(2,:));
U2s = -(c0(2).*Tb2(1,:).^5./5+c1(2).*Tb2(1,:).^4./4-Tb2(1,:).^3./3+c2(2).*mu_asym.*Tb2(1,:).^2./2+mu_asym.*Tb2(1,:));
U2uns = -(c0(2).*Tb2(2,:).^5./5+c1(2).*Tb2(2,:).^4./4-Tb2(2,:).^3./3+c2(2).*mu_asym.*Tb2(2,:).^2./2+mu_asym.*Tb2(2,:));
U3s = -(c0(3).*Tb3(1,:).^5./5+c1(3).*Tb3(1,:).^4./4-Tb3(1,:).^3./3+c2(3).*mu_asym.*Tb3(1,:).^2./2+mu_asym.*Tb3(1,:));
U3uns = -(c0(3).*Tb3(2,:).^5./5+c1(3).*Tb3(2,:).^4./4-Tb3(2,:).^3./3+c2(3).*mu_asym.*Tb3(2,:).^2./2+mu_asym.*Tb3(2,:));
H = abs(U1s-U1uns);
H2 = abs(U2s-U2uns);
H3 = abs(U3s-U3uns);
Fig6 = figure;
plot(mu_asym.*8/3/A/B1+mu_e,H,'b-','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,H2,'r--','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,H3,'k-.','linewidth',2);hold on;
xlabel('\mu','fontsize',40);
ylabel('Height','fontsize',40);
%print(Fig6,'-depsc','figure_height_neg_c2.eps')