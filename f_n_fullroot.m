%find roots of the fully normalized temperature model including term T^4
function T = f_n_fullroot(mu,c0,c1,c2)

% T gives the absolute value for three root
% mu is the normalized version of one specific mu_t

% T = zeros(3,1); %sure there're three real roots
C0 = c0;
C1 = c1;
C2 = -1;
C3 = c2*mu;
C4 = mu;
C = [C0 C1 C2 C3 C4];
root = roots(C);

%T = sort(root,'descend');
R = root(imag(root)==0);
T = sort(R,'descend');