%load files for pdf of full asymmetric symmetric system with different
%bifurcation structure
%generate the figure to compare the CDF approximation above T2 in the full
%system based on matching asymptotics, numerical simulation with different
%bifurcation structure
%output the figures under the original variable \bar{T} and \bar{\mu}
%change the lower boundary of the CDF to be their own unstable equilibrium

%{
%load merged pdf based on full leading order approximation with
%epsilon=0.05 and beta=2.5
load file_PDF_asym_merge3.dat
[m1,n1] = size(file_PDF_asym_merge3);
T_asym = file_PDF_asym_merge3(1:m1-1,1);
mu_asym = file_PDF_asym_merge3(end,2:end);
pdf_asym_merge = file_PDF_asym_merge3(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num3.dat
pdf_asym_num = file_PDF_asym_num3(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.1 and
%beta=2.5
load file_PDF_asym_merge4.dat
[m2,n2] = size(file_PDF_asym_merge4);
T_asym2 = file_PDF_asym_merge4(1:m2-1,1);
mu_asym2 = file_PDF_asym_merge4(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge4(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_num4.dat
pdf_asym_num2 = file_PDF_asym_num4(1:m2-1,2:end);
%}

%load merged pdf based on full leading order approximation with
%epsilon=0.05 and v=0.0005
%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T_asym = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu_asym = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];
load file_PDF_asym_num6.dat
pdf_asym_num = file_PDF_asym_num6(1:m1-1,2:end);

%load pdf based on full leading order approximation with epsilon=0.1 and
%v=0.0005
load file_PDF_oppo_merge.dat
[m2,n2] = size(file_PDF_oppo_merge);
T_asym2 = file_PDF_oppo_merge(1:m2-1,1);
mu_asym2 = file_PDF_oppo_merge(end,2:end);
pdf_asym_merge2 = file_PDF_oppo_merge(1:m2-1,2:end); %each column is pdf at mu(i)
load file_PDF_oppo_num.dat
pdf_asym_num2 = file_PDF_oppo_num(1:m2-1,2:end);

%load oppo_merge2.dat with different Td
load file_PDF_oppo_merge8.dat
[m3,n3] = size(file_PDF_oppo_merge8);
T_asym3 = file_PDF_oppo_merge8(1:m3-1,1);
mu_asym3 = file_PDF_oppo_merge8(end,2:end);
pdf_asym_merge3 = file_PDF_oppo_merge8(1:m3-1,2:end); %each column is pdf at mu(i)

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_e = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
%beta = 2.5;
%Ts = T2+10*epsilon;
%Td = (T2-2*epsilon).*ones(1,length(mu_asym));


%compute probability above T2 for small noise
T2 = zeros(1,length(mu_asym)); %need to be computed for each mu(i)
index_imag = find(mu_asym<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym(i),c0,c1,c2);
    T2(i) = R(2);
end;
CDF_asym = 1;
CDF_asym_num = 1;
for i=1:length(mu_asym);
    index_T = min(find(T_asym>T2(i)));
    
    P_asym = trapz(T_asym(index_T:end),pdf_asym_merge(index_T:end,i));
    CDF_asym = [CDF_asym P_asym];
    P_asym_num = trapz(T_asym(index_T:end),pdf_asym_num(index_T:end,i));
    CDF_asym_num = [CDF_asym_num P_asym_num];
end;

%compute probability above T2 for opposite asymmetry
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    R = f_n_fullroot(mu_asym2(i),c0,c1,c2);
    T2(i) = R(2);
    %R = f_n_fullroot(mu_asym2(i),0,-c1,-c2); %find T2 for opposite asymmetry case
    %T2(i) = R(3);
end;
%{
Ts = T2+10*epsilon;
Tm = T2+epsilon;
%show if the PDF agrees at each mu(i)
for i=2:length(mu_asym2);
    figure(1);
    plot(T_asym2,pdf_asym_merge2(:,i),'b-');hold on;
    plot(T_asym2,pdf_asym_num2(:,i),'r-.');hold on;
    %plot(Tm(i-1),0,'ro');hold on;%plot(T1,0,'b*');hold on;
    plot(T2(i),0,'bo');hold on;
    plot(Ts(i),0,'k*');
    hold off;
    %axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu_asym2(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}
%use the unstable branch for the opposite asymmetry case
c0 = 0;
c1 = 4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = -2*B2/(3*A*B1);%2*B2/(3*A*B1);
T2 = zeros(1,length(mu_asym2)); %need to be computed for each mu(i)
index_imag = find(mu_asym2<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    %R = f_n_fullroot(mu_asym2(i),c0,c1,c2);
    %T2(i) = R(2);
    R = f_n_fullroot(mu_asym2(i),c0,c1,c2); %find T2 for opposite asymmetry case
    T2(i) = R(3);
end;
CDF_asym2 = 1;
CDF_asym_num2 = 1;
for i=1:length(mu_asym2);
    index_T = min(find(T_asym2>T2(i)));
    P_asym = trapz(T_asym2(index_T:end),pdf_asym_merge2(index_T:end,i));
    CDF_asym2 = [CDF_asym2 P_asym];
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,i));
    CDF_asym_num2 = [CDF_asym_num2 P_asym_num];
    %{
    figure(1);
    plot(mu_asym2(i),P_asym,'b*');hold on;
    plot(mu_asym2(i),P_asym_num,'ko');hold on;
    pause;
    %}
end;
%{
for i=1:length(mu_asym3);
    index_mu = min(find(mu_asym2<mu_asym3(i)))-1;
    mu_asym2(index_mu)
    figure(1);
    plot(T_asym3,pdf_asym_merge3(:,i),'m-');hold on;
    plot(T_asym2,pdf_asym_num2(:,index_mu),'r-.');hold on;
    plot(T_asym2,pdf_asym_merge2(:,index_mu),'b-');hold on;
    plot(T2(index_mu),0,'k*');hold on;
    title(['$$\mu$$= ',num2str(mu_asym3(i))],'fontsize',20,'interpreter','latex');
    hold off;
    pause;
end;
%}
CDF_asym3 = 1;
T2 = zeros(1,length(mu_asym3)); %need to be computed for each mu(i)
index_imag = find(mu_asym3<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    %R = f_n_fullroot(mu_asym3(i),c0,c1,c2);
    %T2(i) = R(2);
    R = f_n_fullroot(mu_asym3(i),c0,c1,c2); %find T2 for opposite asymmetry case
    T2(i) = R(3);
end;
for i=1:length(mu_asym3);
    mu_i = mu_asym3(i)
    index_T = min(find(T_asym3>T2(i)));
    
    P_asym = trapz(T_asym3(index_T:end),pdf_asym_merge3(index_T:end,i))
    CDF_asym3 = [CDF_asym3 P_asym];
    
    
    %compare with the original asymptotic results
    index_mu = min(find(mu_asym2<mu_asym3(i)))-1;
    %index_T2 = min(find(T_asym3>T2(index_mu)));
    P_asym_ori = trapz(T_asym2(index_T:end),pdf_asym_merge2(index_T:end,index_mu));
    P_asym_num = trapz(T_asym2(index_T:end),pdf_asym_num2(index_T:end,index_mu));
    %{
    figure(1);
    plot(T_asym2,pdf_asym_merge2(:,index_mu),'b-');hold on;
    plot(T_asym2,pdf_asym_num2(:,index_mu),'r-.');hold on;
    plot(T_asym3,pdf_asym_merge3(:,i),'m--');hold on;
    plot(T2(i),0,'k*');
    title(['$$\mu$$= ',num2str(mu_asym3(i))],'fontsize',20,'interpreter','latex');
    hold off;
    pause;
    %}
    %{
    figure(1)
    plot(mu_asym3(i),P_asym,'mo');hold on;
    plot(mu_asym3(i),P_asym_ori,'b*');hold on;
    plot(mu_asym3(i),P_asym_num,'k^');hold on;
    pause;
    %}
end;
%}

Fig = figure;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym(2:end),'b-','linewidth',2);hold on;
plot(mu_asym.*8/3/A/B1+mu_e,CDF_asym_num(2:end),'k*--','linewidth',1.5);hold on;
%plot(mu_asym2,CDF_asym2(2:end),'m-.','linewidth',2);hold on;
plot(mu_asym2.*8/3/A/B1+mu_e,CDF_asym_num2(2:end),'k--','linewidth',1.5);hold on;
plot(mu_asym3.*8/3/A/B1+mu_e,CDF_asym3(2:end),'r-.','linewidth',2);hold on;
xlabel('$$\bar{\mu}$$','FontSize',40,'interpreter','latex');
ylabel('$$P(\bar{T}>\bar{T}_{unstable})$$','FontSize',32,'interpreter','latex');
axis([0.*8/3/A/B1+mu_e 0.03.*8/3/A/B1+mu_e 0.4 1.1]);
set(gca,'fontsize',18);
print(Fig,'-depsc','figure_CDF_oppo_To2.eps')