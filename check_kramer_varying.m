%caculate the kramer's rate for the symmetric system at every different
%mu(i) and find the transition probability by trapz numerical
%approximation, compare it with numerical simulation

%introduce the parameter values for symmetric stochastic system
mu_0 = 0.01;
mu_n = 0.005;
epsilon = 0.02;
beta = 2.2;
%mu_middle = 0.025;%epsilon^(4/3);
v = epsilon^beta;
dT = 0.001;
dt = 0.1;

mu = mu_0:-v*dt:mu_n;
t = 0:dt:(mu_0-mu_n)/v;
r = zeros(1,length(mu));
p = zeros(1,length(mu));
p_approx = zeros(1,length(mu));
time = (mu(1)-mu(2))/v;
for i=1:length(mu);
    r(i) = sqrt(mu(i))/pi*exp(-8*mu(i)^(3/2)/3/epsilon^2);
    p(i) = trapz(t,r);
    p_approx(i) = sum(r)*dt; %checked to be the same as p;
end;
%P = zeros(1,length(mu));
%P(1) = p(1);
%for i=2:length(mu);
    %P(i) = P(i-1)+p(i);
%end;

figure;
plot(mu,r,'r-');hold on;
%plot(mu,p_approx,'b-.');hold on;
plot(mu,p,'m-');hold on;
grid on;

%Compare this with the asymptotic probability
mu = mu_0:-0.001:mu_n;
p_asym = zeros(1,length(mu));
int_a = zeros(1,length(mu));
for i=1:length(mu);
    T_r = -sqrt(mu(i))+dT:dT:1;
    p_a = f_a_npdf(mu(i),T_r,0.001,0,epsilon,beta);
    int_a(i) = trapz(T_r,p_a);
    p_asym(i) = 1-int_a(i)/int_a(1);
end;
plot(mu,p_asym,'b-.');

%Compare this with the numerical escape probability
%loop = 10000;
%dC = 1/200; %length of bin for histgram;
%dt = 0.1;
%T_min = -5;%f_T_neg(mu(i),A,B1); %escape boundary is different for each mu(i)
%mu2 = mu_0:-0.001:mu_n;
%p_num = zeros(1,length(mu2));
%for i=1:length(mu2);
    %mu3 = 0.1:(-v*dt):mu2(i);
    %[h,NUM] = f_ntem_histgram_count(loop,mu3,dt,epsilon,-sqrt(mu2(i)),T_min,0.001,0);
    %p_num(i) = NUM/loop;
%end;
%plot(mu2,p_num,'k:','Linewidth',2);