%compute partial pdf at mu=mu2 based on brownian motion and initial
%asymptotic approximation by using one-stage conditional pdf approximation
%given pdf at mu=mu1

function p_one = f_npdf_sym_local_one(mu1,mu2,T,T_s,p,epsilon,beta);
%mu1, initial condition where asymptotic approximation alone provides good
%approximation
%mu2, where the system stops
%dt, length of small time step
%T, series of the whole temperature range, which should be separated by
%T_s
%p, pdf at mu=mu1 in the range T

p_one = zeros(1,length(T));
%separate T by -sqrt(mu2);
%dT = T(2)-T(1);
v = epsilon^beta;
time = (mu1-mu2)/v;
m=1;
n = 1;
while m<length(T) && T(m)<=T_s+0.0001;
    m = m+1;
    if T(m)>-sqrt(mu1)
        n = n+1;
    end;
end;
%m
%n
%pause;
%test how many small fragments between -sqrt(mu1) and -sqrt(mu2) where the
%conditional probability actually work
%frag = (sqrt(mu1)-sqrt(mu2))/dT
%pause;

T1 = zeros(1,m+1);
T2 = zeros(1,length(T)-m);
p_11 = zeros(m+1,1); %pdf at mu1
for i=1:m+1; %assign T1 completely first then compute p_tran
    T1(i) = T(i);
    p_11(i) = p(i);
end;
p_tran = f_transit_pdf(T1,mu1,mu2,1000,0.1,epsilon,beta);
p_tran_check = zeros(m+1,m+1); %transit probability based on brownian motion, compared with numerical simulation one
for i=1:m+1;
    for j=1:m+1;
        p_tran_check(i,j) = exp(-(T1(i)-T1(j))^2/2/time/epsilon^2)/sqrt(2*time*pi)/epsilon;%the transition matrix should be replaced by the simulated pdf based on the full system
    end;  
end;
%p_tran
figure(3);plot(T1,p_tran(:,m+1),'k-');
figure(4);plot(T1,p_tran_check(:,m+1),'b-.');
%plot(T1,p_tran(m+1,:));%check the
%transition probability
%axis([-sqrt(mu1)-0.05 -sqrt(mu2)+0.05 0 max(p_tran(m+1,:))]);
%p_11
%pause;
p_1 = p_tran*p_11;
for i=1:length(T)-m;
    T2(i) = T(m+i);
end;
p_2 = f_npdf_sym6(mu2,T2,epsilon,beta); %quasi-stationary only, p_2(1) is used to connect the two pieces
%compare p_1 and p_2 to be one series under T
for i=1:length(T);
    if i<=m+1;
        p_one(i) = p_1(i)/p_1(m)*p_2(1);
    else
        p_one(i) = p_2(i-m);
    end;
end;

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%figure(1);
%plot(T,p_one);hold on;plot(T_s,0,'ko');hold on;plot(-sqrt(mu2),0,'b*');
%axis([-sqrt(mu1) -sqrt(mu2) 0 max(p_one)]);
%pause;

%check whether the pdf between -sqrt(mu(i)) and -sqrt(mu(i-1)) is nonzero
%before normalization with the asymptotic part
figure(2);
plot(T1,p_11,'r-.');hold on; %show the conditional pdf part based on T_s
plot(-sqrt(mu2),0,'ko');
%figure(3);
%plot(T2,p_2);
axis([-sqrt(mu1)-0.05 -sqrt(mu2)+0.05 0 max(p_11)]);
pause;