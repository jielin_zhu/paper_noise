%parameters for temperature model after simplification

function SP = sim_para(epsilon,sigma,I_0,a,b,c,D)
% return to parameters for normalized temperature model
SP = zeros(1,4);

mu_e = 4^5*epsilon*sigma*(a-1)^3/(3^3*b^4*I_0); %1
T_e = 4*(a-1)/(3*b); %2
A = epsilon*sigma*T_e^3/(D*c); %3
B1 = I_0*b/(D*c); %4

SP(1) = mu_e;
SP(2) = T_e;
SP(3) = A;
SP(4) = B1;

