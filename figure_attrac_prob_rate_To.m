%load files for concentration of trajectories of full asymmetric symmetric system with different
%drifting rate of bifurcation parameter
%generate the figure to compare the region of concentration in which P(T) is roughly 90% in the full
%system based on matching asymptotics with different amplitude of noise

%{
load file_PDF_asym_merge3.dat
[m1,n1] = size(file_PDF_asym_merge3);
T = file_PDF_asym_merge3(1:m1-1,1);
mu = file_PDF_asym_merge3(end,2:end);
pdf_asym_merge = file_PDF_asym_merge3(1:m1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_merge4.dat
[m2,n2] = size(file_PDF_asym_merge4);
T2 = file_PDF_asym_merge4(1:m2-1,1);
mu2 = file_PDF_asym_merge4(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge4(1:m2-1,2:end); %each column is pdf at mu(i)
%}

%combine two pieces of PDF_asym_merge6 mu>0 or mu<=0
load file_PDF_asym_merge6.dat
[m1,n1] = size(file_PDF_asym_merge6);
T = file_PDF_asym_merge6(1:m1-1,1);
mu1 = file_PDF_asym_merge6(end,2:end-2);
pdf_asym_merge1 = file_PDF_asym_merge6(1:m1-1,2:end-2); %each column is pdf at mu(i)
load file_PDF_asym_merge6_2.dat
[m1_2,n1_2] = size(file_PDF_asym_merge6_2);
mu2 = file_PDF_asym_merge6_2(end,2:end);
pdf_asym_merge2 = file_PDF_asym_merge6_2(1:m1_2-1,2:end); %each column is pdf at mu(i)
mu = [mu1 mu2];
pdf_asym_merge = [pdf_asym_merge1 pdf_asym_merge2];

load file_PDF_asym_merge8.dat
[m2_1,n2_1] = size(file_PDF_asym_merge8);
T2 = file_PDF_asym_merge8(1:m2_1-1,1);
mu_asym2_1 = file_PDF_asym_merge8(end,2:end);
pdf_asym_merge2_1 = file_PDF_asym_merge8(1:m2_1-1,2:end); %each column is pdf at mu(i)
load file_PDF_asym_merge8_2.dat
[m2_2,n2_2] = size(file_PDF_asym_merge8_2);
%T_asym2_2 = file_PDF_asym_merge8_2(1:m2_2-1,1);
mu_asym2_2 = file_PDF_asym_merge8_2(end,2:end);
pdf_asym_merge2_2 = file_PDF_asym_merge8_2(1:m2_2-1,2:end); %each column is pdf at mu(i)
mu2 = [mu_asym2_1 mu_asym2_2];
pdf_asym_merge2 = [pdf_asym_merge2_1 pdf_asym_merge2_2];

%introduce parameter value T2 and Td for asymmetry full case
epsilon_0 = 0.69;
sigma = 5.67*10^(-8);
I_0 = 1.366*10^3;
a = 2.8;
b = 0.009;
c = 1.5*10^8;
D = 1/(2*10^7);
SP = sim_para(epsilon_0,sigma,I_0,a,b,c,D);
mu_e = SP(1);
T_eq = SP(2);
A = SP(3);
B1 = SP(4);
A2 = A;
B2 = B1;
c0 = -A2/(6*A)^3;
c1 = -4*A2/(6*A)^2;%-4*A2/(6*A)^2; %needs to be negative to fits the log term
c2 = 2*B2/(3*A*B1);%2*B2/(3*A*B1);
epsilon = 0.05;
%beta = 2.5;
dt = 0.1;
T_min = -3;

%{
T2 = zeros(1,length(mu)); %need to be computed for each mu(i)
index_imag = find(mu<0);
for i=1:min(index_imag)-1;%i=max(index_imag)+1:length(mu);
    %R = f_n_fullroot(mu(i),c0,c1,c2);
    R = f_n_root(mu(i),c1,c2);
    T2(i) = R(2);
end;
Ts = T2+10*epsilon;
Ts(index_imag) = 0.2;
Ts(min(index_imag)-m:min(index_imag)-1) = 0.2;
Td = (T2-2*epsilon).*ones(1,length(mu));


%show if the PDF agrees at each mu(i)
for i=2:length(mu);
    figure(1);
    plot(T,pdf_asym_merge(:,i),'b-');hold on;
    %plot(T,pdf_asym_num(:,i),'r-.');hold on;
    %plot(Td(i-1),0,'ro');hold on;plot(T1,0,'b*');hold on;plot(T2,0,'bo');hold on;
    plot(Ts(i),0,'k*');
    hold off;
    %axis([-0.5 0.5 0 9]);
    title(['$$\mu$$= ',num2str(mu(i))],'fontsize',20,'interpreter','latex');
    pause;
end;
%}

%two series for upper and lower bound of T at each mu(i)
%satisfying prob(T<Tl)=prob(T>Th)=0.05
Tl = zeros(length(mu),1);
Th = zeros(length(mu),1);
bound = 0.025;
for i=1:length(mu);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T,pdf_asym_merge(:,i)); %computing the escaping probability below Td
    while j1<length(T) && prob_upper<bound;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T(end-j1:end)',pdf_asym_merge(end-j1:end,i));
    end;
    %prob_upper
    Th(i) = T(end-j1);
    while j2<length(T) && prob_lower<bound; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T,pdf_asym_merge(:,i));
        prob_lower = X+trapz(T(1:j2)',pdf_asym_merge(1:j2,i));
    end;
    %prob_lower
    Tl(i) = T(j2);
end;

%generate the concentration region for larger noise
Tl2 = zeros(length(mu2),1);
Th2 = zeros(length(mu2),1);
for i=1:length(mu2);
    j1 = 1; %how many T requires for Tl
    j2 = 1; %how many T requires for Th
    prob_upper = 0;
    prob_lower = 1-trapz(T2,pdf_asym_merge2(:,i)); %computing the escaping probability below Td
    while j1<length(T2) && prob_upper<bound;
        j1 = j1+1;
        %size(T(end-j1:end))
        %size(pdf_asym_merge(end-j1:end))
        %pause
        prob_upper = trapz(T2(end-j1:end)',pdf_asym_merge2(end-j1:end,i));
    end;
    %prob_upper
    Th2(i) = T2(end-j1);
    while j2<length(T2) && prob_lower<bound; %not counting the escaping rate below Td
        j2 = j2+1;
        X = 1-trapz(T2,pdf_asym_merge2(:,i));
        prob_lower = X+trapz(T2(1:j2)',pdf_asym_merge2(1:j2,i));
    end;
    %prob_lower
    Tl2(i) = T2(j2);
end;

%also generate bifurcation diagram
%compute equilibirum of the normalized temperature asymmetric system
mu_s = mu(1):(-epsilon^beta*dt):mu(end);
index_pos = max(find(mu_s>0));
T_e = zeros(2,index_pos);
for i=1:index_pos;
    R = f_n_root(mu_s(i),c1,c2);
    %pause;
    T_e(1,i) = R(1);
    T_e(2,i) = R(2);
    %T(1,i) = R(3);
end;

Fig = figure;
plot(mu.*8/3/A/B1+mu_e,(Tl+1).*T_eq,'b--','linewidth',2);hold on;
plot(mu*8/3/A/B1+mu_e,(Th+1).*T_eq,'b--','linewidth',2);hold on;
plot(mu2.*8/3/A/B1+mu_e,(Tl2+1).*T_eq,'r--','linewidth',2);hold on;
plot(mu2*8/3/A/B1+mu_e,(Th2+1).*T_eq,'r--','linewidth',2);hold on;
plot(mu_s(1:index_pos).*8/3/A/B1+mu_e,(T_e(1,:)+1).*T_eq,'k-','linewidth',2);hold on;
plot(mu_s(1:index_pos)*8/3/A/B1+mu_e,(T_e(2,:)+1).*T_eq,'k-.','linewidth',2);hold on;
xlabel('$$\bar{\mu}$$','FontSize',40,'interpreter','latex');
ylabel('$$\bar{T}$$','FontSize',40,'interpreter','latex');
set(gca,'fontsize',18);
axis([0*8/3/A/B1+mu_e 0.06*8/3/A/B1+mu_e (-0.5+1)*T_eq (0.5+1)*T_eq]);
print(Fig,'-depsc','figure_attract_prob_rate_To.eps')